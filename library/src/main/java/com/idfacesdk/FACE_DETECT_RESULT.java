package com.idfacesdk;

import android.os.Parcel;
import android.os.Parcelable;

// FACE_DETECT_RESULT类, 存放检测到的人脸参数
public class FACE_DETECT_RESULT implements Parcelable {
    public int nFaceLeft, nFaceTop, nFaceRight, nFaceBottom; // 人脸坐标
    public int nLeftEyeX, nLeftEyeY, nRightEyeX, nRightEyeY; // 左右眼坐标
    public int nMouthX, nMouthY, nNoseX, nNoseY; // 嘴巴鼻子坐标
    public int nAngleYaw, nAnglePitch, nAngleRoll; // 人脸偏转角度（左右，上下，水平方向）
    public int nQuality; // 人脸质量
    public byte[] FaceData; // 脸部详细参数，可据此判断睁闭眼张嘴微笑等姿状，特征提取需要此数据确认脸部轮廓，所以提特征时需要将人脸检测的结果原样输入

    public FACE_DETECT_RESULT() {
        FaceData = new byte[512];
    } // 定义对象时自动分配空间


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.nFaceLeft);
        dest.writeInt(this.nFaceTop);
        dest.writeInt(this.nFaceRight);
        dest.writeInt(this.nFaceBottom);
        dest.writeInt(this.nLeftEyeX);
        dest.writeInt(this.nLeftEyeY);
        dest.writeInt(this.nRightEyeX);
        dest.writeInt(this.nRightEyeY);
        dest.writeInt(this.nMouthX);
        dest.writeInt(this.nMouthY);
        dest.writeInt(this.nNoseX);
        dest.writeInt(this.nNoseY);
        dest.writeInt(this.nAngleYaw);
        dest.writeInt(this.nAnglePitch);
        dest.writeInt(this.nAngleRoll);
        dest.writeInt(this.nQuality);
        dest.writeByteArray(this.FaceData);
    }

    protected FACE_DETECT_RESULT(Parcel in) {
        this.nFaceLeft = in.readInt();
        this.nFaceTop = in.readInt();
        this.nFaceRight = in.readInt();
        this.nFaceBottom = in.readInt();
        this.nLeftEyeX = in.readInt();
        this.nLeftEyeY = in.readInt();
        this.nRightEyeX = in.readInt();
        this.nRightEyeY = in.readInt();
        this.nMouthX = in.readInt();
        this.nMouthY = in.readInt();
        this.nNoseX = in.readInt();
        this.nNoseY = in.readInt();
        this.nAngleYaw = in.readInt();
        this.nAnglePitch = in.readInt();
        this.nAngleRoll = in.readInt();
        this.nQuality = in.readInt();
        this.FaceData = in.createByteArray();
    }

    public static final Creator<FACE_DETECT_RESULT> CREATOR = new Creator<FACE_DETECT_RESULT>() {
        @Override
        public FACE_DETECT_RESULT createFromParcel(Parcel source) {
            return new FACE_DETECT_RESULT(source);
        }

        @Override
        public FACE_DETECT_RESULT[] newArray(int size) {
            return new FACE_DETECT_RESULT[size];
        }
    };
}
