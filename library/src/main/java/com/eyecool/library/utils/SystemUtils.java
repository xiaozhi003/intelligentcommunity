package com.eyecool.library.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.PowerManager;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.util.Enumeration;

/**
 * Created by ouyangfan on 2018/3/19.
 */

public class SystemUtils {

    //获取跟路径
    public static String getRootPath() {
        return Environment.getExternalStorageDirectory().getPath();
    }

    public static String getAppCachePath(Context context) {
        return context.getApplicationContext().getCacheDir().getAbsolutePath();
    }

    //获取包信息
    public static PackageInfo getPackageInfo(Context context) {
        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return packageInfo;
    }

    //获取当前app的版本号名字
    public static String getVersionName(Context context) {
        PackageInfo packageInfo = getPackageInfo(context);
        String versionName = null;
        if (packageInfo != null) {
            versionName = packageInfo.versionName;
        }
        return versionName;
    }

    //获取当前app的版本号
    public static int getVersionCode(Context context) {
        PackageInfo packageInfo = getPackageInfo(context);
        int versionCode = -1;
        if (packageInfo != null) {
            versionCode = packageInfo.versionCode;
        }
        return versionCode;
    }

    //获取包名
    public static String getPackageName(Context context) {
        return context.getPackageName();
    }

    /**
     * 获取屏幕方向
     *
     * @param context
     * @return Configuration.ORIENTATION_LANDSCAPE和Configuration.ORIENTATION_PORTRAIT
     */
    public static int getScreenDirection(Context context) {
        return context.getResources().getConfiguration().orientation;
    }

    /**
     * 隐藏键盘
     *
     * @param context
     */
    public static void hideKeyboard(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }


    public static boolean checkPermission(Context context, String perssion) {
        PackageManager pm = context.getApplicationContext().getPackageManager();
        boolean permission = (PackageManager.PERMISSION_GRANTED == pm.checkPermission(perssion, getPackageName(context)));
        return permission;
    }


    //静默安装
    public static boolean install(String apkPath) {
        Logs.e("开始安装");
        boolean result = false;
        DataOutputStream dataOutputStream = null;
        BufferedReader errorStream = null;
        try {
            // 申请su权限
            Process process = Runtime.getRuntime().exec("su");
            dataOutputStream = new DataOutputStream(process.getOutputStream());
            // 执行pm install命令
            String command = "pm install -r " + apkPath + "\n";
            dataOutputStream.write(command.getBytes(Charset.forName("utf-8")));
            dataOutputStream.flush();
            dataOutputStream.writeBytes("exit\n");
            dataOutputStream.flush();
            process.waitFor();
            errorStream = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String msg = "";
            String line;
            // 读取命令的执行结果
            while ((line = errorStream.readLine()) != null) {
                msg += line;
            }
            Log.d("TAG", "install msg is " + msg);
            // 如果执行结果中包含Failure字样就认为是安装失败，否则就认为安装成功
            if (!msg.contains("Failure")) {
                result = true;
                Logs.e("安装成功------");
            } else {
                Logs.e("安装失败------");
            }
        } catch (Exception e) {
            Log.e("TAG", e.getMessage(), e);
        } finally {
            try {
                if (dataOutputStream != null) {
                    dataOutputStream.close();
                }
                if (errorStream != null) {
                    errorStream.close();
                }
            } catch (IOException e) {
                Log.e("TAG", e.getMessage(), e);
            }
        }
        return result;
    }

    // 获取本地IP函数
    public static String getLocalIPAddress() {
        try {
            for (Enumeration<NetworkInterface> mEnumeration = NetworkInterface.getNetworkInterfaces(); mEnumeration.hasMoreElements(); ) {
                NetworkInterface intf = mEnumeration.nextElement();
                for (Enumeration<InetAddress> enumIPAddr = intf.getInetAddresses(); enumIPAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIPAddr.nextElement();
                    // 如果不是回环地址
                    if (!inetAddress.isLoopbackAddress()) {
                        // 直接返回本地IP地址
                        String ipAddress = inetAddress.getHostAddress().toString();
                        Logs.e("Ip地址： " + ipAddress);
                        return ipAddress;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("Error", ex.toString());
        }
        return null;
    }

    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ipAddress = inetAddress.getHostAddress().toString();
                        if (!ipAddress.contains("::"))//通过“::”将IPV6地址过滤掉
                            return inetAddress.getHostAddress().toString();
                    } else
                        continue;
                }
            }
        } catch (SocketException ex) {
            System.out.println("WifiPreference IpAddress" + ex.toString());
        }
        return null;
    }

    public static String getIp(Context context) {
        // 实例化mConnectivityManager对象
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);// 获取系统的连接服务
        // 实例化mActiveNetInfo对象
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();// 获取网络连接的信息
        if (networkInfo != null) {
            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                Logs.e("ip is wifi");
                return getWifiIp(context);
            } else if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                Logs.e("ip is local");
                return getLocalIPAddress();
            } else if (networkInfo.getType() == ConnectivityManager.TYPE_ETHERNET) {
                Logs.e("ip is eth");
                return getLocalIpAddress();
            }
        }
        Logs.e("ip is null");
        return null;
    }

    /**
     * 获取wifi的IP地址
     */
    public static String getWifiIp(Context context) {
        WifiManager wifiMan = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifiMan.getConnectionInfo();
        int ipAddress = info.getIpAddress();
        String ipString = ""; // 本机在WIFI状态下路由分配给的IP地址
        // 获得IP地址的方法一：
        if (ipAddress != 0) {
            ipString = ((ipAddress & 0xff) + "." + (ipAddress >> 8 & 0xff) + "." + (ipAddress >> 16 & 0xff) + "." + (ipAddress >> 24 & 0xff));
        } else {
            Logs.e(ipString + "----");
        }
        return ipString;
    }

    public static void restart(Context context) {
        Intent intent2 = new Intent(Intent.ACTION_REBOOT);
        intent2.putExtra("nowait", 1);
        intent2.putExtra("interval", 1);
        intent2.putExtra("window", 0);
        context.sendBroadcast(intent2);
    }

    public static void restart2(Context context) {
        PowerManager manager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        manager.reboot(null);
    }
}
