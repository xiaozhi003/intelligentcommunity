package com.eyecool.library.utils;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ouyangfan on 2018/4/18.
 */

public class GsonUtils {


    /**
     * 数据解析
     *
     * @param jsonStr JSON字符串
     * @return UniApiResult<GoodsInfoModel> 数据对象
     */
    public static <T> NetworkData<T> parseJson(String jsonStr) {
        Gson gson = new Gson();
        Type jsonType = new TypeToken<NetworkData<T>>() {
        }.getType();
        return gson.fromJson(jsonStr, jsonType);
    }

    public static <T> T fromJson(String json, Class<T> classOfT) {
        Gson gson = new Gson();
        return gson.fromJson(json, classOfT);
    }

    public static String toJson(Object object) {
        Gson gson = new Gson();
        return gson.toJson(object);
    }
}
