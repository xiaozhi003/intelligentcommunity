package com.eyecool.library.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ouyangfan on 2018/4/2.
 */

public class ActivityUtils {
    private static ActivityUtils sActivityUtils;
    private ArrayList<Activity> activities;

    public static ActivityUtils getInstance() {
        if (sActivityUtils == null) {
            sActivityUtils = new ActivityUtils();
        }
        return sActivityUtils;
    }

    private ActivityUtils() {
        activities = new ArrayList<>();
    }

    public void add(Activity activity) {
        activities.add(activity);
    }

    public void remove(Activity activity) {
        if (activity != null) {
            activity.finish();
        }
        activities.remove(activity);
    }

    public void removeAll() {
        for (Activity activity : activities) {
            activity.finish();
        }
        activities.clear();
    }

    /**
     * 判断activity是否在栈顶，获取当前20个activity栈，只要有一个栈的栈顶是改activity则判断为真
     *
     * @param activity 将要判断的activity
     * @return true表示在栈顶，反之则false
     */
    public boolean isTopActivity(Activity activity) {
        try {
            ActivityManager activityManager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> runningTaskInfos = activityManager.getRunningTasks(20);
            for (ActivityManager.RunningTaskInfo runningTaskInfo : runningTaskInfos) {
                if (TextUtils.equals(runningTaskInfo.topActivity.getClassName(), activity.getClass().getName())) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void exitApp() {
        removeAll();
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }
}
