package com.eyecool.library.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ouyangfan on 2018/4/18.
 */

public class SharedPreferencesUtils {

  public static final String RESTART_TEST_START = "restart_test_start";
  public static final String RESTART_FIRST_TIME = "restart_first_time";
  public static final String RESTART_COUNT = "restart_count";
  public static final String KEY_CAMERA_LIGHT_START_TIME = "key_camera_light_start_time";
  public static final String KEY_CAMERA_LIGHT_CLOSE_TIME = "key_camera_light_close_time";
  public static final String KEY_CAMERA_LIGHT_SETTING = "key_camera_light_setting";
  public static final String KEY_SOFT = "key_soft";
  public static final String KEY_VOLUME = "key_volume";
  public static final String KEY_GPIO_DELAY = "key_gpio_delay";
  public static final String KEY_IS_SAVE_FAILE_PIC = "key_is_save_faile_pic";
  public static final String KEY_IS_REBOOTED = "key_is_rebooted";

  public static void saveString(Context context, String name, String key, String value) {
    SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPreferences.edit();
    editor.putString(key, value);
    editor.commit();
  }

  public static String getString(Context context, String name, String key) {
    SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
    return sharedPreferences.getString(key, "");
  }

  public static void saveBoolean(Context context, String name, String key, boolean value) {
    SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPreferences.edit();
    editor.putBoolean(key, value);
    editor.commit();
  }

  public static void saveInt(Context context, String name, String key, int value) {
    SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPreferences.edit();
    editor.putInt(key, value);
    editor.commit();
  }

  public static boolean getBoolean(Context context, String name, String key) {
    SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
    return sharedPreferences.getBoolean(key, false);
  }

  public static boolean getBoolean(Context context, String name, String key, boolean defValue) {
    SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
    return sharedPreferences.getBoolean(key, defValue);
  }

  public static int getInt(Context context, String name, String key) {
    SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
    return sharedPreferences.getInt(key, 0);
  }

  public static int getInt(Context context, String name, String key, int defaultValue) {
    SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
    return sharedPreferences.getInt(key, defaultValue);
  }
}
