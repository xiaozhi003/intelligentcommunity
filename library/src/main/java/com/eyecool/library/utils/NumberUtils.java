package com.eyecool.library.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;

/**
 * Created by ouyangfan on 2018/4/13.
 */

public class NumberUtils {
    /**
     * 大陆号码或香港号码均可
     */
    public static boolean isPhoneLegal(String str) throws PatternSyntaxException {
        return isChinaPhoneLegal(str) || isHKPhoneLegal(str);
    }

    /**
     * 大陆手机号码11位数，匹配格式：前三位固定格式+后8位任意数
     * 此方法中前三位格式有：
     * 13+任意数
     * 15+除4的任意数
     * 18+除1和4的任意数
     * 17+除9的任意数
     * 147
     */
    public static boolean isChinaPhoneLegal(String str) throws PatternSyntaxException {
        String regExp = "^((13[0-9])|(15[^4])|(18[0,2,3,5-9])|(17[0-8])|(147))\\d{8}$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(str);
        return m.matches();
    }

    /**
     * 香港手机号码8位数，5|6|8|9开头+7位任意数
     */
    public static boolean isHKPhoneLegal(String str) throws PatternSyntaxException {
        String regExp = "^(5|6|8|9)\\d{7}$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(str);
        return m.matches();
    }

    /**
     * 查找数组中最大的数
     *
     * @param src
     * @return
     */
    public static int findMaxNum(int[] src) {
        if (src != null) {
            int max = src[0];
            for (int i = 1; i < src.length; i++) {
                if (src[i] > max) {
                    max = src[i];
                }
            }
            return max;
        }
        return -1;
    }

    /**
     * 查找数组中最大的数
     *
     * @param src
     * @return
     */
    public static int findMaxNum(byte[] src) {
        if (src != null) {
            int max = src[0];
            for (int i = 1; i < src.length; i++) {
                if (src[i] > max) {
                    max = src[i];
                }
            }
            return max;
        }
        return -1;
    }
}
