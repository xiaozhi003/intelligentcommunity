package com.eyecool.library.utils;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ouyangfan on 2018/8/24.
 */

public class NetworkData<T>{

    private int resCode;
    private String resMsg;
    private int surplusCount;
    private T resData;

    public int getResCode() {
        return resCode;
    }

    public void setResCode(int resCode) {
        this.resCode = resCode;
    }

    public String getResMsg() {
        return resMsg;
    }

    public void setResMsg(String resMsg) {
        this.resMsg = resMsg;
    }

    public int getSurplusCount() {
        return surplusCount;
    }

    public void setSurplusCount(int surplusCount) {
        this.surplusCount = surplusCount;
    }

    public T getResData() {
        return resData;
    }

    public void setResData(T resData) {
        this.resData = resData;
    }

    @Override
    public String toString() {
        return "NetworkData{" +
                "resCode=" + resCode +
                ", resMsg='" + resMsg + '\'' +
                ", surplusCount=" + surplusCount +
                ", resData=" + resData +
                '}';
    }
}
