package com.eyecool.library.utils;

import android.util.Log;

import com.apkfuns.logutils.LogUtils;
import com.eyecool.library.BuildConfig;

/**
 * Created by ouyangfan on 2018/3/19.
 * 打印类，用于输出打印信息
 */

public class Logs {

  private static final boolean isDebug = BuildConfig.LOG;

  public static void d(Object o) {
    if (isDebug) {
      LogUtils.d(o);
    }
  }

  public static void d(String tag, String msg) {
    if (isDebug) {
      Log.d(tag, msg);
    }
  }

  public static void i(Object o) {
    if (isDebug) {
      LogUtils.i(o);
    }
  }

  public static void i(String tag, String msg) {
    if (isDebug) {
      Log.i(tag, msg);
    }
  }

  public static void e(Object o) {
    if (isDebug) {
      LogUtils.e(o);
    }
  }

  public static void e(String tag, String msg) {
    if (isDebug) {
      Log.e(tag, msg);
    }
  }

  public static void v(Object o) {
    if (isDebug) {
      LogUtils.v(o);
    }
  }

  public static void v(String tag, String msg) {
    if (isDebug) {
      Log.v(tag, msg);
    }
  }

  public static void w(Object o) {
    if (isDebug) {
      LogUtils.w(o);
    }
  }

  public static void w(String tag, String msg) {
    if (isDebug) {
      Log.w(tag, msg);
    }
  }

  public static void json(String json) {
    if (isDebug) {
      LogUtils.json(json);
    }
  }
}
