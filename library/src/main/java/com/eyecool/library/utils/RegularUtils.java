package com.eyecool.library.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ouyangfan on 2016/11/10.
 */

public class RegularUtils {
    /**
     * 判断是否手机号码
     *
     * @param mobiles
     * @return
     */
    public static boolean isMobileNO(String mobiles) {
        return Pattern.matches("^((13[0-9])|(147)|(15[^4,\\D])|(17[67])|(18[^4,\\D]))\\d{8}$", mobiles);
    }

    /**
     * 判断是否邮箱
     *
     * @param email
     * @return
     */
    public static boolean isEmailNO(String email) {
        return Pattern.matches("[a-zA-Z0-9_]+@[a-zA-Z0-9]+(\\.[a-zA-Z]+)+", email);
    }

    /**
     * 判断是否是数字
     *
     * @param number
     * @return
     */
    public static boolean isNumber(String number) {
        return Pattern.matches("[0-9]*", number);
    }

    /**
     * 检查是否是字母和数字
     *
     * @param number
     * @return
     */
    public static boolean isLetterNumber(String number) {
        return Pattern.matches("[0-9][a-zA-Z0-9]*", number);
    }

    /**
     * 判断是否是字母
     *
     * @param letter
     * @return
     */
    public static boolean isLetter(String letter) {
        return Pattern.matches("[A-Z]", letter);
    }

    /**
     * 判断单个字符是不是中文
     *
     * @param c
     * @return
     */
    private static boolean isChinese(char c) {
        Character.UnicodeBlock unicodeBlock = Character.UnicodeBlock.of(c);
        return null != unicodeBlock && (unicodeBlock == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || unicodeBlock == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || unicodeBlock == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || unicodeBlock == Character.UnicodeBlock.GENERAL_PUNCTUATION
                || unicodeBlock == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || unicodeBlock == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS);
    }

    /**
     * 判断字符串是不是中文
     *
     * @param name
     * @return
     */
    public static boolean isChinese(String name) {
        char[] ch = name.toCharArray();
        if (null != ch && ch.length > 0) {
            for (int i = 0, len = ch.length; i < len; i++) {
                char c = ch[i];
                if (isChinese(c)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 判断是否是数字,英文字母和中文
     *
     * @param str
     * @return
     */
    public static boolean isDataEnglishAndChinese(String str) {
        Pattern p = Pattern.compile("^[\u4E00-\u9FA50-9a-zA-Z_-]{0,}$");
        Matcher m = p.matcher(str);
        return m.matches();
    }

    /**
     * 检查是否是字母数字下划线
     *
     * @param number
     * @return
     */
    public static boolean isLetterNumber2(String number) {
        return Pattern.matches("[a-zA-Z0-9_]*", number);
    }

    //截取数字
    public static String getNumbers(String content) {
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()) {
            return matcher.group(0);
        }
        return "";
    }

    //判断是否是ip地址
    public static boolean isIP(String addr) {
        if (addr.length() < 7 || addr.length() > 15 || "".equals(addr)) {
            return false;
        }
        /**
         * 判断IP格式和范围
         */
        String rexp = "([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}";
        Pattern pat = Pattern.compile(rexp);
        Matcher mat = pat.matcher(addr);
        boolean ipAddress = mat.find();
        return ipAddress;
    }
}
