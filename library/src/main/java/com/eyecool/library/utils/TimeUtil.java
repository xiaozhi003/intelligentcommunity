package com.eyecool.library.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by ouyangfan on 2016/11/4.
 */

public class TimeUtil {

    public static long getTime(String time) {
        String[] split = time.split(":");
        int hour = Integer.parseInt(split[0]);
        int min = Integer.parseInt(split[1]);

        //得到日历实例，主要是为了下面的获取时间
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        //获取当前毫秒值
        long systemTime = System.currentTimeMillis();

        //是设置日历的时间，主要是让日历的年月日和当前同步
        calendar.setTimeInMillis(System.currentTimeMillis());
        // 这里时区需要设置一下，不然可能个别手机会有8个小时的时间差
        calendar.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        //设置在几点提醒
        calendar.set(Calendar.HOUR, hour);
        //设置在几分提醒
        calendar.set(Calendar.MINUTE, min);
        //下面这两个看字面意思也知道
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTimeInMillis();
    }

    public static String getCurrentTime() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
        return simpleDateFormat.format(date);
    }

    public static String getCurrentTime2() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        return simpleDateFormat.format(date);
    }

    public static String getCurrentYear() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
        return simpleDateFormat.format(date);
    }

    public static String getCurrentMonth() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM");
        return simpleDateFormat.format(date);
    }

    public static String getCurrentDay() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd");
        return simpleDateFormat.format(date);
    }

    public static String getCurrent24Hour() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH");
        return simpleDateFormat.format(date);
    }

    public static String getCurrent12Hour() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh");
        return simpleDateFormat.format(date);
    }

    public static String getCurrentMin() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm");
        return simpleDateFormat.format(date);
    }

    public static String getCurrentSecond() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ss");
        return simpleDateFormat.format(date);
    }

    public static String getCurrentMillis() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("SSS");
        return simpleDateFormat.format(date);
    }

    public static String getCurrentTime(String pattern) {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    public static String formatTime(long millis, String pattern) {
        Date date = new Date(millis);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    public static long formatTime(String time, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        try {
            Date date = simpleDateFormat.parse(time);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }

    /*获取星期几*/
    public static String getWeek() {
        Calendar cal = Calendar.getInstance();
        int i = cal.get(Calendar.DAY_OF_WEEK);
        switch (i) {
            case 1:
                return "星期日";
            case 2:
                return "星期一";
            case 3:
                return "星期二";
            case 4:
                return "星期三";
            case 5:
                return "星期四";
            case 6:
                return "星期五";
            case 7:
                return "星期六";
            default:
                return "";
        }
    }

    /**
     * 比较两个时间的先后顺序
     *
     * @param time1
     * @param time2
     * @return 前者比后者早，则返回true，否则返回false
     */
    public static boolean compareTime(String time1, String time2) throws ParseException {
        //如果想比较日期则写成"yyyy-MM-dd"就可以了
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        //将字符串形式的时间转化为Date类型的时间
        Date a = sdf.parse(time1);
        Date b = sdf.parse(time2);
        //Date类的一个方法，如果a早于b返回true，否则返回false
        if (a.before(b))
            return true;
        else
            return false;
    }

    /**
     * 比较两个时间的先后顺序
     *
     * @param time1
     * @param time2
     * @return 0代表两个时间相等，小于0代表参数time1就是在time2之后，大于0代表参数time1就是在time2之前
     */
    public static int compareTime(long time1, long time2) {
        //将日期转成Date对象作比较
        Date fomatDate1 = new Date(time1);
        Date fomatDate2 = new Date(time2);
        int result = fomatDate2.compareTo(fomatDate1);
        return result;
    }

    /**
     * 把秒数转换成时间，比如70转换成01：10
     *
     * @param time
     * @return
     */
    public static String number2Time(int time) {
        if (time > 60 || time == 60) {
            int min = time / 60;
            int second = time - (min * 60);
            String minStr;
            if (min < 10) {
                minStr = "0" + String.valueOf(min);
            } else {
                minStr = String.valueOf(min);
            }
            String secondStr;
            if (second < 10) {
                secondStr = "0" + second;
            } else {
                secondStr = String.valueOf(second);
            }
            return minStr + ":" + secondStr;
        } else {
            if (time < 10) {
                return "00:0" + time;
            }
            return "00:" + time;
        }
    }
}

