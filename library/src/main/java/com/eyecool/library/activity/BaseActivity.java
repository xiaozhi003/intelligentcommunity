package com.eyecool.library.activity;

import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eyecool.library.utils.ActivityUtils;

import java.util.List;

public abstract class BaseActivity extends AppCompatActivity {

    private Toast mToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        ActivityUtils.getInstance().add(this);
        initData();
        initView(savedInstanceState);
        loadData();
    }

    //初始化数据，包括上一个界面传递过来的数据、处理数据、动态添加权限等
    public abstract void initData();
    //初始化布局，包括设置布局、给控件赋值等
    public abstract void initView(Bundle savedInstanceState);
    //加载外部数据，外部数据包括数据库、网络、文件
    public abstract void loadData();

    public void showToast(int messageId) {
        String message = getString(messageId);
        showToast(message);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityUtils.getInstance().remove(this);
    }

    public void showToast(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mToast == null) {
                    mToast = Toast.makeText(BaseActivity.this, "", Toast.LENGTH_LONG);
                }
                mToast.setGravity(Gravity.BOTTOM,0,20);
                mToast.setText(message);
                mToast.show();
            }
        });
    }

    public void cancelToast() {
        if (mToast != null) {
            mToast.cancel();
        }
    }

    //动态添加权限
    public void addPermission(List<String> permissions, String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissions.add(permission);
            }
        }
    }

    //申请以上添加的权限
    public void requestPermission(List<String> permissions) {
        if (Build.VERSION.SDK_INT >= 23 && permissions != null && permissions.size() > 0) {
            String[] permissionArray = new String[permissions.size()];
            permissions.toArray(permissionArray);
            requestPermissions(permissionArray, 0);
        }
    }
}