package com.eyecool.library.http;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import rx.Subscriber;

/**
 * Created by ouyangfan on 2018/8/23.
 */

public class HttpManager {

    private static final String TAG = "HttpManager";

    public static final MediaType JSON = MediaType.parse("application/json;charset=UTF-8");

    private OkHttpClient mOkHttpClient;

    public HttpManager() {
        mOkHttpClient = new OkHttpClient();
    }

    public HttpManager(int connectTimeout, int readTimeout) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(connectTimeout, TimeUnit.SECONDS);
        builder.readTimeout(readTimeout, TimeUnit.SECONDS);
        mOkHttpClient = builder.build();
    }

    public void get(String url, Subscriber<? super String> subscriber) {
        get(url, null, subscriber);
    }

    public void get(String url, HashMap<String, String> parameters, Subscriber<? super String> subscriber) {
        String parameter = setGetParameters(parameters);
        url = url + parameter;
        Request request = new Request.Builder().url(url).build();
        execute(request, subscriber);
    }

    public void post(String url, Subscriber<? super String> subscriber) {
        post(url, null, subscriber);
    }

    //设置请求参数
    private JSONObject setParameters(HashMap<String, Object> parameters) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        if (parameters != null) {
            for (Map.Entry<String, Object> parameter : parameters.entrySet()) {
                jsonObject.put(parameter.getKey(), parameter.getValue());
            }
        }
        return jsonObject;
    }

    private String setGetParameters(HashMap<String, String> parameters) {
        StringBuffer result = new StringBuffer();
        result.append("?");
        if (parameters != null) {
            for (Map.Entry<String, String> parameter : parameters.entrySet()) {
                result.append(parameter.getKey());
                result.append("=");
                result.append(parameter.getValue());
                result.append("&");
            }
            result.delete(result.length() - 1, result.length());//删掉最后一个&符号
        }
        return result.toString();
    }

    public void post(String url, HashMap<String, Object> parameters, Subscriber<? super String> subscriber) {
        try {
            JSONObject jsonObject = setParameters(parameters);
            String data = jsonObject.toString();
//            Logs.e("request： " + data);
            Log.e(TAG, "request: " + data);
            RequestBody body = FormBody.create(JSON, data);
            Request request = new Request.Builder().url(url).post(body).build();
            execute(request, subscriber);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //执行请求
    private void execute(Request request, final Subscriber<? super String> subscriber) {
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                subscriber.onError(e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String data = response.body().string();
//                    Logs.e("response：  " + data);
                    subscriber.onNext(data);
//                    Logs.e(data);
                } else {
//                    Logs.e(response);
                    subscriber.onNext("服务器出错");
                }
                subscriber.onCompleted();
            }
        });
    }
}
