package com.eyecool.library.algorithm.door;

/**
 * Created by ouyangfan on 2018/8/30.
 */

public interface IDoor {

    void open();

    void close();
}
