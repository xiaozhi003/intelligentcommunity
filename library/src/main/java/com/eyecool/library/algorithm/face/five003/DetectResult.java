package com.eyecool.library.algorithm.face.five003;

import android.os.Parcel;
import android.os.Parcelable;

import com.idfacesdk.FACE_DETECT_RESULT;

import java.util.Arrays;

/**
 * Created by ouyangfan on 2018/8/20.
 */

public class DetectResult implements Parcelable {

    private FACE_DETECT_RESULT mFaceDetectResult;

    private int mWidth;

    private int mHeight;

    private byte[] mRgb24Data;

    public FACE_DETECT_RESULT getFaceDetectResult() {
        return mFaceDetectResult;
    }

    public void setFaceDetectResult(FACE_DETECT_RESULT faceDetectResult) {
        mFaceDetectResult = faceDetectResult;
    }

    public int getWidth() {
        return mWidth;
    }

    public void setWidth(int width) {
        mWidth = width;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setHeight(int height) {
        mHeight = height;
    }

    public byte[] getRgb24Data() {
        return mRgb24Data;
    }

    public void setRgb24Data(byte[] rgb24Data) {
        mRgb24Data = rgb24Data;
    }

    @Override
    public String toString() {
        return "DetectResult{" +
                "mFaceDetectResult=" + mFaceDetectResult +
                ", mWidth=" + mWidth +
                ", mHeight=" + mHeight +
                ", mRgb24Data=" + Arrays.toString(mRgb24Data) +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mFaceDetectResult, flags);
        dest.writeInt(this.mWidth);
        dest.writeInt(this.mHeight);
        dest.writeByteArray(this.mRgb24Data);
    }

    public DetectResult() {
    }

    protected DetectResult(Parcel in) {
        this.mFaceDetectResult = in.readParcelable(FACE_DETECT_RESULT.class.getClassLoader());
        this.mWidth = in.readInt();
        this.mHeight = in.readInt();
        this.mRgb24Data = in.createByteArray();
    }

    public static final Parcelable.Creator<DetectResult> CREATOR = new Parcelable.Creator<DetectResult>() {
        @Override
        public DetectResult createFromParcel(Parcel source) {
            return new DetectResult(source);
        }

        @Override
        public DetectResult[] newArray(int size) {
            return new DetectResult[size];
        }
    };
}
