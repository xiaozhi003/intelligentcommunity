package com.eyecool.library.algorithm.face.five003;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.idfacesdk.FACE_DETECT_RESULT;

/**
 * Created by ouyangfan on 2018/6/4.
 *
 * 5003算法人脸框
 */

@SuppressLint("AppCompatCustomView")
public class FaceView extends ImageView {

    private FACE_DETECT_RESULT mFaceDetectResult;

    private Paint mPaint;

    public FaceView(Context context) {
        super(context);
        initPaint();
    }

    public FaceView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    public FaceView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaint();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mFaceDetectResult != null) {//具体算法具体调整
            int width = getWidth();
            int left = width - mFaceDetectResult.nFaceLeft - 20;
            int top = mFaceDetectResult.nFaceTop;
            int right = width - mFaceDetectResult.nFaceRight - 20;
            int bottom = mFaceDetectResult.nFaceBottom;
            // 绘制一个矩形
            canvas.drawRect(new Rect(right, top, left, bottom), mPaint);
        }
    }

    public void setFace(FACE_DETECT_RESULT faceDetectResult) {
        this.mFaceDetectResult = faceDetectResult;
        invalidate();
    }

    private void initPaint(){
        // 首先定义一个paint
        mPaint = new Paint();
        // 设置颜色
        mPaint.setColor(Color.YELLOW);
        // 设置样式-空心矩形
        mPaint.setStyle(Paint.Style.STROKE);
    }
}
