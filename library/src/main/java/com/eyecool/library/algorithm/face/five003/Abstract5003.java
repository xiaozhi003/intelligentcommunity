package com.eyecool.library.algorithm.face.five003;

import android.content.Context;
import android.graphics.Bitmap;

import com.ChlFaceSdk.ChlFaceSdk;
import com.eyecool.library.algorithm.face.IFaceRecognition;
import com.eyecool.library.utils.FileUtils;
import com.eyecool.library.utils.Logs;
import com.eyecool.library.utils.SystemUtils;
import com.eyecool.library.utils.TimeUtil;
import com.idfacesdk.FACE_DETECT_RESULT;

import java.io.IOException;

/**
 * Created by ouyangfan on 2018/8/16.
 */

public abstract class Abstract5003 implements IFaceRecognition {

  private static final String TAG = Abstract5003.class.getSimpleName();

  protected boolean isInit = false;

  protected IInitListener mIInitListener;

  protected String mCacheDir = ""; // 本APP的cache目录


  public FaceAlgorithmLogInfo faceAlgorithmLogInfo;

  public void setFaceAlgorithmLogInfo(FaceAlgorithmLogInfo f) {
    this.faceAlgorithmLogInfo = f;
  }

  public interface FaceAlgorithmLogInfo {
    void d(String debug);

    void e(String error);
  }

  public Abstract5003(Context context) {
    mCacheDir = SystemUtils.getAppCachePath(context.getApplicationContext());
  }

  public interface IInitListener {
    void init(boolean isInit);
  }

  public boolean isInit() {
    return isInit;
  }

  public void setIInitListener(IInitListener IInitListener) {
    mIInitListener = IInitListener;
  }

  protected void initExecute() {
    //初始化
    new Thread() {
      @Override
      public void run() {
        long start = TimeUtil.getCurrentTimeMillis();
        int ret = ChlFaceSdk.Init(4, mCacheDir);
        ChlFaceSdk.SetDetectSize(640);
        Logs.e(TAG, "初始化算法结果： " + ret);
        if (ret == 0) {
          isInit = true;
          Logs.e(TAG, "加密狗SDK初始化成功，初始化SDK所需时间为：  " + (TimeUtil.getCurrentTimeMillis() - start));
          if (mIInitListener != null) {
            mIInitListener.init(true);
          }
          // 初始化1：N算法列表句柄
          ChlFaceSdk.sFeatureList = ChlFaceSdk.ListCreate(ChlFaceSdk.sMaxFeatureNum);
          ChlFaceSdk.sSpecialFeatureList = ChlFaceSdk.ListCreate(ChlFaceSdk.sMaxSpecialFeatureNum);
          Logs.i(TAG, "算法句柄：" + ChlFaceSdk.sFeatureList);
          Logs.i(TAG, "算法句柄(红黑名单)：" + ChlFaceSdk.sSpecialFeatureList);
        } else {
          Logs.e(TAG, "加密狗SDK初始化失败，所需时间为：  " + (TimeUtil.getCurrentTimeMillis() - start));
          if (mIInitListener != null) {
            mIInitListener.init(false);
          }
        }
      }
    }.start();
  }

  /////////////////////////////
  //                        //
  //  以下为检测人脸          //
  //                        //
  ///////////////////////////

  @Override
  public synchronized DetectResult detectFaceInfrared(Bitmap bitmap) {
    if (!isInit) {
      return null;
    }
    String path = SystemUtils.getRootPath() + "/temp/";
    String fileName = "tempInfrared.jpg";
    try {
      FileUtils.saveFile(path, fileName, bitmap);
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
    if (bitmap == null) {
      Logs.e(TAG, "detectFace   bitmap == null");
      return null;
    }
    int width = bitmap.getWidth();
    int height = bitmap.getHeight();
//    return null;
    return detectFace(0, path + fileName, width, height);

//    DetectResult result = detectFace(SystemUtils.getRootPath() + "/" + fileName, width, height);
//    Logs.d(TAG,"检测人脸结果 红外：" + (result == null));
//    return result;
  }

  @Override
  public synchronized DetectResult detectFaceColours(Bitmap bitmap) {
    if (!isInit) {
      return null;
    }
    String path = SystemUtils.getRootPath() + "/temp/";
    String fileName = "tempColours.jpg";
    try {
      FileUtils.saveFile(path, fileName, bitmap);
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
    if (bitmap == null) {
      Logs.e(TAG, "detectFace   bitmap == null");
      return null;
    }
    int width = bitmap.getWidth();
    int height = bitmap.getHeight();
    return detectFace(1, path + fileName, width, height);
//    DetectResult result = detectFace(SystemUtils.getRootPath() + "/" + fileName, width, height);
//    Logs.d(TAG,"检测人脸结果 可见光：" + (result == null));
//
//    return result;
  }

  @Override
  public synchronized DetectResult detectFaceByNV21(byte[] nv21, int width, int height) {
    return detectFaceByNV21(0, nv21, width, height);
  }

  public synchronized DetectResult detectFaceByNV21(int channelNo, byte[] nv21, int width, int height) {
    byte[] rgb24 = new byte[3 * width * height];
    FACE_DETECT_RESULT faceResult = new com.idfacesdk.FACE_DETECT_RESULT();
    int[] w = {0};
    int[] h = {0};
    long startTime = TimeUtil.getCurrentTimeMillis();
//    int ret = ChlFaceSdk.DetectFaceEx(channelNo, 2, nv21, width, height, 80, 0, 559, 480, 0, 0, rgb24, w, h, faceResult);
    int ret = ChlFaceSdk.DetectFaceEx(channelNo, 2, nv21, width, height, 0, 0, 0, 0, 0, 0, rgb24, w, h, faceResult);
    Logs.d(TAG, "检测yuv人脸为：" + ret + "  检测时间： " + (TimeUtil.getCurrentTimeMillis() - startTime) + " w:" + w[0] + " h:" + h[0]);
    if (ret == 1) {
      DetectResult detectResult = new DetectResult();
      detectResult.setFaceDetectResult(faceResult);
      detectResult.setWidth(w[0]);
      detectResult.setHeight(h[0]);
      detectResult.setRgb24Data(rgb24);
      return detectResult;
    } else {
      DetectResult detectResult = new DetectResult();
      detectResult.setWidth(w[0]);
      detectResult.setHeight(h[0]);
      detectResult.setRgb24Data(rgb24);
      return detectResult;
    }
  }

  public DetectResult detectFace(String path, int width, int height) {
    int bufferSize = 3 * width * height;//3是因为有RGB三种颜色
    byte[] data = new byte[bufferSize];
    int[] nWidth = new int[1];
    int[] nHeight = new int[1];
    nWidth[0] = nHeight[0] = 0;
    long startTime = TimeUtil.getCurrentTimeMillis();
    int ret = ChlFaceSdk.ReadImageFile(path, data, bufferSize, nWidth, nHeight, 24);
    Logs.e(TAG, "转RGB24时间：" + (TimeUtil.getCurrentTimeMillis() - startTime));
    if (ret < 0) {
      Logs.e(TAG, "读取本地照片失败   " + ret + "   " + path);
      return null;
    }
    return detectFaceByRGB24(data, nWidth[0], nHeight[0]);
  }

  public DetectResult detectFace(int channelNo, String path, int width, int height) {
    int bufferSize = 3 * width * height;//3是因为有RGB三种颜色
    byte[] data = new byte[bufferSize];
    int[] nWidth = new int[1];
    int[] nHeight = new int[1];
    nWidth[0] = nHeight[0] = 0;
    long startTime = TimeUtil.getCurrentTimeMillis();
    int ret = ChlFaceSdk.ReadImageFile(path, data, bufferSize, nWidth, nHeight, 24);
    Logs.e(TAG, "转RGB24时间：" + (TimeUtil.getCurrentTimeMillis() - startTime));
    if (ret < 0) {
      Logs.e(TAG, "读取本地照片失败   " + ret + "   " + path);
      return null;
    }
    return detectFaceByRGB24(channelNo, data, nWidth[0], nHeight[0]);
  }

  public DetectResult detectFaceByRGB24(byte[] rgb24, int width, int height) {
    long startTime = TimeUtil.getCurrentTimeMillis();
    FACE_DETECT_RESULT faceResult = new com.idfacesdk.FACE_DETECT_RESULT();
    int ret = ChlFaceSdk.DetectFace(0, rgb24, width, height, faceResult);
    Logs.e(TAG, "检测人脸结果为：" + ret + "  检测时间： " + (TimeUtil.getCurrentTimeMillis() - startTime));
    if (ret == 1) {
      DetectResult detectResult = new DetectResult();
      detectResult.setFaceDetectResult(faceResult);
      detectResult.setHeight(height);
      detectResult.setWidth(width);
      detectResult.setRgb24Data(rgb24);
      return detectResult;
    }
    return null;
  }

  public DetectResult detectFaceByRGB24(int channelNo, byte[] rgb24, int width, int height) {
    long startTime = TimeUtil.getCurrentTimeMillis();
    FACE_DETECT_RESULT faceResult = new com.idfacesdk.FACE_DETECT_RESULT();
    int ret = ChlFaceSdk.DetectFace(channelNo, rgb24, width, height, faceResult);
    Logs.e(TAG, "检测人脸结果为：" + ret + "  检测时间： " + (TimeUtil.getCurrentTimeMillis() - startTime));
    if (ret == 1) {
      DetectResult detectResult = new DetectResult();
      detectResult.setFaceDetectResult(faceResult);
      detectResult.setHeight(height);
      detectResult.setWidth(width);
      detectResult.setRgb24Data(rgb24);
      return detectResult;
    }
    return null;
  }


  @Override
  public <T> byte[] getFeature(T t) {
    if (!isInit) {
      throw new RuntimeException("算法未初始化");
    }
    if (t == null) {
      throw new RuntimeException("提取特征数据为空");
    }
    if (t instanceof DetectResult) {
      DetectResult detectResult = (DetectResult) t;
      return getFeature(detectResult);
    }
    return null;
  }

  public byte[] getFeature(DetectResult detectResult) {
    byte[] rgb24 = detectResult.getRgb24Data();
    int width = detectResult.getWidth();
    int height = detectResult.getHeight();
    FACE_DETECT_RESULT faceDetectResult = detectResult.getFaceDetectResult();
    byte[] feature = new byte[2600];
    long startTime = TimeUtil.getCurrentTimeMillis();
    int ret = ChlFaceSdk.FeatureGet(2, rgb24, width, height, faceDetectResult, feature);
    Logs.e(TAG, "提取特征结果： " + ret + "  提取特征时间： " + (TimeUtil.getCurrentTimeMillis() - startTime));
    if (ret == 0) {
      return feature;
    }
    return null;
  }

  /////////////////////////////
  //                        //
  //  以下为提取特征          //
  //                        //
  ///////////////////////////

  @Override
  public int compare(byte[] feature1, byte[] feature2) {
    Long start = TimeUtil.getCurrentTimeMillis();
    int ret = ChlFaceSdk.FeatureCompare(3, feature1, feature2);
    Logs.i(TAG, "对比结果为：" + ret + "  对比所需时间为： " + (TimeUtil.getCurrentTimeMillis() - start));
    return ret;
  }

  /////////////////////////////
  //                        //
  //  以下为检活              //
  //                        //
  ///////////////////////////


  public int liveDetect(int width, int height, byte[] colorData, FACE_DETECT_RESULT
      colorFaceDetectResult, byte[] bwData, FACE_DETECT_RESULT bwFaceDetectResult) {
    Long start = TimeUtil.getCurrentTimeMillis();
    int ret = ChlFaceSdk.LiveDetect(0, width, height, colorData, colorFaceDetectResult, bwData, bwFaceDetectResult);
    Logs.i(TAG, "检活ret:" + ret + " 时间： " + (TimeUtil.getCurrentTimeMillis() - start));
    return ret;
  }

  /**
   * 单目检测可见光
   *
   * @param channelNo
   * @param cameraType
   * @param width
   * @param height
   * @param rgb24
   * @param faceDetectResult
   * @return
   */
  public int liveDetectColor(int channelNo,int cameraType, int width, int height, byte[] rgb24, FACE_DETECT_RESULT faceDetectResult) {
    Long start = TimeUtil.getCurrentTimeMillis();
    int ret = ChlFaceSdk.LiveDetectOneCamera(channelNo, cameraType, width, height, rgb24, faceDetectResult);
    Logs.i(TAG, "单目检活ret:" + ret + " 时间： " + (TimeUtil.getCurrentTimeMillis() - start));
    return ret;
  }

//  public int quickLiveDetect(int channelNo, int width, int height, byte[] bwData, FACE_DETECT_RESULT bwFaceDetectResult) {
//    Long start = TimeUtil.getCurrentTimeMillis();
//    int ret = ChlFaceSdk.QuickLiveDetect(channelNo, width, height, bwData, bwFaceDetectResult);
//    Logs.i(TAG, "快速检活ret:" + ret + " 时间： " + (TimeUtil.getCurrentTimeMillis() - start));
//    return ret;
//  }
}
