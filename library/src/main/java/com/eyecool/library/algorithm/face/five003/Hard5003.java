package com.eyecool.library.algorithm.face.five003;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.text.TextUtils;

import com.ChlFaceSdk.ChlFaceSdk;
import com.eyecool.library.utils.Logs;
import com.eyecool.library.utils.TimeUtil;

import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by ouyangfan on 2018/8/21.
 */

public class Hard5003 extends Abstract5003 {

    private static Hard5003 sHard5003;
    private Context mContext;

    private UsbManager mUsbManager;
    private UsbConnectListener mUsbConnectListener;

    private int VID1 = 0x096E, PID1 = 0x0304; // 白色USB加密狗
    private int VID2 = 0x3689, PID2 = 0x8762; // 红色或灰色加密狗
    private UsbReceiver mUsbReceiver;

    private static final String ACTION_USB_PERMISSION = "com.eyeCool.Hard5003";

    public static Hard5003 getInstance(Context context) {
        if (sHard5003 == null) {
            synchronized (Hard5003.class) {
                if (sHard5003 == null) {
                    sHard5003 = new Hard5003(context);
                }
            }
        }
        return sHard5003;
    }

    private Hard5003(Context context) {
        super(context);
        mContext = context.getApplicationContext();
    }

    public void setUsbConnectListener(UsbConnectListener usbConnectListener) {
        mUsbConnectListener = usbConnectListener;
    }

    @Override
    public synchronized void init() {
        if (isInit) {
            if (mIInitListener != null) {
                mIInitListener.init(true);
            }
            return;
        }
        initExecute();
    }

    //连接USB加密狗
    public synchronized int connectUsb() {
        mUsbManager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
        if (mUsbManager == null) {
            Logs.e("创建mUsbManager失败");
            return -3;
        }
        HashMap<String, UsbDevice> stringDeviceMap = mUsbManager.getDeviceList();
        if (stringDeviceMap.isEmpty()) {
            Logs.e("请连接加密狗 !");
            return -2;
        } else {
            if (mUsbReceiver != null) {
                return -4;
            }
            mUsbReceiver = new UsbReceiver(this);
            Collection<UsbDevice> usbDevices = stringDeviceMap.values();
            //注册USB监听广播
            IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
            mContext.registerReceiver(mUsbReceiver, filter);
            //给USB加密狗添加读取权限
            PendingIntent permissionIntent = PendingIntent.getBroadcast(mContext.getApplicationContext(), 0, new Intent(ACTION_USB_PERMISSION), 0);
            Iterator<UsbDevice> usbDeviceIterator = usbDevices.iterator();
            while (usbDeviceIterator.hasNext()) {
                UsbDevice usbDevice = usbDeviceIterator.next();
                //添加USB加密狗的读取权限，必须先拿到设备才能够开通权限，所以必须先插入USB加密狗才能够添加权限，不能够监听插入USB插入时的监听（也许是我思路不对）
                if ((usbDevice.getVendorId() == VID1 && usbDevice.getProductId() == PID1) || (usbDevice.getVendorId() == VID2 && usbDevice.getProductId() == PID2)) {
                    // Request permission to access the device.
                    mUsbManager.requestPermission(usbDevice, permissionIntent);
                    return 0;
                }
            }
            Logs.e("连接加密狗失败");
            return -1;
        }
    }

    static class UsbReceiver extends BroadcastReceiver {

        private WeakReference<Hard5003> mHard5003WeakReference;

        public UsbReceiver(Hard5003 hard5003) {
            mHard5003WeakReference = new WeakReference<>(hard5003);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (TextUtils.equals(ACTION_USB_PERMISSION, action)) {
                synchronized (this) {
                    Logs.e("收到了加密狗的广播" + action);
                    final Hard5003 hard5003 = mHard5003WeakReference.get();
                    UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (device != null) {
                            UsbDeviceConnection connection = hard5003.mUsbManager.openDevice( device );
                            int fd = connection.getFileDescriptor(), nAuthType = 1;
                            if(device.getVendorId() == hard5003.VID2 && device.getProductId() == hard5003.PID2) nAuthType = 0;
                            long startTime = TimeUtil.getCurrentTimeMillis();
                            // 设置加密方式及加密狗句柄
                            ChlFaceSdk.SetAuth(nAuthType, fd);
                            Logs.e("加密狗连接成功，连接时间为：  " + (TimeUtil.getCurrentTimeMillis() - startTime));
                            if (hard5003.mUsbConnectListener != null) {
                                hard5003.mUsbConnectListener.usbConnected(true);
                            }
                        } else {
                            Logs.e("找不到加密狗设备");
                            if (hard5003.mUsbConnectListener != null) {
                                hard5003.mUsbConnectListener.usbConnected(false);
                            }
                        }
                    } else {
                        Logs.e("未授权给设备 " + device);
                        hard5003.mUsbConnectListener.usbConnected(false);
                    }
                }
            }
        }
    }

    @Override
    public void release() {
        mContext.unregisterReceiver(mUsbReceiver);
        unInit();
        sHard5003 = null;
    }

    @Override
    public void unInit() {
        ChlFaceSdk.Uninit();
    }

    public interface UsbConnectListener {
        void usbConnected(boolean isConnect);
    }
}
