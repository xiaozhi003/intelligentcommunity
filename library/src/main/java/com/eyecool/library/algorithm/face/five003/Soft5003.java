package com.eyecool.library.algorithm.face.five003;

import android.app.Activity;
import android.text.TextUtils;

import com.ChlFaceSdk.ChlFaceSdk;
import com.eyecool.library.utils.FileUtils;
import com.eyecool.library.utils.Logs;
import com.eyecool.library.utils.SystemUtils;
import com.idfacesdk.IdFaceSdk;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;

/**
 * Created by ouyangfan on 2018/8/20.
 */

public class Soft5003 extends Abstract5003 {

    private static final String TAG = Soft5003.class.getSimpleName();

    private static Soft5003 soft5003;

    private String strAppPath;

    private Activity mActivity;

    private boolean isRegister = false;


    public static Soft5003 getInstance(Activity activity) {
        if (soft5003 == null) {
            synchronized (Soft5003.class) {
                if (soft5003 == null) {
                    soft5003 = new Soft5003(activity);
                }
            }
        }
        return soft5003;
    }

    private Soft5003(Activity activity) {
        super(activity);
        mActivity = activity;
        // 获取本APP的cache目录
        try {
            strAppPath = SystemUtils.getRootPath() + "/FaceSdk";
            File localFile = new File(strAppPath);
            if (localFile.exists() == false) {
                Logs.e(TAG, "App path " + strAppPath + " invalid. Make it ...");
                localFile.mkdir();
            }
            if (localFile.exists() == false) {
                strAppPath = SystemUtils.getRootPath();
            }
        } catch (Exception e) {
            strAppPath = mCacheDir;
        }
    }

    public String getRunCode() {
        // 保存运行码文件
        String runCodeId = IdFaceSdk.IdFaceSdkGetRunCode(mActivity);
        String runCode = ChlFaceSdk.GetRunCode(mActivity);
        Logs.e(TAG, "运行码：" + runCodeId);
        Logs.e(TAG, "运行码：" + runCode);
        if (!TextUtils.isEmpty(runCode)) {//不为空则保存
            FileUtils.saveFile(strAppPath, "device.dat", runCode);
        }
        return runCode;
    }

    public boolean isRegister() {
        debugLog("开始读取本地注册码");
        String registerCode = readRegisterCode(strAppPath, "license.dat");
        Logs.e(TAG, "运行 注册码：" + registerCode);
        debugLog("软算法运行注册码："+registerCode);
        if (TextUtils.isEmpty(registerCode)) {
            Logs.e(TAG, "未授权");
            errorLog("当前终端未进行软授权!");
            return false;
        }
        debugLog("读取注册码，开始进行算法授权");
        return register(registerCode);
    }

    //算法授权，初始化算法之前调用
    public boolean register(String regCode) {
        int result = ChlFaceSdk.SetRegCode(mActivity, regCode);
        if (result == 0) {
            isRegister = true;
            Logs.e(TAG, "注册成功");
            debugLog("注册成功!");
            FileUtils.saveFile(strAppPath, "license.dat", regCode);
            return true;
        }
        errorLog("注册失败!");
        Logs.e(TAG, "注册失败 result:" + result);
        return false;
    }

    private boolean isIniting = false;

    @Override
    public synchronized void init() {
        if (isInit || isIniting) {//正在初始化或者已经初始化了则不初始化
            Logs.e(TAG, "加密狗正在初始化或已初始化");
            if (mIInitListener != null) {
                mIInitListener.init(true);
            }
            return;
        }
        if (!isRegister) {
            if (mIInitListener != null) {
                mIInitListener.init(false);
            }
            throw new RuntimeException("算法未授权，请输入注册码");
        }
        isIniting = true;
        Logs.e(TAG, "开始进行加密狗初始化");
        initExecute();
    }

    @Override
    protected synchronized void initExecute() {
        super.initExecute();
    }

    @Override
    public synchronized void release() {
        unInit();
        soft5003 = null;
    }

    @Override
    public void unInit() {
        if (isInit) {
            isRegister = false;
            ChlFaceSdk.Uninit();
            isInit = false;
        }
    }

    //读取本地注册码
    private String readRegisterCode(String strAppPath, String s) {
        String strLicFileName = strAppPath + "/license.dat";
        File fLic = new File(strLicFileName);
        if (!fLic.exists()) {
            Logs.e(TAG, "注册文件不存在");
            errorLog("注册文件不存在!");
            return null;
        }
        String str = null;
        try {
            // 读取文件内容
            byte[] buffer = new byte[35];
            DataInputStream reader = new DataInputStream(new FileInputStream(fLic));
            int i, len = reader.read(buffer, 0, buffer.length);
            if (len == 35 && buffer[0] == 'A' && (buffer[1] == 'K' || buffer[1] == 'T')) {
                for (i = 2; i < len; i++) {
                    if (i == 8 || i == 17 || i == 26) {
                        if (buffer[i] != '-') break;
                    } else {
                        if (!(buffer[i] >= '0' && buffer[i] <= '9' || buffer[i] >= 'A' && buffer[i] <= 'F' || buffer[i] >= 'a' && buffer[i] <= 'f'))
                            break;
                    }
                }
                if (i == len) str = new String(buffer);
            }
        } catch (Exception e) {
            Logs.e(TAG, "读取注册文件失败");
            errorLog("读取注册文件失败!");
            e.printStackTrace();
        }
        return str;
    }

    public boolean checkRegCode(String regCode) {
        String strRegCode = regCode.replace(" ", ""); // 输入内容去掉空格
        int len = strRegCode.length();
        if (len == 35) { // 检测有效性
            byte[] buffer = strRegCode.getBytes();
            if (buffer[0] == 'A' && (buffer[1] == 'K' || buffer[1] == 'T')) {
                int i;
                for (i = 2; i < len; i++) {
                    if (i == 8 || i == 17 || i == 26) {
                        if (buffer[i] != '-') break;
                    } else {
                        if (!(buffer[i] >= '0' && buffer[i] <= '9' || buffer[i] >= 'A' && buffer[i] <= 'F' || buffer[i] >= 'a' && buffer[i] <= 'f'))
                            break;
                    }
                }
                if (i == len) {
                    Logs.e(TAG, "验证通过");
                    return true;
                }
            }
        }
        return false;
    }

    private void debugLog(String msg) {
        if (faceAlgorithmLogInfo != null) {
            faceAlgorithmLogInfo.d(msg);
        }
    }

    private void errorLog(String error) {
        if (faceAlgorithmLogInfo != null) {
            faceAlgorithmLogInfo.e(error);
        }
    }

}
