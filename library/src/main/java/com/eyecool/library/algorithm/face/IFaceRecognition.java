package com.eyecool.library.algorithm.face;

import android.graphics.Bitmap;

/**
 * Created by ouyangfan on 2018/8/16.
 */

public interface IFaceRecognition {

  //初始化
  void init();

  //检测人脸
  <T> T detectFaceInfrared(Bitmap bitmap);

  //彩色图片
  <T> T detectFaceColours(Bitmap bitmap);

  //检测人脸
  <T> T detectFaceByNV21(byte[] nv21, int width, int height);

  //提取特征值
  <T> byte[] getFeature(T t);

  //特征值比对
  int compare(byte[] feature1, byte[] feature2);

  void release();

  void unInit();
}
