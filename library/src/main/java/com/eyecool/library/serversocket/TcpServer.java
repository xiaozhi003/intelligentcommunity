package com.eyecool.library.serversocket;

import com.eyecool.library.utils.Logs;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

/**
 * Created by ouyangfan on 2018/8/29.
 */

public class TcpServer extends Thread {

    private ServerSocket mServerSocket;
    private SocketThread.IReceiveMessage mReceiveMessage;

    public interface IReceiveMessage {
        void receiverMessage(byte[] message);
    }

    public TcpServer(int port) {
        try {
            mServerSocket = new ServerSocket(port);
            Logs.e("创建mServerSocket");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setReceiveMessage(SocketThread.IReceiveMessage receiveMessage) {
        mReceiveMessage = receiveMessage;
    }

    @Override
    public void run() {
        if (mServerSocket != null) {
            while (!isInterrupted()) {
                InputStream inputStream = null;
                try {
                    Logs.e("等待socket连接");
                    Socket socket = mServerSocket.accept();
                    Logs.e("收到了一个Socket");
                    try {
                        inputStream = socket.getInputStream();
                        byte[] buffer = new byte[19];
                        while (inputStream.read(buffer) != -1) {
                            Logs.e(new String(buffer));
                            if (inputStream != null) {
                                mReceiveMessage.receiverMessage(buffer);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                            inputStream = null;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        }
    }
}
