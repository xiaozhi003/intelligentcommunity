package com.eyecool.library.serversocket;

import com.eyecool.library.utils.Logs;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;

/**
 * Created by ouyangfan on 2018/8/29.
 * 服务端socket接收数据线程
 */

public class SocketThread extends Thread {

    private Socket mSocket;
    private InputStream mInputStream;
    private OutputStream mOutputStream;
    private IReceiveMessage mReceiveMessage;


    public SocketThread(Socket socket) {
        mSocket = socket;
        try {
            mInputStream = socket.getInputStream();
            mOutputStream = socket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setReceiveMessage(IReceiveMessage receiveMessage) {
        mReceiveMessage = receiveMessage;
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                byte[] buffer = new byte[18];
                while (mInputStream.read(buffer) != -1) {
                    Logs.e(Arrays.toString(buffer));
                    Logs.e(new String(buffer));
                    if (mReceiveMessage != null) {
                        mReceiveMessage.receiverMessage(buffer);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMessage(String message) {
        byte[] data = message.getBytes();
        if (mOutputStream != null) {
            try {
                mOutputStream.write(data, 0, data.length);
                mOutputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public interface IReceiveMessage {
        void receiverMessage(byte[] message);
    }
}
