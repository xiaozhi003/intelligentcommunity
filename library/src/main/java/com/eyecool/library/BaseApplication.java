package com.eyecool.library;

import android.app.Application;

/**
 * Created by ouyangfan on 2018/4/2.
 */

public class BaseApplication extends Application {

    private static BaseApplication ba;

    @Override
    public void onCreate() {
        super.onCreate();
        ba = this;

    }

    public static BaseApplication get() {
        return ba;
    }

}
