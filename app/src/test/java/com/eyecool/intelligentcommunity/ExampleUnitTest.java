package com.eyecool.intelligentcommunity;

import com.eyecool.library.utils.TimeUtil;

import org.junit.Test;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void test01() {
        List<String> original = Arrays.asList("1", "2", "10", "20");

        List<String> tmpList = Arrays.asList("1", "10");

        original.removeAll(tmpList);

        for (String s : original) {
            System.out.println(s);
        }
    }

    @Test
    public void test02() {
        List<Integer> list0 = Arrays.asList(0, 1, 2, 3, 4, 5);
        List<Integer> list1 = Arrays.asList(0, 1, 2);
        for (int i : list1) {
            for (int j : list0) {
                if (i == j) {
                    list0.add(10);
                    break;
                }
            }
        }
        for (Integer integer : list0) {
            System.out.println(integer);
        }
    }

    @Test
    public void test03() {
        String startTime = "18:00";
        String stopTime = "14:00";

        int currentHour = Integer.parseInt(TimeUtil.getCurrent24Hour());
        int currentMin = Integer.parseInt(TimeUtil.getCurrentMin());
        int totalCurrentMin = currentHour * 60 + currentMin;

        String[] splitStart = startTime.split(":");
        int startHour = Integer.parseInt(splitStart[0]);
        int startMin = Integer.parseInt(splitStart[1]);
        int totalStartMin = startHour * 60 + startMin;

        String[] splitStop = stopTime.split(":");
        int stopHour = Integer.parseInt(splitStop[0]);
        int stopMin = Integer.parseInt(splitStop[1]);
        int totalStopMin = stopHour * 60 + stopMin;

        System.out.println("totalCurrentMin=" + totalCurrentMin + ", totalStartMin=" + totalStartMin + ", totalStopMin=" + totalStopMin);

        if (totalStartMin > totalStopMin) {
            //跨一天时间
            if (totalCurrentMin < totalStopMin || totalCurrentMin > totalStartMin) {
                //开启补光灯
                System.out.println("handleCameraLight: 开启补光灯");
//                MyApp.mDoor.openCameraLight();
            } else {
//                MyApp.mDoor.closeCameraLight();
            }
        } else if (totalStartMin == totalStopMin) {
            System.out.println("handleCameraLight: 开启补光灯");
//            MyApp.mDoor.openCameraLight();
        } else {
            //一天时间段内
            if (totalCurrentMin > totalStartMin && totalCurrentMin < totalStopMin) {
                System.out.println("handleCameraLight: 开启补光灯");
//                MyApp.mDoor.openCameraLight();
            } else {
//                MyApp.mDoor.closeCameraLight();
            }
        }
    }

    @Test
    public void test04() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, 7);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        System.out.println(hour);
    }
}