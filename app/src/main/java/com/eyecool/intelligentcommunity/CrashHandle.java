package com.eyecool.intelligentcommunity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.os.Power;
import android.util.Log;
import android.widget.Toast;

import com.eyecool.intelligentcommunity.activity.MainActivity;
import com.eyecool.intelligentcommunity.utils.GpioJhc3399;
import com.eyecool.intelligentcommunity.utils.GpioManager;
import com.eyecool.intelligentcommunity.utils.SystemUtil;
import com.eyecool.library.utils.ActivityUtils;
import com.eyecool.library.utils.FileUtils;
import com.eyecool.library.utils.Logs;

import java.io.File;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TreeSet;
import java.util.UUID;

/**
 * 奔溃信息捕获
 *
 * @author Xiaozhi
 */
public class CrashHandle implements UncaughtExceptionHandler {
  public static final String TAG = "CrashHandle";
  private static final String CRASH_REPORTER_EXTENSION = ".cr";
  private static CrashHandle instance = new CrashHandle();
  // 系统默认的UncaughtException处理类
  private UncaughtExceptionHandler mDefaultHandler;
  private Context mContext;
  /* 使用Properties 来保存设备的信息和错误堆栈信息 */
  private Properties mDeviceCrashInfo = new Properties();
  // 存储设备信息
  private Map<String, String> mDevInfos = new HashMap<String, String>();
  private SimpleDateFormat formatdate = new SimpleDateFormat(
      "yyyy-MM-dd-HH-mm-ss");
  private String mCrashPath = Environment.getExternalStorageDirectory().getPath() + "/FaceSdk/crash_log/";

  private CrashHandle() {

  }// 保证只有一个实例

  public static CrashHandle getInstance() {
    if (instance == null)
      instance = new CrashHandle();
    return instance;
  }

  // 使用HTTP服务之前，需要确定网络畅通，我们可以使用下面的方式判断网络是否可用
  public static boolean isNetworkAvailable(Context context) {
    ConnectivityManager mgr = (ConnectivityManager) context
        .getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo[] info = mgr.getAllNetworkInfo();
      if (info != null) {
      for (int i = 0; i < info.length; i++) {
        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
          return true;
        }
      }
    }
    return false;
  }

  public void init(Context context) {
    mContext = context;
    // 获得默认的handle
    mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
    // 重新设置handle
    Thread.setDefaultUncaughtExceptionHandler(this);
  }

  public void uncaughtException(Thread thread, Throwable ex) {
    // TODO Auto-generated method stub
    // 如果用户没有处理则让系统默认的异常处理器来处理
    if (!handleException(ex) && mDefaultHandler != null) {
      mDefaultHandler.uncaughtException(thread, ex);
    } else {
      Logs.i(TAG,"强制关闭程序...");
      try {
        Thread.sleep(3000);
      } catch (InterruptedException e) {
        Log.e(TAG, "error");
      }


      android.os.Process.killProcess(android.os.Process.myPid());
      System.exit(1);
//      if ("rk3399-all".equals(SystemUtil.getSystemModel())) { //YZ3399
//        // 结束程序
//        android.os.Process.killProcess(android.os.Process.myPid());
//        System.exit(1);
//      }else if ("rk3399-mid".equals(SystemUtil.getSystemModel())) {//JHC3399
//        // 结束程序
//        ActivityUtils.getInstance().exitApp();
//      }
      // 重启app
      restartApp(mContext);
    }
  }

  // Throwable 包含了其线程创建时线程执行堆栈的快照
  private boolean handleException(Throwable ex) {
    // TODO Auto-generated method stub
    if (ex == null) {
      return false;
    }
    final String msg = ex.getLocalizedMessage();
    new Thread() {
      public void run() {
        Looper.prepare();
        Toast.makeText(mContext, "程序异常退出", Toast.LENGTH_LONG).show();
        Looper.loop();
      }
    }.start();
    // 收集设备信息
    collectDeviceInfo(mContext);
    // 保存信息
    String filePath = saveCrashInfo2File(ex);
    // 发送错误报告
//    sendCrashReportsToServer(mContext);
    // 发送错误报告文件到服务器
    sendCrashFileToServer(mContext, filePath);

    return true;
  }

  /**
   * 重启应用
   */
  private void restartApp(Context context) {
    Logs.i(TAG,"restartApp...");
    Intent intent = new Intent(context, MainActivity.class);
    PendingIntent restartIntent = PendingIntent.getActivity(context, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
    AlarmManager mgr = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
    mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, restartIntent);
    android.os.Process.killProcess(android.os.Process.myPid());
  }


  private void sendCrashFileToServer(Context context, String filePath) {
    Logs.i(TAG, "sendCrashFileToServer...");
    // 发送错误报告文件到服务器
  }

  public void sendPreviousReportsToServer() {
    sendCrashReportsToServer(mContext);
  }

  private void sendCrashReportsToServer(Context mcontext) {
    Logs.i(TAG, "sendCrashReportsToServer...");
    String[] crFiles = getCrashReportFiles(mcontext);
    if (crFiles != null && crFiles.length > 0) {
      TreeSet<String> sortedFiles = new TreeSet<String>();
      sortedFiles.addAll(Arrays.asList(crFiles));
      for (String fileName : sortedFiles) {
        File cr = new File(mcontext.getFilesDir(), fileName);
        postReport(cr);
        // cr.delete();
      }
    }
  }

  private void postReport(File cr) {
    Logs.d("发送错误报告到服务器 cr");
    // TODO 使用HTTP Post 发送错误报告到服务器
  }

  private String[] getCrashReportFiles(Context mcontext) {
    File filesDir = mcontext.getFilesDir();
    FilenameFilter filter = new FilenameFilter() {

      public boolean accept(File dir, String filename) {
        // TODO Auto-generated method stub
        return filename.endsWith(CRASH_REPORTER_EXTENSION);
      }
    };
    return filesDir.list(filter);
  }

  private String saveCrashInfo2File(Throwable ex) {
    Logs.i(TAG, "发送错误报告到文件");
    StringBuffer buffer = new StringBuffer();
    for (Map.Entry<String, String> entry : mDevInfos.entrySet()) {
      String key = entry.getKey();
      String value = entry.getValue();
      buffer.append(key + "=" + value + '\n');
    }
    // 可以用其回收在字符串缓冲区中的输出来构造字符串
    Writer writer = new StringWriter();
    // 向文本输出流打印对象的格式化表示形式
    PrintWriter printWriter = new PrintWriter(writer);
    // 将此 throwable 及其追踪输出至标准错误流
    ex.printStackTrace(printWriter);
    Throwable cause = ex.getCause();
    while (cause != null) {
      // 异常链
      cause.printStackTrace();
      cause = cause.getCause();
    }
    printWriter.close();
    String result = writer.toString();
    buffer.append(result);
    try {
      long timestamp = System.currentTimeMillis();
      String time = formatdate.format(new Date(timestamp));

      String fileName = "crash-" + time + "-" + timestamp + getRandomStr() + ".log";
      FileUtils.writeFile(mCrashPath + fileName, buffer.toString());
      return mCrashPath + fileName;
    } catch (Exception e) {
      Log.e(TAG, "an error occured while writing file...", e);
    }
    return null;
  }

  private void collectDeviceInfo(Context ctx) {
    Logs.i(TAG, "collectDeviceInfo...");
    // TODO Auto-generated method stub
    try {
      PackageManager pm = ctx.getPackageManager();
      PackageInfo pi = pm.getPackageInfo(ctx.getPackageName(),
          PackageManager.GET_ACTIVITIES);
      if (pi != null) {
        String versionName = pi.versionName == null ? "null"
            : pi.versionName;
        String versionCode = pi.versionCode + "";
        mDevInfos.put("versionName", versionName);
        mDevInfos.put("versionCode", versionCode);
      }
    } catch (NameNotFoundException e) {
      Log.v(TAG, "NameNotFoundException");
    }
    // 使用反射 获得Build类的所有类里的变量
    // Class 代表类在运行时的一个映射
    // 在Build类中包含各种设备信息,
    // 例如: 系统版本号,设备生产商 等帮助调试程序的有用信息
    // 具体信息请参考后面的截图

    Field[] fields = Build.class.getDeclaredFields();
    for (Field field : fields) {
      try {
        field.setAccessible(true);
        // get方法返回指定对象上此 Field 表示的字段的值
        mDevInfos.put(field.getName(), field.get(null).toString());
        Log.v(TAG, field.getName() + ":" + field.get(null).toString());
      } catch (Exception e) {
        Log.e(TAG, "an error occured when collect crash info", e);
      }
    }
  }

  public static String getRandomStr() {
    UUID uuid = UUID.randomUUID();
    return uuid.toString();
  }

  public static String getRandomName() {
    UUID uuid = UUID.randomUUID();
    Date now = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
    String name = dateFormat.format(now);
    return uuid.toString() + ".jpg";
  }
}