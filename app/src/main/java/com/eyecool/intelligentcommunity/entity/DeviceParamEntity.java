package com.eyecool.intelligentcommunity.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ouyangfan on 2018/8/24.
 */

public class DeviceParamEntity implements Parcelable {
    private int pubId;
    private String paramKey;
    private String paramVal;
    private String paramDefaultVal;
    private String deviceCode;
    private String proCode;
    private long createTime;
    private long updateTime;

    public int getPubId() {
        return pubId;
    }

    public void setPubId(int pubId) {
        this.pubId = pubId;
    }

    public String getParamKey() {
        return paramKey;
    }

    public void setParamKey(String paramKey) {
        this.paramKey = paramKey;
    }

    public String getParamVal() {
        return paramVal;
    }

    public void setParamVal(String paramVal) {
        this.paramVal = paramVal;
    }

    public String getParamDefaultVal() {
        return paramDefaultVal;
    }

    public void setParamDefaultVal(String paramDefaultVal) {
        this.paramDefaultVal = paramDefaultVal;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getProCode() {
        return proCode;
    }

    public void setProCode(String proCode) {
        this.proCode = proCode;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "DeviceParam{" +
                "pubId='" + pubId + '\'' +
                ", paramKey='" + paramKey + '\'' +
                ", paramVal='" + paramVal + '\'' +
                ", paramDefaultVal='" + paramDefaultVal + '\'' +
                ", deviceCode='" + deviceCode + '\'' +
                ", proCode='" + proCode + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.pubId);
        dest.writeString(this.paramKey);
        dest.writeString(this.paramVal);
        dest.writeString(this.paramDefaultVal);
        dest.writeString(this.deviceCode);
        dest.writeString(this.proCode);
        dest.writeLong(this.createTime);
        dest.writeLong(this.updateTime);
    }

    public DeviceParamEntity() {
    }

    protected DeviceParamEntity(Parcel in) {
        this.pubId = in.readInt();
        this.paramKey = in.readString();
        this.paramVal = in.readString();
        this.paramDefaultVal = in.readString();
        this.deviceCode = in.readString();
        this.proCode = in.readString();
        this.createTime = in.readLong();
        this.updateTime = in.readLong();
    }

    public static final Parcelable.Creator<DeviceParamEntity> CREATOR = new Parcelable.Creator<DeviceParamEntity>() {
        @Override
        public DeviceParamEntity createFromParcel(Parcel source) {
            return new DeviceParamEntity(source);
        }

        @Override
        public DeviceParamEntity[] newArray(int size) {
            return new DeviceParamEntity[size];
        }
    };
}
