package com.eyecool.intelligentcommunity.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ouyangfan on 2018/8/31.
 */

public class TcpResult implements Parcelable {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "TcpResult{" +
                "message='" + message + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
    }

    public TcpResult() {
    }

    protected TcpResult(Parcel in) {
        this.message = in.readString();
    }

    public static final Parcelable.Creator<TcpResult> CREATOR = new Parcelable.Creator<TcpResult>() {
        @Override
        public TcpResult createFromParcel(Parcel source) {
            return new TcpResult(source);
        }

        @Override
        public TcpResult[] newArray(int size) {
            return new TcpResult[size];
        }
    };
}
