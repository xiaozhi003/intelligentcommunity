package com.eyecool.intelligentcommunity.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ouyangfan on 2018/8/24.
 */

public class DeviceCameraEntity implements Parcelable {

    private String pubId;
    private String cameraName;
    private String userName;
    private String userPwd;
    private String cameraAddr;
    private String proCode;
    private String deviecCode;
    private long createTime;
    private long updateTime;
    private int cameraBand;

    public String getPubId() {
        return pubId;
    }

    public void setPubId(String pubId) {
        this.pubId = pubId;
    }

    public String getCameraName() {
        return cameraName;
    }

    public void setCameraName(String cameraName) {
        this.cameraName = cameraName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getCameraAddr() {
        return cameraAddr;
    }

    public void setCameraAddr(String cameraAddr) {
        this.cameraAddr = cameraAddr;
    }

    public String getProCode() {
        return proCode;
    }

    public void setProCode(String proCode) {
        this.proCode = proCode;
    }

    public String getDeviecCode() {
        return deviecCode;
    }

    public void setDeviecCode(String deviecCode) {
        this.deviecCode = deviecCode;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public int getCameraBand() {
        return cameraBand;
    }

    public void setCameraBand(int cameraBand) {
        this.cameraBand = cameraBand;
    }

    @Override
    public String toString() {
        return "DeviceCamera{" +
                "pubId='" + pubId + '\'' +
                ", cameraName='" + cameraName + '\'' +
                ", userName='" + userName + '\'' +
                ", userPwd='" + userPwd + '\'' +
                ", cameraAddr='" + cameraAddr + '\'' +
                ", proCode='" + proCode + '\'' +
                ", deviecCode='" + deviecCode + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", cameraBand=" + cameraBand +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.pubId);
        dest.writeString(this.cameraName);
        dest.writeString(this.userName);
        dest.writeString(this.userPwd);
        dest.writeString(this.cameraAddr);
        dest.writeString(this.proCode);
        dest.writeString(this.deviecCode);
        dest.writeLong(this.createTime);
        dest.writeLong(this.updateTime);
        dest.writeInt(this.cameraBand);
    }

    public DeviceCameraEntity() {
    }

    protected DeviceCameraEntity(Parcel in) {
        this.pubId = in.readString();
        this.cameraName = in.readString();
        this.userName = in.readString();
        this.userPwd = in.readString();
        this.cameraAddr = in.readString();
        this.proCode = in.readString();
        this.deviecCode = in.readString();
        this.createTime = in.readLong();
        this.updateTime = in.readLong();
        this.cameraBand = in.readInt();
    }

    public static final Parcelable.Creator<DeviceCameraEntity> CREATOR = new Parcelable.Creator<DeviceCameraEntity>() {
        @Override
        public DeviceCameraEntity createFromParcel(Parcel source) {
            return new DeviceCameraEntity(source);
        }

        @Override
        public DeviceCameraEntity[] newArray(int size) {
            return new DeviceCameraEntity[size];
        }
    };
}
