package com.eyecool.intelligentcommunity.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

/**
 * Created by ouyangfan on 2018/8/27.
 */

public class FeatureEntry implements Parcelable {

    private byte[] feature;
    private String peopleCode;//关联user表的pubId
    private String faceCode;

    public byte[] getFeature() {
        return feature;
    }

    public void setFeature(byte[] feature) {
        this.feature = feature;
    }

    public String getPeopleCode() {
        return peopleCode;
    }

    public void setPeopleCode(String peopleCode) {
        this.peopleCode = peopleCode;
    }

    public String getFaceCode() {
        return faceCode;
    }

    public void setFaceCode(String faceCode) {
        this.faceCode = faceCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FeatureEntry entry = (FeatureEntry) o;
        return Objects.equals(peopleCode, entry.peopleCode) &&
            Objects.equals(faceCode, entry.faceCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(peopleCode, faceCode);
    }

    @Override
    public String toString() {
        return "FeatureEntry{" +
                ", peopleCode='" + peopleCode + '\'' +
                ", faceCode='" + faceCode + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByteArray(this.feature);
        dest.writeString(this.peopleCode);
        dest.writeString(this.faceCode);
    }

    public FeatureEntry() {
    }

    protected FeatureEntry(Parcel in) {
        this.feature = in.createByteArray();
        this.peopleCode = in.readString();
        this.faceCode = in.readString();
    }

    public static final Parcelable.Creator<FeatureEntry> CREATOR = new Parcelable.Creator<FeatureEntry>() {
        @Override
        public FeatureEntry createFromParcel(Parcel source) {
            return new FeatureEntry(source);
        }

        @Override
        public FeatureEntry[] newArray(int size) {
            return new FeatureEntry[size];
        }
    };
}
