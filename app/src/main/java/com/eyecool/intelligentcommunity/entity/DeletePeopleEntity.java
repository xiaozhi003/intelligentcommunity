package com.eyecool.intelligentcommunity.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ouyangfan on 2018/8/30.
 */

public class DeletePeopleEntity implements Parcelable {

    private int taskId;
    private String recordId;
    private String tableName;

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public String toString() {
        return "DeletePeopleEntity{" +
                "taskId=" + taskId +
                ", recordId='" + recordId + '\'' +
                ", tableName='" + tableName + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.taskId);
        dest.writeString(this.recordId);
        dest.writeString(this.tableName);
    }

    public DeletePeopleEntity() {
    }

    protected DeletePeopleEntity(Parcel in) {
        this.taskId = in.readInt();
        this.recordId = in.readString();
        this.tableName = in.readString();
    }

    public static final Parcelable.Creator<DeletePeopleEntity> CREATOR = new Parcelable.Creator<DeletePeopleEntity>() {
        @Override
        public DeletePeopleEntity createFromParcel(Parcel source) {
            return new DeletePeopleEntity(source);
        }

        @Override
        public DeletePeopleEntity[] newArray(int size) {
            return new DeletePeopleEntity[size];
        }
    };
}
