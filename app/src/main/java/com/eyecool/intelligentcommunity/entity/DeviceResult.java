package com.eyecool.intelligentcommunity.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;

/**
 * Created by ouyangfan on 2018/8/24.
 */

public class DeviceResult implements Parcelable {

    private DeviceCameraEntity[] deviceCameraList;
    private DeviceParamEntity[] deviceParamList;
    private DeviceEntity device;

    public DeviceCameraEntity[] getDeviceCameraList() {
        return deviceCameraList;
    }

    public void setDeviceCameraList(DeviceCameraEntity[] deviceCameraList) {
        this.deviceCameraList = deviceCameraList;
    }

    public DeviceParamEntity[] getDeviceParamList() {
        return deviceParamList;
    }

    public void setDeviceParamList(DeviceParamEntity[] deviceParamList) {
        this.deviceParamList = deviceParamList;
    }

    public DeviceEntity getDevice() {
        return device;
    }

    public void setDevice(DeviceEntity device) {
        this.device = device;
    }

    @Override
    public String toString() {
        return "DeviceResult{" +
                "deviceCameraList=" + Arrays.toString(deviceCameraList) +
                ", deviceParamList=" + Arrays.toString(deviceParamList) +
                ", device=" + device +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedArray(this.deviceCameraList, flags);
        dest.writeTypedArray(this.deviceParamList, flags);
        dest.writeParcelable(this.device, flags);
    }

    public DeviceResult() {
    }

    protected DeviceResult(Parcel in) {
        this.deviceCameraList = in.createTypedArray(DeviceCameraEntity.CREATOR);
        this.deviceParamList = in.createTypedArray(DeviceParamEntity.CREATOR);
        this.device = in.readParcelable(DeviceEntity.class.getClassLoader());
    }

    public static final Parcelable.Creator<DeviceResult> CREATOR = new Parcelable.Creator<DeviceResult>() {
        @Override
        public DeviceResult createFromParcel(Parcel source) {
            return new DeviceResult(source);
        }

        @Override
        public DeviceResult[] newArray(int size) {
            return new DeviceResult[size];
        }
    };
}
