package com.eyecool.intelligentcommunity.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ouyangfan on 2018/8/24.
 */

public class DeviceEntity implements Parcelable {
    private String userName;
    private String deviceAlias;
    private int isPublic;
    private String deviceIp;
    private String deviceAddr;
    private String userPwd;
    private int devicePort;
    private String deviceName;
    private int deviceOs;
    private String nodeId;
    private int deviceType;
    private String createTime;
    private String updateTime;
    private String proCode;
    private String creator;
    private String pubId;
    private String operationCode;
    private String authorizationCode;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDeviceAlias() {
        return deviceAlias;
    }

    public void setDeviceAlias(String deviceAlias) {
        this.deviceAlias = deviceAlias;
    }

    public int getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(int isPublic) {
        this.isPublic = isPublic;
    }

    public String getDeviceIp() {
        return deviceIp;
    }

    public void setDeviceIp(String deviceIp) {
        this.deviceIp = deviceIp;
    }

    public String getDeviceAddr() {
        return deviceAddr;
    }

    public void setDeviceAddr(String deviceAddr) {
        this.deviceAddr = deviceAddr;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public int getDevicePort() {
        return devicePort;
    }

    public void setDevicePort(int devicePort) {
        this.devicePort = devicePort;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public int getDeviceOs() {
        return deviceOs;
    }

    public void setDeviceOs(int deviceOs) {
        this.deviceOs = deviceOs;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getProCode() {
        return proCode;
    }

    public void setProCode(String proCode) {
        this.proCode = proCode;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getPubId() {
        return pubId;
    }

    public void setPubId(String pubId) {
        this.pubId = pubId;
    }

    public String getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    @Override
    public String toString() {
        return "DeviceEntity{" +
            "userName='" + userName + '\'' +
            ", deviceAlias='" + deviceAlias + '\'' +
            ", isPublic=" + isPublic +
            ", deviceIp='" + deviceIp + '\'' +
            ", deviceAddr='" + deviceAddr + '\'' +
            ", userPwd='" + userPwd + '\'' +
            ", devicePort=" + devicePort +
            ", deviceName='" + deviceName + '\'' +
            ", deviceOs=" + deviceOs +
            ", nodeId='" + nodeId + '\'' +
            ", deviceType=" + deviceType +
            ", createTime='" + createTime + '\'' +
            ", updateTime='" + updateTime + '\'' +
            ", proCode='" + proCode + '\'' +
            ", creator='" + creator + '\'' +
            ", pubId='" + pubId + '\'' +
            ", operationCode='" + operationCode + '\'' +
            ", authorizationCode='" + authorizationCode + '\'' +
            '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.userName);
        dest.writeString(this.deviceAlias);
        dest.writeInt(this.isPublic);
        dest.writeString(this.deviceIp);
        dest.writeString(this.deviceAddr);
        dest.writeString(this.userPwd);
        dest.writeInt(this.devicePort);
        dest.writeString(this.deviceName);
        dest.writeInt(this.deviceOs);
        dest.writeString(this.nodeId);
        dest.writeInt(this.deviceType);
        dest.writeString(this.createTime);
        dest.writeString(this.updateTime);
        dest.writeString(this.proCode);
        dest.writeString(this.creator);
        dest.writeString(this.pubId);
        dest.writeString(this.operationCode);
        dest.writeString(this.authorizationCode);
    }

    public DeviceEntity() {
    }

    protected DeviceEntity(Parcel in) {
        this.userName = in.readString();
        this.deviceAlias = in.readString();
        this.isPublic = in.readInt();
        this.deviceIp = in.readString();
        this.deviceAddr = in.readString();
        this.userPwd = in.readString();
        this.devicePort = in.readInt();
        this.deviceName = in.readString();
        this.deviceOs = in.readInt();
        this.nodeId = in.readString();
        this.deviceType = in.readInt();
        this.createTime = in.readString();
        this.updateTime = in.readString();
        this.proCode = in.readString();
        this.creator = in.readString();
        this.pubId = in.readString();
        this.operationCode = in.readString();
        this.authorizationCode = in.readString();
    }

    public static final Parcelable.Creator<DeviceEntity> CREATOR = new Parcelable.Creator<DeviceEntity>() {
        @Override
        public DeviceEntity createFromParcel(Parcel source) {
            return new DeviceEntity(source);
        }

        @Override
        public DeviceEntity[] newArray(int size) {
            return new DeviceEntity[size];
        }
    };
}
