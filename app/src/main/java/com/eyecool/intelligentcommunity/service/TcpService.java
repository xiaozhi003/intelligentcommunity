package com.eyecool.intelligentcommunity.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.eyecool.intelligentcommunity.MyApp;
import com.eyecool.intelligentcommunity.dao.DaoManager;
import com.eyecool.intelligentcommunity.dao.FilterRecordEntity;
import com.eyecool.intelligentcommunity.dao.RecordEntity;
import com.eyecool.intelligentcommunity.entity.TcpResult;
import com.eyecool.intelligentcommunity.manager.ApkUpdateManager;
import com.eyecool.intelligentcommunity.model.NetworkDataProcessing;
import com.eyecool.intelligentcommunity.utils.Constant;
import com.eyecool.intelligentcommunity.utils.LogSaveUtil;
import com.eyecool.library.serversocket.SocketThread;
import com.eyecool.library.serversocket.TcpServer;
import com.eyecool.library.utils.GsonUtils;
import com.eyecool.library.utils.Logs;

import java.util.ArrayList;
import java.util.List;

public class TcpService extends Service {
    
    private static final String TAG = TcpService.class.getSimpleName();

    private TcpServer mTcpServer;
    private DaoManager mDaoManager;
    private NetworkDataProcessing mNetworkDataProcessing;

    public TcpService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mDaoManager = new DaoManager(this);
        mNetworkDataProcessing = NetworkDataProcessing.getInstance();
        mTcpServer = new TcpServer(8888);
        mTcpServer.setReceiveMessage(new SocketThread.IReceiveMessage() {
            @Override
            public void receiverMessage(byte[] bytes) {
                String data = new String(bytes);
                TcpResult tcpResult = GsonUtils.fromJson(data, TcpResult.class);
                if (tcpResult != null) {
                    String message = tcpResult.getMessage();
                    Logs.e(TAG,"message: " + message);
                    switch (message) {
                        case "60000"://立即删除人员数据
                            mNetworkDataProcessing.deletePeople("pro_people");
                            break;
                        case "60001"://立即删除人脸数据
                            mNetworkDataProcessing.deletePeople("pro_face_feature");
                            break;
                        case "60002"://立即更新人员数据
                            mNetworkDataProcessing.getUser(null);
                            break;
                        case "60003"://立即更新人脸数据
                            mNetworkDataProcessing.getFeature(null);
                            break;
                        case "60004"://立即更新红黑名单数据
                            mNetworkDataProcessing.getSpecialUser(null);
                            break;
                        case "60005"://立即更新设备参数数据
                            mNetworkDataProcessing.getDevicesParam(Constant.sDeviceCode, null);
                            break;
                        case "60006"://立即上传离线识别数据
                            //正常识别
                            List<RecordEntity> list = mDaoManager.getRecordEntities();
                            if (list != null && list.size() > 0) {
                                mNetworkDataProcessing.uploadRecord((ArrayList<RecordEntity>) list);
                            }
                            //红黑名单识别
                            List<FilterRecordEntity> list1 = mDaoManager.getFilterRecordEntityEntities();
                            if (list1 != null && list1.size() > 0) {
                                mNetworkDataProcessing.uploadFilterRecord((ArrayList<FilterRecordEntity>) list1);
                            }
                            break;
                        case "60007"://立即上传设备监控状态数据
                            break;
                        case "60008"://立即删除红黑名单数据
                            mNetworkDataProcessing.deletePeople("pro_filter_list");
                            break;
                        case "60009"://恢复设备数据
                            mDaoManager.clearAllTable();
                            Constant.sFeatureEntries.clear();
                            mNetworkDataProcessing.getUser(null);
                            break;
                        case "60010"://删除数据
                            mDaoManager.clearAllTable();
                            break;
                        case "70000"://设备更新
                            LogSaveUtil.d(TAG, "___________设备应用检查更新__________");
                            MyApp.runUITask(new Runnable() {
                                @Override
                                public void run() {
                                    ApkUpdateManager.getInstance().getUpdata();
                                }
                            });
                            break;
                    }
                }
            }
        });
        mTcpServer.start();
    }
}
