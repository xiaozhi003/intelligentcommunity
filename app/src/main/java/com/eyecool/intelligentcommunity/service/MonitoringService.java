package com.eyecool.intelligentcommunity.service;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

import com.eyecool.intelligentcommunity.activity.MainActivity;
import com.eyecool.intelligentcommunity.utils.CheckUtil;
import com.eyecool.intelligentcommunity.utils.Constant;
import com.eyecool.intelligentcommunity.utils.SystemUtil;
import com.eyecool.library.utils.Logs;
import com.eyecool.library.utils.SharedPreferencesUtils;
import com.eyecool.library.utils.TimeUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by xiaozhi on 2018/11/22 :)
 * 检测APP页面是否一直运行,不运行就直接启动
 */
public class MonitoringService extends Service {

    private final static String TAG = MonitoringService.class.getSimpleName();

    private static final String APP_PACKAGE_NAME = "com.eyecool.intelligentcommunity";

    // 关机重启起始时间
    public String mRebootStartTime = "02:00:00";
    // 关机重启结束时间
    public String mRebootEndTime = "02:10:00";

    private boolean isRebooted = false;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ("kill_self".equals(intent.getAction())) {
                Logs.e(TAG, "onReceive:杀死自己的进程！");
                killMyselfPid(); // 杀死自己的进程
            }
        }
    };

    private Timer timer = new Timer();
    private TimerTask task = new TimerTask() {
        @Override
        public void run() {
            checkIsAlive();

            checkIsRebooted();
        }
    };

    /**
     * 检测应用是否活着
     */
    private void checkIsAlive() {
        String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                Locale.CHINA).format(new Date());
        Logs.i(TAG, "Service Run: " + format);

        boolean AIsRunning = CheckUtil.isClsRunning(
                MonitoringService.this, APP_PACKAGE_NAME, APP_PACKAGE_NAME + ".activity.MainActivity");
        boolean BIsRunning = CheckUtil.isClsRunning(
                MonitoringService.this, APP_PACKAGE_NAME, APP_PACKAGE_NAME + ".activity.SettingActivity");
        boolean CIsRunning = CheckUtil.isClsRunning(
                MonitoringService.this, APP_PACKAGE_NAME, APP_PACKAGE_NAME + ".activity.UpdateApkActivity");
        boolean isRunning = (AIsRunning || BIsRunning);

        Logs.d(TAG, "AIsRunning || BIsRunning is running:" + isRunning);

        if (!isRunning) {
            if (!isRunning) { //如果界面挂掉直接启动AActivity
                if (!CIsRunning) {
                    Intent intent = new Intent(this, MainActivity.class);
                    @SuppressLint("WrongConstant") PendingIntent restartIntent = PendingIntent.getActivity(this, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
                    AlarmManager mgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
                    mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, restartIntent);
                }
            }
        }
    }

    /**
     * 检测设备是否在规定时间段重启过
     */
    private void checkIsRebooted() {
        // 2:00到3:00重启设备
        String currentTime = TimeUtil.getCurrentTime("HH:mm:ss");
        try {
            if (!TimeUtil.compareTime(currentTime, mRebootStartTime) && TimeUtil.compareTime(currentTime, mRebootEndTime)) { // 设备重启时间段
                Logs.i(TAG, "设备重启时段...");
                if (!isRebooted) {
                    SharedPreferencesUtils.saveBoolean(this, Constant.sharedPreferencesName, SharedPreferencesUtils.KEY_IS_REBOOTED, true);
                    SystemUtil.restartDevice();
                } else {
                    Logs.i(TAG, "设备在重启时段已重启...");
                }
            } else { // 设备非重启时间段,重置是否重启状态
                if (isRebooted) {
                    Logs.i(TAG, "重置重启状态...");
                    SharedPreferencesUtils.saveBoolean(this, Constant.sharedPreferencesName, SharedPreferencesUtils.KEY_IS_REBOOTED, false);
                    isRebooted = false;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Logs.e(TAG, "onCreate: 启动监控服务! ");
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("kill_self");
        registerReceiver(broadcastReceiver, intentFilter);
        timer.schedule(task, 0, 10000);// 设置检测的时间周期(毫秒数)
        isRebooted = SharedPreferencesUtils.getBoolean(this, Constant.sharedPreferencesName, SharedPreferencesUtils.KEY_IS_REBOOTED, false);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    /**
     * 杀死自身的进程
     */
    private void killMyselfPid() {
        int pid = android.os.Process.myPid();
        String command = "kill -9 " + pid;
        Logs.e(TAG, "killMyselfPid: " + command);
        stopService(new Intent(MonitoringService.this, MonitoringService.class));
        try {
            Runtime.getRuntime().exec(command);
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
        if (task != null) {
            task.cancel();
        }
        if (timer != null) {
            timer.cancel();
        }
    }
}

