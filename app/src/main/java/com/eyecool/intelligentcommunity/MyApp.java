package com.eyecool.intelligentcommunity;

import android.os.Handler;
import android.os.Looper;
import android.support.v8.renderscript.RenderScript;

import com.eyecool.intelligentcommunity.model.ZHTDoor;
import com.eyecool.intelligentcommunity.utils.Constant;
import com.eyecool.library.BaseApplication;
import com.eyecool.library.algorithm.face.five003.Abstract5003;
import com.eyecool.library.utils.FileUtils;
import com.eyecool.library.utils.Logs;
import com.eyecool.library.utils.SharedPreferencesUtils;
import com.eyecool.library.utils.SystemUtils;

import org.apache.log4j.Level;

import de.mindpipe.android.logging.log4j.LogConfigurator;

/**
 * Created by ouyangfan on 2018/8/22.
 */

public class MyApp extends BaseApplication {

    public static ZHTDoor mDoor;
    private static Handler mHandler = new Handler(Looper.getMainLooper());
    //    public static Soft5003 mFaceAlgorithm;
    public static Abstract5003 mFaceAlgorithm;
    private static RenderScript mRs;
    public static String sRunCode = "";
    public static void runUITask(Runnable run) {
        mHandler.post(run);
    }
    public static int  screenWidth, screenHeight;

    /**
     * 是否是软授权，true：软授权；false：硬授权
     */
    public static boolean isSoft = true;

    @Override
    public void onCreate() {
        super.onCreate();
        mRs = RenderScript.create(this);
//        LeakCanary.install(this);

        FileUtils.makeDirs(SystemUtils.getRootPath() + "/temp/");

        MyApp.isSoft = SharedPreferencesUtils.getBoolean(this, Constant.sharedPreferencesName, SharedPreferencesUtils.KEY_SOFT, true);
        Constant.sVolume = SharedPreferencesUtils.getInt(this, Constant.sharedPreferencesName, SharedPreferencesUtils.KEY_VOLUME, 50);
        Constant.sGPIODelay = SharedPreferencesUtils.getInt(this, Constant.sharedPreferencesName, SharedPreferencesUtils.KEY_GPIO_DELAY, 300);
        Constant.isSaveFailePic = SharedPreferencesUtils.getBoolean(this, Constant.sharedPreferencesName, SharedPreferencesUtils.KEY_IS_SAVE_FAILE_PIC, false);
        initCrashHandle();
        initLog4j();
    }

    private void initCrashHandle() {
        Logs.v("MyApp");
        CrashHandle handle = CrashHandle.getInstance();
        handle.init(getApplicationContext());
        handle.sendPreviousReportsToServer();
    }

    //运行日志保存到本地
    private void initLog4j() {
        String logDir = FileUtils.getLogPath(getApplicationContext());
        LogConfigurator logConfigurator = new LogConfigurator();
        logConfigurator.setFileName(logDir + "/log.txt");
        logConfigurator.setRootLevel(Level.ALL);
        logConfigurator.setLevel("org.apache", Level.ALL);
        logConfigurator.setFilePattern("%d %-5p [%c{2}]-[%L] %m%n");
        logConfigurator.setMaxFileSize(2 * 1024 * 1024);
        logConfigurator.setImmediateFlush(true);
        logConfigurator.setMaxBackupSize(3);
        logConfigurator.configure();
    }

    public static RenderScript getRs() {
        return mRs;
    }
}
