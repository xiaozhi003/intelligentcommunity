package com.eyecool.intelligentcommunity.exceptionhandle;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;

import com.eyecool.intelligentcommunity.utils.SystemUtil;
import com.eyecool.library.utils.Logs;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 相机异常情况处理类（卡死）
 * 红外相机40s内没有数据回调，认为相机卡死，重新启动android系统
 */
public class CameraErrorHandle {
    private final String TAG = "CameraErrorHandle";
    private Context context;
    private long lastPreviewTime = 0;
    private Timer timer = new Timer();
    private TimerTask task = new TimerTask() {
        @Override
        public void run() {
            if (!checkIRCameraIsOk()) {
                Logs.e(TAG, "IR Camera Error,Reboot System!");
                handler.sendEmptyMessage(0);
            } else {
                Logs.e(TAG, "IR Camera Normal");
            }
        }
    };

    private boolean checkIRCameraIsOk() {
        boolean cameraPrevieeIsOk = false;
        if (System.currentTimeMillis() - lastPreviewTime < 50 * 1000) {//50s相机没有预览数据回来，认为卡死
            cameraPrevieeIsOk = true;
        }
        return cameraPrevieeIsOk;
    }


    Handler handler = new Handler(){
        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
            if ("rk3399-all".equals(SystemUtil.getSystemModel())) { //YZ3399
                try{
                    Intent intent2 = new Intent(Intent.ACTION_REBOOT);
                    intent2.putExtra("nowait", 1);
                    intent2.putExtra("interval", 1);
                    intent2.putExtra("window", 0);
                    context.sendBroadcast(intent2);
                }catch (Exception e){
                    Logs.e(TAG, e.getMessage());
                }

            } else if ("rk3399-mid".equals(SystemUtil.getSystemModel())) {//JHC3399，关闭
                String cmd = "su -c reboot";
                try {
                    Runtime.getRuntime().exec(cmd);
                } catch (IOException e) {
                    Logs.e(TAG, e.getMessage());
                }
            }
        }
    };


    public void setLastPreviewTime(long lastPreviewTime) {
        this.lastPreviewTime = lastPreviewTime;
    }

    public CameraErrorHandle(Context context) {
        this.context = context;
    }

    public void startWork() {
        timer.schedule(task,60 * 1000, 50 * 1000);// 50s检查一次
    }

    public void release() {
        if (timer != null) {
            timer.cancel();
        }
    }
}
