package com.eyecool.intelligentcommunity.exceptionhandle;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.format.Time;

import com.eyecool.intelligentcommunity.utils.SystemUtil;
import com.eyecool.library.utils.Logs;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 系统后半夜2：00-3：00重启一次
 */
public class DeviceRestartHandle {
    private final String TAG = DeviceRestartHandle.class.toString();
    private Context context;
    private Timer timeerChcktime;

    public DeviceRestartHandle(Context context) {
        this.context = context;
    }

    /**
     * 检测是否在时间段内，是的话重启板子
     */
    public void startCheckTimeToReStart() {
        timeerChcktime = new Timer();
        timeerChcktime.schedule(new CheckTimeBusiness(), 60 * 60 * 1000, 60 * 60 * 1000);//延迟一小时执行,一小时执行一次
    }

    class CheckTimeBusiness extends TimerTask {
        @Override
        public void run() {
            mHanlder.sendEmptyMessage(1);
        }
    }

    Handler mHanlder = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
            if (checkTime()) {
                if ("rk3399-all".equals(SystemUtil.getSystemModel())) { //YZ3399
                    try{
                        Intent intent2 = new Intent(Intent.ACTION_REBOOT);
                        intent2.putExtra("nowait", 1);
                        intent2.putExtra("interval", 1);
                        intent2.putExtra("window", 0);
                        context.sendBroadcast(intent2);
                    }catch (Exception e){
                        Logs.e(TAG, e.getMessage());
                    }

                } else if ("rk3399-mid".equals(SystemUtil.getSystemModel())) {//JHC3399，关闭
                    String cmd = "su -c reboot";
                    try {
                        Runtime.getRuntime().exec(cmd);
                    } catch (IOException e) {
                        Logs.e(TAG, e.getMessage());
                    }
                }
            }
        }
    };

    /**
     * 当前时间在2:00到3:00之间，重启板子
     *
     * @return
     */
    private boolean checkTime() {
        boolean result = false;
        final long aDayInMillis = 1000 * 60 * 60 * 24;
        final long currentTimeMillis = System.currentTimeMillis();
        Time now = new Time();
        now.set(currentTimeMillis);
        Time startTime = new Time();
        startTime.set(currentTimeMillis);
        startTime.hour = 2;
        startTime.minute = 00;
        Time endTime = new Time();
        endTime.set(currentTimeMillis);
        endTime.hour = 3;
        endTime.minute = 00;

        if (!startTime.before(endTime)) {
            // 跨天的特殊情况（比如22:00-8:00）
            startTime.set(startTime.toMillis(true) - aDayInMillis);
            result = !now.before(startTime) && !now.after(endTime); // startTime <= now <= endTime
            Time startTimeInThisDay = new Time();
            startTimeInThisDay.set(startTime.toMillis(true) + aDayInMillis);
            if (!now.before(startTimeInThisDay)) {
                result = true;
            }
        } else {
            // 普通情况(比如 8:00 - 14:00)
            result = !now.before(startTime) && !now.after(endTime); // startTime <= now <= endTime
        }
        return result;
    }

}
