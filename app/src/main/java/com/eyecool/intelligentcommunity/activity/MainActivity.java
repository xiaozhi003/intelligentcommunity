package com.eyecool.intelligentcommunity.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Power;
import android.os.PowerManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ChlFaceSdk.ChlFaceSdk;
import com.eyecool.intelligentcommunity.eventmsg.EventMsg;
import com.eyecool.intelligentcommunity.manager.CameraManager;
import com.eyecool.intelligentcommunity.MyApp;
import com.eyecool.intelligentcommunity.R;
import com.eyecool.intelligentcommunity.model.NetworkDataProcessing;
import com.eyecool.intelligentcommunity.model.ZHTDoor;
import com.eyecool.intelligentcommunity.presenter.IInitPresenter;
import com.eyecool.intelligentcommunity.presenter.InitPresenterImpl;
import com.eyecool.intelligentcommunity.presenter.RecognitionPresenter;
import com.eyecool.intelligentcommunity.receiver.NetworkChangeReceiver;
import com.eyecool.intelligentcommunity.service.MonitoringService;
import com.eyecool.intelligentcommunity.service.TcpService;
import com.eyecool.intelligentcommunity.utils.CheckUtil;
import com.eyecool.intelligentcommunity.utils.Constant;
import com.eyecool.intelligentcommunity.utils.GpioJhc3399;
import com.eyecool.intelligentcommunity.utils.GpioManager;
import com.eyecool.intelligentcommunity.utils.LogSaveUtil;
import com.eyecool.intelligentcommunity.utils.SystemUtil;
import com.eyecool.intelligentcommunity.utils.WhiteLightManager;
import com.eyecool.intelligentcommunity.view.CameraSurfaceView;
import com.eyecool.intelligentcommunity.view.CustomToast;
import com.eyecool.intelligentcommunity.view.IInitView;
import com.eyecool.intelligentcommunity.view.IRecognitionView;
import com.eyecool.library.activity.BaseActivity;
import com.eyecool.library.algorithm.face.five003.Soft5003;
import com.eyecool.library.utils.FileUtils;
import com.eyecool.library.utils.Logs;
import com.eyecool.library.utils.SharedPreferencesUtils;
import com.eyecool.library.utils.SystemUtils;
import com.eyecool.library.utils.TimeUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * 识别界面，本来初始化、广告界面显示在另一个界面，故这里需要实现两个view的接口，以便于后期维护
 */

public class MainActivity extends BaseActivity implements IInitView, IRecognitionView, CameraManager.ICameraPreviewListener, SurfaceHolder.Callback, View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private InitPresenterImpl mInitPresenterImpl;
    //初始化是否完成
    private boolean isInitFinish = false;
    private RecognitionPresenter mRecognitionPresenter;
    private LinearLayout mPreviewLL;
    //普通摄像头预览
    private CameraSurfaceView mMainSurfaceView;
    private SurfaceView mInfraredSurfaceView;
    //黑白摄像头预览
    private SurfaceHolder mSurfaceHolder;
    private SurfaceHolder mInfraredSurfaceHolder;
    //广告
    private ImageView mImgAdv;
    //日期
    private String mDate;
    private TextView mTextDate;
    //等待条
    private ProgressBar mProgressBar;
    //扫描
    private ImageView mImageScan;
    //设置
    private ImageView mImageSetting;
    //名字和欢迎语
    private TextView mTextName, mTextWelcome;
    //监听日期变化
    private DataChangeReceiver mDataChangeReceiver;
    //监听网络变化
    private NetworkChangeReceiver networkChangeReceiver;
    //请求码
    private static final int REQUEST_CODE_SETTING = 1;
    //是否显示了dialog
    private boolean isShowDialog = false;
    private boolean isPause;
    private Intent mTcpServiceIntent;
    //屏幕常亮
    private PowerManager powerManager = null;
    private PowerManager.WakeLock wakeLock = null;

    private boolean isRegisterEvent = false;//是否注册了eventbus

    private Handler handler = new Handler();

    private CustomToast mCustomToast;
    private long lastShowToastTime;
    private boolean lastShowToastType;
    private TextView mTvStartTime;
    private TextView mTvRestartNum;
    private Button mBtnStartTest;
    private Button mBtnStopTest;
    private TextView mTvTestContent;
    private Button mBtnHome;

    class DataChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Logs.e(TAG, "日期发生变化了");
            if (intent.getAction().equals(Intent.ACTION_DATE_CHANGED)) {
                mDate = TimeUtil.getCurrentMonth() + "月";
                mDate = mDate + TimeUtil.getCurrentDay() + "日 | ";
                mDate = mDate + TimeUtil.getWeek();
                mTextDate.setText(mDate);
            }
        }
    }

    @Override
    public void initData() {
        NetworkDataProcessing.getInstance().init(getApplicationContext(), this);
        MyApp.sRunCode = ChlFaceSdk.GetRunCode(this);
        LogSaveUtil.d(TAG, "初始化数据，设备算法运行码:" + MyApp.sRunCode);
        //获取设备编号
        String deviceCode = SharedPreferencesUtils.getString(this, Constant.sharedPreferencesName, "deviceCode");
        LogSaveUtil.d(TAG, "数据库读取设备编号:" + deviceCode);
        if (!TextUtils.isEmpty(deviceCode)) {
            Constant.sDeviceCode = deviceCode;
        }
        //设置屏幕常亮
        powerManager = (PowerManager) this.getSystemService(this.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My Lock");
        wakeLock.acquire();
        //设置音量
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int max = audioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
        audioManager.setStreamVolume(AudioManager.STREAM_ALARM, max, AudioManager.FLAG_PLAY_SOUND);
        //日期
        mDate = TimeUtil.getCurrentMonth() + "月";
        mDate = mDate + TimeUtil.getCurrentDay() + "日 | ";
        mDate = mDate + TimeUtil.getWeek();
        //tcp服务
        mTcpServiceIntent = new Intent(this, TcpService.class);
        startService(mTcpServiceIntent);
        //开门操作
        MyApp.mDoor = ZHTDoor.getInstance();
        MyApp.mDoor.openCameraLight();
        MyApp.mDoor.closeLight();
        //白灯
        WhiteLightManager.handleFaceCameraLight(this);
        mInitPresenterImpl = new InitPresenterImpl(this, this);
        mRecognitionPresenter = new RecognitionPresenter(this, this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        networkChangeReceiver = new NetworkChangeReceiver();
        registerReceiver(networkChangeReceiver, intentFilter);
        IntentFilter intentFilter1 = new IntentFilter();
        intentFilter1.addAction(Intent.ACTION_DATE_CHANGED);
        mDataChangeReceiver = new DataChangeReceiver();
        registerReceiver(mDataChangeReceiver, intentFilter1);
        // 启动监控服务
        if (!CheckUtil.isServiceWorked(this, "com.eyecool.intelligentcommunity.service." + MonitoringService.class.getSimpleName())) {
            Intent intent = new Intent(MainActivity.this, MonitoringService.class);
            intent.setAction("android.intent.action.RESPOND_VIA_MESSAGE");
            startService(intent);
        }
        registerEventBus();
        // 初始化门禁，gpio控制开关门（关门）
        if ("rk3399-all".equals(SystemUtil.getSystemModel())) { //YZ3399
            GpioManager.close();
        } else if ("rk3288".equals(SystemUtil.getSystemModel())) { //rk3388 new
            Power.set_zysj_gpio_value(4, 0);
        } else if ("rk3399-mid".equals(SystemUtil.getSystemModel())) {//JHC3399，关闭
            GpioJhc3399.init();
            GpioJhc3399.setValue(85, 0);
        }
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        hideStatusNavigationBar();
        setContentView(R.layout.activity_main);

        //日期
        mTextDate = findViewById(R.id.text_date);
        mTextDate.setText(mDate);
        //普通摄像头预览画面
        mMainSurfaceView = findViewById(R.id.surface_main);
        mSurfaceHolder = mMainSurfaceView.getHolder();
        mSurfaceHolder.setFormat(PixelFormat.TRANSPARENT);
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mSurfaceHolder.addCallback(this);
        //红外摄像头预览画面，肉眼不可见
        mInfraredSurfaceView = findViewById(R.id.surface_main_infrared);
        mInfraredSurfaceHolder = mInfraredSurfaceView.getHolder();
        mInfraredSurfaceHolder.setFormat(PixelFormat.TRANSPARENT);
        mInfraredSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mInfraredSurfaceHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                Logs.e(TAG, "InfraredSurfaceCreated");
                mInitPresenterImpl.openInfraredCamera();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                Logs.e(TAG, "InfraredSurfaceChanged");
                mInitPresenterImpl.setInfraredPreview(mInfraredSurfaceHolder, mICameraPreviewListener);
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                Logs.e(TAG, "InfraredSurfaceDestroyed");
            }
        });
        // 预览框
        mPreviewLL = findViewById(R.id.previewLL);
        //广告图片
        mImgAdv = findViewById(R.id.img_main_adv);
        mImgAdv.setOnClickListener(this);
        //设置
        mImageSetting = findViewById(R.id.image_logo_setting);
        mImageSetting.setOnClickListener(this);
//        mTextName = findViewById(R.id.text_main_name);
        mTextWelcome = findViewById(R.id.text_main_welcome);
        //等待条
        mProgressBar = findViewById(R.id.progress_main);
        //扫描
        mImageScan = findViewById(R.id.img_recog_scan);
        TranslateAnimation animation = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 1.0f
        );
        animation.setRepeatCount(-1);
        animation.setDuration(3000);
        if (!isScreenLandscape()) {
            mImageScan.startAnimation(animation);
        } else {
            mImageScan.setVisibility(View.GONE);
        }

//        restartRemind();
        mBtnStartTest = (Button) findViewById(R.id.btn_start_test);
        mBtnStopTest = (Button) findViewById(R.id.btn_stop_test);
        mTvTestContent = (TextView) findViewById(R.id.tv_test_content);
        mTvStartTime = (TextView) findViewById(R.id.tv_start_time);
        mTvRestartNum = (TextView) findViewById(R.id.tv_restart_num);
        mBtnStartTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //开始测试重启问题

            }
        });
        mBtnStopTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //终止测试

            }
        });

        mBtnHome = (Button) findViewById(R.id.btn_home);
        mBtnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addCategory(Intent.CATEGORY_HOME);
                startActivity(intent);
            }
        });
        findViewById(R.id.btn_delete_record).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread() {
                    @Override
                    public void run() {
                        mRecognitionPresenter.deleteAllRecord();
                        Logs.e(TAG, "xxxxxxx: " + "删除成功");
                    }
                }.start();
            }
        });

        // 初始化算法
        LogSaveUtil.d(TAG, "开始初始化算法");
        mInitPresenterImpl.init(this);
    }

    //红外摄像头预览数据回调
    CameraManager.ICameraPreviewListener mICameraPreviewListener = new CameraManager.ICameraPreviewListener() {
        @Override
        public void onPreviewFrame(Bitmap bitmap) {
            Logs.e(TAG, "IR data Bitmap callback");
            mRecognitionPresenter.detectBWFace(bitmap);
        }

        @Override
        public void onPreviewYuv(byte[] yuv) {
            Logs.e(TAG, "IR data yuv callback");
            mRecognitionPresenter.detectBWFace(yuv);
        }
    };

    @Override
    public void loadData() {
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Logs.e(TAG, "surfaceCreated");
        mInitPresenterImpl.openColorCamera();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Logs.e(TAG, "surfaceChanged");
        mInitPresenterImpl.setColorPreview(mSurfaceHolder, MainActivity.this);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Logs.e(TAG, "surfaceDestroyed");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logs.i(TAG, "onResume...");
        hideStatusNavigationBar();
        isPause = false;
        mRecognitionPresenter.onResume();

        mInitPresenterImpl.openColorCamera();
        mInitPresenterImpl.openInfraredCamera();
        mInitPresenterImpl.setColorPreview(mSurfaceHolder, MainActivity.this);
        mInitPresenterImpl.setInfraredPreview(mInfraredSurfaceHolder, mICameraPreviewListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Logs.i(TAG, "onPause...");
        isPause = true;
        mInitPresenterImpl.release();
        mRecognitionPresenter.onPause();
    }

    @Override
    protected void onDestroy() {
        wakeLock.release();
        MyApp.mDoor.closeCameraLight();
        mInitPresenterImpl.release();
        unregisterReceiver(networkChangeReceiver);
        unregisterReceiver(mDataChangeReceiver);
        if (mTcpServiceIntent != null) {
            stopService(mTcpServiceIntent);
        }
        //关闭补光灯
        MyApp.mDoor.closeFaceCameraLight();
        unregisterEvent();
        super.onDestroy();
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }

    //创建对话框
    private void createDialog(String title, String message, String positive, String negative, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positive, onClickListener);
        builder.setNegativeButton(negative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        isShowDialog = true;
        builder.show();
    }

    @Override
    public void showPermissionDialog(String message) {
        Logs.e(TAG, "显示权限提示框");
        createDialog("温馨提示", "没有照相机权限，请开启照相机权限", "确定", "取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        List<String> permissions = new ArrayList<>();
                        addPermission(permissions, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                        addPermission(permissions, Manifest.permission.READ_EXTERNAL_STORAGE);
                        addPermission(permissions, Manifest.permission.CAMERA);
                        requestPermission(permissions);
                        dialog.dismiss();
                        isShowDialog = false;
                    }
                };
            }
        });
    }

    @Override
    public void showAlgorithmDialog(final int type) {
        Logs.e(TAG, "显示算法提示框");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (type) {
                    case IInitPresenter.ALGORITHM_TYPE_INPUT://软授权算法注册码输入框
                        mProgressBar.setVisibility(View.GONE);
                        String runCode = mInitPresenterImpl.getRunCode();
                        MyApp.sRunCode = runCode;
                        setRegisterCode(runCode);
                        break;
                    case IInitPresenter.ALGORITHM_TYPE_ERROR://没有插算法硬狗
                        createDialog("温馨提示", "请插入算法硬狗后再打开App", "确定", "取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                isShowDialog = false;
                                finish();
                            }
                        });
                        break;
                    case IInitPresenter.ALGORITHM_TYPE_FAILED://算法初始化失败
                        createDialog("温馨提示", "初始化算法失败", "确定", "取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                isShowDialog = false;
                                finish();
                            }
                        });
                        break;
                }
            }
        });
    }

    @Override
    public void initComplete() {
        mProgressBar.setVisibility(View.GONE);
        isInitFinish = true;
    }

    @Override
    public void onRegCode(String regCode) {
        if (MyApp.mFaceAlgorithm == null || MyApp.mFaceAlgorithm.isInit()) {//假如已经初始化了算法
            Logs.e(TAG, "算法已初始化...");
            return;
        }
        if ("".equals(regCode) || null == regCode) {
            Logs.e(TAG, "注册码为空...");
            return;
        }

        Logs.i(TAG, "初始化算法..." + regCode);
        FileUtils.saveFile(SystemUtils.getRootPath() + "/FaceSdk", "license.dat", regCode);
        if (MyApp.isSoft) {
            MyApp.mFaceAlgorithm = Soft5003.getInstance(this);
            Soft5003 soft5003 = (Soft5003) MyApp.mFaceAlgorithm;
            boolean isRegister = soft5003.isRegister();
            Logs.i(TAG, "isRegister:" + isRegister);
            if (isRegister) {
                mInitPresenterImpl.softInit();
            } else {
                showToast("注册码不正确，请输入正确的注册码");
                showAlgorithmDialog(IInitPresenter.ALGORITHM_TYPE_INPUT);
            }
        }
    }

    //创建带有一个输入框的对话框
    private void createInputDialog(final String message, final int type, String input) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_password, null, false);
        TextView textView = view.findViewById(R.id.text_dialog_text);
        textView.setText(message);
        builder.setCancelable(false);
        final EditText editText = view.findViewById(R.id.edit_dialog_input);
        editText.setText(input);
        builder.setView(view);
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String deviceCode = editText.getText().toString();
                switch (type) {
                    case 1://请输入注册码，软授权使用
                        if (TextUtils.isEmpty(deviceCode)) {
                            showToast(message);
                            dialog.dismiss();
                            isShowDialog = false;
                            return;
                        }
                        break;
                    case 2://请输入设备号
                        if (TextUtils.isEmpty(deviceCode)) {
                            showToast(message);
                            initComplete();
                            dialog.dismiss();
                            isShowDialog = false;
                            return;
                        }
                        initNetworkData(deviceCode);
                        break;
                }
                dialog.dismiss();
                isShowDialog = false;
            }
        });
        isShowDialog = true;
        builder.show();
    }

    private void initNetworkData(String deviceCode) {
        mInitPresenterImpl.getDevicesParam(deviceCode);
//    sendBroadcast(new Intent("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    public void showCameraDialog(int type) {
        String message;
        if (type == Camera.CameraInfo.CAMERA_FACING_BACK) {
            message = "打开照相机失败，请重启设备后再试";
        } else {
            message = "红外摄像头打开失败，请确定是否有红外摄像头";
        }
        createDialog("温馨提示", message, "确定", "取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                isShowDialog = false;
                finish();
            }
        });
    }

    @Override
    public void showDeviceCodeDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                createInputDialog("请输入设备号", 2, "");
            }
        });
    }

    @Override
    public void showDeviceCodeErrorDialog(String message) {
        createDialog("温馨提示", message, "设置", "取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(MainActivity.this, SettingActivity.class);
                startActivityForResult(intent, 1);
                dialog.dismiss();
                isShowDialog = false;
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        if (KeyEvent.KEYCODE_HOME == keyCode) {
            return true;
        }
        return super.onKeyDown(keyCode, event);//继续执行父类其他点击事件
    }

    @Override
    public void showInitType(String message) {
        showToast(message);
    }

    @Override
    public void showAdv() {
        if (isShowDialog) {//已经显示提示框了
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mImgAdv.setVisibility(View.VISIBLE);
                mPreviewLL.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void hideAdv() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isInitFinish) {
                    mImgAdv.setVisibility(View.GONE);
                    mPreviewLL.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void showToast(final boolean isCorrect, final String message) {
        if (isPause) {
            Logs.e(TAG, "已经退出了");
            return;
        }
        if (lastShowToastType && !isCorrect && ((TimeUtil.getCurrentTimeMillis() - lastShowToastTime) < 3000)) {
            //只有当第一次成功了，3秒内失败了则不显示失败
            return;
        }
        lastShowToastType = isCorrect;
        lastShowToastTime = TimeUtil.getCurrentTimeMillis();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mCustomToast == null) {
                    mCustomToast = new CustomToast(false, MainActivity.this, "");
                }
                mCustomToast.setIsCorrect(isCorrect);
                mCustomToast.setMessage(message);
                mCustomToast.show();
            }
        });
    }

    @Override
    public void showResult(int type, final String message) {
        switch (type) {
            case RecognitionPresenter.RECOGNITION_TYPE_SUCCESS:
                showToast(true, "识别成功");
                handler.post(new Runnable() {
                    @Override
                    public void run() {
//                        mTextName.setText(message);
                        mTextWelcome.setVisibility(View.VISIBLE);
                    }
                });
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        mTextName.setText("");
                        mTextWelcome.setVisibility(View.GONE);
                    }
                }, 2000);
                break;
            case RecognitionPresenter.RECOGNITION_TYPE_FAILED:
                showToast(false, "识别失败");
                break;
            case RecognitionPresenter.RECOGNITION_TYPE_BLACKLIST:
                showToast(false, "未具备通行权限");
                break;
            case RecognitionPresenter.RECOGNITION_TYPE_OUT_EXPIRY_TIME:
                showToast(false, "超过有效期");
                break;
            case RecognitionPresenter.RECOGNITION_TYPE_NO_LIVE:
                showToast(false, "认证失败");
                break;
            case RecognitionPresenter.RECOGNITION_TYPE_NO_PEOPLE:
                showToast(false, "无人员");
                break;
        }
    }

    @Override
    public void onPreviewFrame(Bitmap bitmap) {
        if (!isInitFinish || isPause) {
            return;
        }
        mRecognitionPresenter.detectColorFace(bitmap);
    }

    @Override
    public void onPreviewYuv(byte[] yuv) {
        if (!isInitFinish || isPause) {
            return;
        }
        mRecognitionPresenter.detectColorFace(yuv);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_OK:
                if (requestCode == REQUEST_CODE_SETTING) {
                    String deviceCode = data.getStringExtra("deviceCode");
                    initNetworkData(deviceCode);
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_logo_setting:
                createDialog(3);
                break;
            case R.id.img_main_adv:
                mImgAdv.setVisibility(View.GONE);
                mPreviewLL.setVisibility(View.VISIBLE);
                mRecognitionPresenter.setCount(TimeUtil.getCurrentTimeMillis());
                break;
        }
    }

    private synchronized void createDialog(final int type) {
        isInitFinish = false;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_password, null, false);
        TextView textView = view.findViewById(R.id.text_dialog_text);
        textView.setText("请输入密码");
        final EditText editText = view.findViewById(R.id.edit_dialog_input);
        editText.setTypeface(Typeface.DEFAULT);
        editText.setTransformationMethod(new PasswordTransformationMethod());
        //隐藏密码
        editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        builder.setView(view);
        builder.setCancelable(false);
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isShowDialog = false;
                isInitFinish = true;
            }
        });
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String password = editText.getText().toString();
                if (TextUtils.isEmpty(password)) {
                    showToast("请输入密码");
                    isInitFinish = true;
                    return;
                }
                if (!TextUtils.equals("eyecool", password)) {
                    showToast("密码错误");
                    isInitFinish = true;
                    return;
                }
                switch (type) {
                    case 3://设置
                        Intent intent = new Intent(MainActivity.this, SettingActivity.class);
                        startActivityForResult(intent, REQUEST_CODE_SETTING);
                        break;
                }
                dialog.dismiss();
                isShowDialog = false;
                isInitFinish = true;
            }
        });
        isShowDialog = true;
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        dismissDialog(alertDialog);
    }

    private void dismissDialog(final AlertDialog alertDialog) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isShowDialog = false;
                isInitFinish = true;
                alertDialog.dismiss();
            }
        }, 30000);
    }

    private void setRegisterCode(String runCode) {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_register, null, false);
        TextView textView = view.findViewById(R.id.text_dialog_runcode);
        textView.setText(runCode);
        final EditText editPassword = view.findViewById(R.id.edit_register_input);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String regCode = editPassword.getText().toString();
                if (TextUtils.isEmpty(regCode)) {
                    showToast("请输入算法注册码");
                    return;
                }
                if (!mInitPresenterImpl.checkRegCode(regCode)) {
                    showToast("注册码校验不通过");
                    return;
                }
                boolean isRegister = mInitPresenterImpl.register(regCode);
                Logs.e(TAG, "注册结果： " + isRegister);
                if (isRegister) {
                    mInitPresenterImpl.softInit();
                } else {
                    showToast("注册码无效");
                }
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                dialog.dismiss();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    private boolean isScreenLandscape() {
        Configuration mConfiguration = this.getResources().getConfiguration(); //获取设置的配置信息
        int ori = mConfiguration.orientation; //获取屏幕方向
        if (ori == mConfiguration.ORIENTATION_LANDSCAPE) {
            //横屏
            return true;
        } else {
            //竖屏
            return false;
        }
    }

    private void registerEventBus() {
        if (!isRegisterEvent) {
            EventBus.getDefault().register(this);
            isRegisterEvent = true;
        }
    }

    private void unregisterEvent() {
        if (isRegisterEvent) {
            EventBus.getDefault().unregister(this);
            isRegisterEvent = false;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventMsgLogIn(EventMsg eventMsg) {
        switch (eventMsg.getType()) {
            case EventMsg.ACTION_START_UPDATE:
                //静默升级
                String url = (String) eventMsg.getBundle();
                LogSaveUtil.d(TAG, "收到准备升级消息，URL：" + url);
                Intent intent = new Intent(this, UpdateApkActivity.class);
                intent.putExtra("downLoadUrl", url);
                startActivity(intent);
                break;
        }

    }

    private void hideStatusNavigationBar() {
        if (Build.VERSION.SDK_INT < 16) {
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            int uiFlags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;
            uiFlags |= 0x00001000;
            getWindow().getDecorView().setSystemUiVisibility(uiFlags);
        }
    }
}