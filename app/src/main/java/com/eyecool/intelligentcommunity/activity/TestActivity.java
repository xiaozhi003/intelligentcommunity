package com.eyecool.intelligentcommunity.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.eyecool.intelligentcommunity.R;

/**
 * @author : yan
 * @date : 2018/10/11 16:35
 * @desc : todo
 */
public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        startActivity(new Intent(this, MainActivity.class));
    }
}
