package com.eyecool.intelligentcommunity.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Power;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.bigkoo.pickerview.view.TimePickerView;
import com.eyecool.intelligentcommunity.BuildConfig;
import com.eyecool.intelligentcommunity.MyApp;
import com.eyecool.intelligentcommunity.R;
import com.eyecool.intelligentcommunity.service.MonitoringService;
import com.eyecool.intelligentcommunity.utils.AlarmManagerUtils;
import com.eyecool.intelligentcommunity.utils.CheckUtil;
import com.eyecool.intelligentcommunity.utils.Constant;
import com.eyecool.intelligentcommunity.utils.GpioJhc3399;
import com.eyecool.intelligentcommunity.utils.GpioManager;
import com.eyecool.intelligentcommunity.utils.SystemUtil;
import com.eyecool.library.activity.BaseActivity;
import com.eyecool.library.utils.ActivityUtils;
import com.eyecool.library.utils.Logs;
import com.eyecool.library.utils.RegularUtils;
import com.eyecool.library.utils.SharedPreferencesUtils;
import com.eyecool.library.utils.SystemUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SettingActivity extends BaseActivity implements View.OnClickListener {

  private static final String TAG = "SettingActivity";

  private EditText mEditIp, mEditPort, mEditDeviceCode, mEditGpioDelay;
  private String mIp, mPort, mDeviceCode;
  private Button mBtnSoftAuth, mBtnSave, mBtnExit,btn_reboot, btn_exception;
  private ImageView mImgBack;
  private String mStartTime;
  private String mStopTime;
  private TextView mTvStartTime;
  private TextView mTvStopTime;
  private TimePickerView mStartTimePickerView;
  private TimePickerView mStopTimePickerView;
  private CheckBox mCbSetting;
  private SeekBar mVolumeSeekBar;
  private RadioGroup mAuthRadioGroup;
  private SwitchCompat mMonitorSwitch,switch_save_pic;
  private TextView mVersionTv;
  private boolean mCameraSetting;

  @Override
  public void initData() {
    mIp = SharedPreferencesUtils.getString(this, Constant.sharedPreferencesName, "ip");
    if (TextUtils.isEmpty(mIp)) {
      mIp = Constant.sIp;
    }
    mPort = SharedPreferencesUtils.getString(this, Constant.sharedPreferencesName, "port");
    if (TextUtils.isEmpty(mPort)) {
      mPort = Constant.sPort;
    }
    mDeviceCode = SharedPreferencesUtils.getString(this, Constant.sharedPreferencesName, "deviceCode");
    if (TextUtils.isEmpty(mDeviceCode)) {
      mDeviceCode = Constant.sDeviceCode;
    }
    mStartTime = SharedPreferencesUtils.getString(this, Constant.sharedPreferencesName,
        SharedPreferencesUtils.KEY_CAMERA_LIGHT_START_TIME);
    mStopTime = SharedPreferencesUtils.getString(this, Constant.sharedPreferencesName,
        SharedPreferencesUtils.KEY_CAMERA_LIGHT_CLOSE_TIME);
    mCameraSetting = SharedPreferencesUtils.getBoolean(this, Constant.sharedPreferencesName,
        SharedPreferencesUtils.KEY_CAMERA_LIGHT_SETTING, true);
    MyApp.isSoft = SharedPreferencesUtils.getBoolean(this, Constant.sharedPreferencesName, SharedPreferencesUtils.KEY_SOFT, true);
    Constant.sVolume = SharedPreferencesUtils.getInt(this, Constant.sharedPreferencesName, SharedPreferencesUtils.KEY_VOLUME, 50);
    Constant.sGPIODelay = SharedPreferencesUtils.getInt(this, Constant.sharedPreferencesName, SharedPreferencesUtils.KEY_GPIO_DELAY, 300);
  }

  @Override
  public void initView(Bundle savedInstanceState) {
    setContentView(R.layout.activity_setting);
    mEditIp = findViewById(R.id.edit_setting_ip);
    mEditPort = findViewById(R.id.edit_setting_port);
    mEditDeviceCode = findViewById(R.id.edit_setting_access_key);
    mEditGpioDelay = findViewById(R.id.gpioDelayEdit);
    mEditIp.setText(mIp);
    mEditPort.setText(mPort);
    mEditDeviceCode.setText(mDeviceCode);
    mEditGpioDelay.setText(Constant.sGPIODelay + "");
    mBtnSave = findViewById(R.id.btn_setting_save);
    mBtnSave.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        saveInfo();
      }
    });
    mBtnSoftAuth = findViewById(R.id.btn_setting_soft_anth);
    mBtnSoftAuth.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        saveInfo();
      }
    });
    mImgBack = findViewById(R.id.img_call_back);
    mImgBack.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });
    mBtnExit = findViewById(R.id.btn_setting_exit);
    mVersionTv = findViewById(R.id.versionTv);
    mBtnExit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingActivity.this);
        builder.setMessage("是否要退出程序");
        builder.setNegativeButton("取消", null);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
//                        finish();
            dialog.dismiss();
            ActivityUtils.getInstance().exitApp();

          }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
      }
    });
//    mBtnException.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        throw new RuntimeException("抛出异常测试用...");
//      }
//    });

    mTvStartTime = findViewById(R.id.tv_start_time);
    mTvStopTime = findViewById(R.id.tv_stop_time);
    mCbSetting = findViewById(R.id.cb_setting);
    mAuthRadioGroup = findViewById(R.id.authRadioGroup);
    mVolumeSeekBar = findViewById(R.id.volumeSeekBar);
    mVolumeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override
      public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        Constant.sVolume = progress;
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {

      }

      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {

      }
    });

    if (TextUtils.isEmpty(mStartTime)) {
      mStartTime = Constant.DEFAULT_OPEN_LIGHT_TIME;
    }
    if (TextUtils.isEmpty(mStopTime)) {
      mStopTime = Constant.DEFAULT_CLOSE_LIGHT_TIME;
    }
    mTvStartTime.setText(mStartTime);
    mTvStopTime.setText(mStopTime);
    mCbSetting.setChecked(mCameraSetting);
    mTvStartTime.setOnClickListener(this);
    mTvStopTime.setOnClickListener(this);

    mStartTimePickerView = getTimePickerView();
    mStopTimePickerView = getTimePickerView();

    mAuthRadioGroup.check(MyApp.isSoft ? R.id.softId : R.id.hardId);
    mVolumeSeekBar.setProgress(Constant.sVolume);

    mMonitorSwitch = findViewById(R.id.switch_monitor);
    mMonitorSwitch.setChecked(CheckUtil.isServiceWorked(this, "com.eyecool.intelligentcommunity.service." + MonitoringService.class.getSimpleName()));
    mMonitorSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
          if (!CheckUtil.isServiceWorked(SettingActivity.this, MonitoringService.class.getSimpleName())) {
            // 启动监控进程
            Intent intent = new Intent(SettingActivity.this, MonitoringService.class);
            intent.setAction("android.intent.action.RESPOND_VIA_MESSAGE");
            startService(intent);
          }
        } else {
          // 发送关闭监控进程广播
          Intent intent = new Intent();
          intent.setAction("kill_self");
          sendOrderedBroadcast(intent, null);
        }
      }
    });
    //是否保存检活失败图片
    switch_save_pic = findViewById(R.id.switch_save_pic);
    switch_save_pic.setChecked(SharedPreferencesUtils.getBoolean(this,Constant.sharedPreferencesName,SharedPreferencesUtils.KEY_IS_SAVE_FAILE_PIC,false));
    switch_save_pic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
         Constant.isSaveFailePic = true;
         SharedPreferencesUtils.saveBoolean(SettingActivity.this,Constant.sharedPreferencesName,SharedPreferencesUtils.KEY_IS_SAVE_FAILE_PIC,true);
        } else {
          Constant.isSaveFailePic = false;
          SharedPreferencesUtils.saveBoolean(SettingActivity.this,Constant.sharedPreferencesName,SharedPreferencesUtils.KEY_IS_SAVE_FAILE_PIC,false);

        }
      }
    });

    mVersionTv.setText("版本号：" + BuildConfig.VERSION_NAME);
    btn_reboot = findViewById(R.id.btn_reboot);
    btn_reboot.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if ("rk3399-all".equals(SystemUtil.getSystemModel())) { //YZ3399
          Intent intent2 = new Intent(Intent.ACTION_REBOOT);
          intent2.putExtra("nowait", 1);
          intent2.putExtra("interval", 1);
          intent2.putExtra("window", 0);
          sendBroadcast(intent2);
        } else if ("rk3399-mid".equals(SystemUtil.getSystemModel())) {//JHC3399，关闭
          String cmd = "su -c reboot";
          try {
            Runtime.getRuntime().exec(cmd);
          } catch (IOException e) {
            Logs.e(TAG, e.getMessage());
          }
        }

      }
    });
    btn_exception = findViewById(R.id.btn_exception);
    btn_exception.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        String k =null;
        int j = k.length();
      }
    });
  }

  private void saveInfo() {
    String ip = mEditIp.getText().toString();
    if (TextUtils.isEmpty(ip)) {
      showToast("请输入ip地址");
      return;
    }
    String ip2 = ip.substring(7, ip.length());
    if (!RegularUtils.isIP(ip2)) {
      showToast("ip地址格式错误");
      return;
    }
    String port = mEditPort.getText().toString();
    if (TextUtils.isEmpty(port)) {
      showToast("请输入端口");
      return;
    }
    String deviceCode = mEditDeviceCode.getText().toString();
    if (TextUtils.isEmpty(deviceCode)) {
      showToast("请输入设备号");
      return;
    }
    String gpioDelay = mEditGpioDelay.getText().toString();
    if (TextUtils.isEmpty(gpioDelay)) {
      showToast("请输入gpio延迟时间");
      return;
    }

    Constant.sGPIODelay = Integer.parseInt(gpioDelay);

    SharedPreferencesUtils.saveString(this, Constant.sharedPreferencesName, "ip", ip);
    SharedPreferencesUtils.saveString(this, Constant.sharedPreferencesName, "port", port);
    SharedPreferencesUtils.saveString(this, Constant.sharedPreferencesName, "deviceCode", deviceCode);

    MyApp.isSoft = mAuthRadioGroup.getCheckedRadioButtonId() == R.id.softId ? true : false;
    SharedPreferencesUtils.saveBoolean(this, Constant.sharedPreferencesName, SharedPreferencesUtils.KEY_SOFT, MyApp.isSoft);
    SharedPreferencesUtils.saveInt(this, Constant.sharedPreferencesName, SharedPreferencesUtils.KEY_VOLUME, Constant.sVolume);
    SharedPreferencesUtils.saveInt(this,Constant.sharedPreferencesName,SharedPreferencesUtils.KEY_GPIO_DELAY,Constant.sGPIODelay);

    String startTime = mTvStartTime.getText().toString();
    String stopTime = mTvStopTime.getText().toString();
    boolean checked = mCbSetting.isChecked();
    SharedPreferencesUtils.saveString(this, Constant.sharedPreferencesName,
        SharedPreferencesUtils.KEY_CAMERA_LIGHT_START_TIME, startTime);
    SharedPreferencesUtils.saveString(this, Constant.sharedPreferencesName,
        SharedPreferencesUtils.KEY_CAMERA_LIGHT_CLOSE_TIME, stopTime);
    SharedPreferencesUtils.saveBoolean(this, Constant.sharedPreferencesName,
        SharedPreferencesUtils.KEY_CAMERA_LIGHT_SETTING, checked);

    SystemUtils.hideKeyboard(this);
    if (checked) {
      //开启定时闹钟
      AlarmManagerUtils.getInstance(this).startRemind(startTime, Constant.ALARM_OPEN_LIGHT);
      AlarmManagerUtils.getInstance(this).startRemind(stopTime, Constant.ALARM_CLOSE_LIGHT);
    } else {
      AlarmManagerUtils.getInstance(this).cancelAll();
    }
    showToast("保存成功");
    Intent intent = new Intent();
    intent.putExtra("deviceCode", deviceCode);
    setResult(RESULT_OK, intent);
    finish();
  }

  @Override
  public void loadData() {
    mEditIp.setText(mIp);
    mEditPort.setText(mPort);
    mEditDeviceCode.setText(mDeviceCode);
  }

  private TimePickerView getTimePickerView() {
    Calendar startDate = Calendar.getInstance();
    Calendar endDate = Calendar.getInstance();

    startDate.set(Calendar.HOUR, 0);
    startDate.set(Calendar.MINUTE, 0);

    endDate.set(Calendar.HOUR, 24);
    endDate.set(Calendar.MINUTE, 60);

    return new TimePickerBuilder(this, new OnTimeSelectListener() {
      @Override
      public void onTimeSelect(Date date, View v) {
        TextView textView = (TextView) v;
        textView.setText(getTime(date));
      }
    })
        .setType(new boolean[]{false, false, false, true, true, false})// 默认全部显示
        .setCancelText("取消")
        .setSubmitText("确定")
        .setContentTextSize(21)//滚轮文字大小
        .setSubmitColor(Color.BLUE)//确定按钮文字颜色
        .setCancelColor(Color.BLUE)//取消按钮文字颜色
        .setRangDate(startDate, endDate)//起始终止年月日设定
        .setLabel("", "", "", "时", "分", "")//默认设置为年月日时分秒
        .isCenterLabel(true)
        .setDividerColor(Color.DKGRAY)
        .setBackgroundId(0x00FFFFFF) //设置外部遮罩颜色
        .setDecorView(null)
        .build();
  }

  private String getTime(Date date) {//可根据需要自行截取数据显示
    SimpleDateFormat format = new SimpleDateFormat("HH:mm");
    return format.format(date);
  }

  private Calendar getDate(String time) {
    if (TextUtils.isEmpty(time)) return null;

    String[] split = time.split(":");
    int hour = Integer.parseInt(split[0]);
    int min = Integer.parseInt(split[1]);
    Logs.e("hour=" + hour + ", min=" + min);

    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.HOUR_OF_DAY, hour);
    calendar.set(Calendar.MINUTE, min);

    return calendar;
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.tv_start_time:
        mStartTimePickerView.setDate(getDate(mTvStartTime.getText().toString()));
        mStartTimePickerView.show(v);
        break;
      case R.id.tv_stop_time:
        mStopTimePickerView.setDate(getDate(mTvStopTime.getText().toString()));
        mStopTimePickerView.show(v);
        break;
      default:
        break;
    }
  }
}
