package com.eyecool.intelligentcommunity.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.eyecool.intelligentcommunity.MyApp;
import com.eyecool.intelligentcommunity.R;
import com.eyecool.intelligentcommunity.manager.ApkUpdateManager;
import com.eyecool.library.activity.BaseActivity;


public class UpdateApkActivity extends BaseActivity implements ApkUpdateManager.CancleCallBack {
    private final String TAG = "UpdateApkActivity";
    private String downLoadUrl = "";
    private TextView tv_tip;
    private View rootView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_apk);
        rootView = findViewById(R.id.rootView);
        tv_tip = findViewById(R.id.tv_tip);
        downLoadUrl = getIntent().getStringExtra("downLoadUrl");
        Log.d(TAG, "downLoadUrl = " + downLoadUrl);
        h.sendEmptyMessageDelayed(0, 2 * 1000);
    }

    @Override
    public void initData() {

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            MyApp.screenHeight = rootView.getHeight();
            MyApp.screenWidth = rootView.getWidth();
        }
    }

    Handler h = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
            tv_tip.setVisibility(View.GONE);
            startUpdate();
        }
    };

    private void startUpdate() {
        Log.d(TAG, "开始更新应用");
        ApkUpdateManager.getInstance().init(this, this);
        ApkUpdateManager.getInstance().startUpdate(rootView, downLoadUrl);
        fullScreenImmersive(getWindow().getDecorView());
    }

    private void fullScreenImmersive(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;
            view.setSystemUiVisibility(uiOptions);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void initView(Bundle savedInstanceState) {

    }

    @Override
    public void loadData() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ApkUpdateManager.getInstance().release();
    }


    @Override
    public void cancleUpdate() {

    }
}
