package com.eyecool.intelligentcommunity.utils;

import android.util.Log;

public class GpioJhc3399 {

  /*
                          gpio map
                          B7 174
                          B6 175
                      */
  private static String gpioNumPath = "/sys/class/gpio/gpio";
  private static String gpioExport = "/sys/class/gpio/export";

  public static int Gpio2C5 = 2 * 32 + 8 + 8 + 5;//C5pin角

  public static String GpioOutputMode = "out";
  public static String GpioIutputMode = "in";

    /*
        use:
        1:I618Gpio.init();
        2:I618Gpio.setvalue(I618Gpio.Gpio0B6,0);
          I618Gpio.setvalue(I618Gpio.Gpio0B7,1);
          or
          int value = I618Gpio.getvalue(I618Gpio.Gpio0B6);
          int value = I618Gpio.getvalue(I618Gpio.Gpio0B7);
    */

  public static void init() {
    ShellUtil.CommandResult r1 = ShellUtil.execCommand("chmod 666 " + gpioExport, true);
    Log.d("GpioJhc3399_chmod666", "result: " + r1.result + " success: " + r1.successMsg + " error: " + r1.errorMsg);
    ShellUtil.CommandResult r2 = ShellUtil.execCommand("echo " + Gpio2C5 + " > " + gpioExport, false);
    Log.d("GpioJhc3399_echo", "result: " + r2.result + " success: " + r2.successMsg + " error: " + r2.errorMsg);
  }

  //mode string: out  in
  public static void setMode(int num, String mode) {
    ShellUtil.execCommand("echo " + num + " > " + gpioExport, false);
    ShellUtil.execCommand("echo " + mode + " > " + gpioNumPath + num + "/direction", false);
  }

  //value int:0 low 1 high
  public static void setValue(int num, int value) {
    ShellUtil.CommandResult r1 = ShellUtil.execCommand("echo " + GpioOutputMode + " > " + gpioNumPath + num + "/direction", true);
    Log.d("GpioJhc3399_echo", "result: " + r1.result + " success: " + r1.successMsg + " error: " + r1.errorMsg);
    ShellUtil.CommandResult r2 = ShellUtil.execCommand("echo " + value + " > " + gpioNumPath + num + "/value", true);
    Log.d("GpioJhc3399_value", "result: " + r2.result + " success: " + r2.successMsg + " error: " + r2.errorMsg);
  }

  //value int:0 low 1 high
  public static int getValue(int num) {
    ShellUtil.execCommand("echo " + GpioIutputMode + " > " + gpioNumPath + num + "/direction", true);
    String ret = ShellUtil.execCommand("cat " + gpioNumPath + num + "/value", true).successMsg;
    return Integer.parseInt(ret);
  }
}


