package com.eyecool.intelligentcommunity.utils;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.util.SparseArray;

/**
 * Created by huangmin on 2018/6/4.
 */

public class AlarmManagerUtils {

    private static final long TIME_INTERVAL = 2 * 60 * 1000;//闹钟执行任务的时间间隔
    private Context context;
    private static AlarmManager updateAlarms;
    private SparseArray<PendingIntent> mPendingIntentMap = new SparseArray<>();
    private SparseArray<Long> mTriggers = new SparseArray<>();

    private AlarmManagerUtils(Context aContext) {
        this.context = aContext.getApplicationContext();
    }

    //
    private static AlarmManagerUtils instance = null;

    public static AlarmManagerUtils getInstance(Context aContext) {
        if (instance == null) {
            synchronized (AlarmManagerUtils.class) {
                if (instance == null) {
                    instance = new AlarmManagerUtils(aContext);
                }
            }
        }
        return instance;
    }

    /**
     * @param time  时间格式  HH:mm     如: 18:00
     * @param reqCode
     */
    public void startRemind(String time, int reqCode) {
        if (TextUtils.isEmpty(time)) {
            return;
        }
        String[] split = time.split(":");
        int hour = Integer.parseInt(split[0]);
        int min = Integer.parseInt(split[1]);

        TimingUtil timingUtil = new TimingUtil();
        timingUtil.setTimingDateTime(0, hour, min);
        //设置前，先取消置顶闹钟
        alarmCancel(reqCode);
        createGetUpAlarmManager(timingUtil.getTimingDateTime(), reqCode);
    }

    public void createGetUpAlarmManager(Long trigger, int reqCode) {
        if (updateAlarms == null) {
            updateAlarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        }
        Intent intent = new Intent("com.eyecool.intelligentcommunity.alarm");
        intent.putExtra("reqCode", reqCode);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, reqCode, intent, 0);
        mPendingIntentMap.put(reqCode, pendingIntent);
        mTriggers.put(reqCode, trigger);

        //版本适配
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {// 6.0及以上
            updateAlarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                    trigger, pendingIntent);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {// 4.4及以上
            updateAlarms.setExact(AlarmManager.RTC_WAKEUP, trigger,
                    pendingIntent);
        } else {
            updateAlarms.setRepeating(AlarmManager.RTC_WAKEUP,
                    trigger, TIME_INTERVAL, pendingIntent);
        }
    }

    @SuppressLint("NewApi")
    public void getUpAlarmManagerWorkOnReceiver(int reqCode, long interval) {
        PendingIntent pendingIntent = mPendingIntentMap.get(reqCode);
        if (pendingIntent == null) return;
        Long lastTime = mTriggers.get(reqCode);
        long nextTime = lastTime + interval;

        //高版本重复设置闹钟达到低版本中setRepeating相同效果
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {// 6.0及以上
            updateAlarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                    nextTime, pendingIntent);
            mTriggers.put(reqCode, nextTime);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {// 4.4及以上
            updateAlarms.setExact(AlarmManager.RTC_WAKEUP, nextTime, pendingIntent);
            mTriggers.put(reqCode, nextTime);
        }
    }

    public void alarmCancel(int reqCode) {
        PendingIntent pendingIntent = mPendingIntentMap.get(reqCode);
        if (pendingIntent == null) return;

        if (updateAlarms != null) {
            updateAlarms.cancel(pendingIntent);
        }
        mPendingIntentMap.remove(reqCode);
        mTriggers.remove(reqCode);
    }

    public void cancelAll() {
        int size = mPendingIntentMap.size();
        for (int i = 0; i < size; i++) {
            PendingIntent pendingIntent = mPendingIntentMap.valueAt(i);
            if (updateAlarms != null && pendingIntent != null) {
                updateAlarms.cancel(pendingIntent);
            }
        }
        mPendingIntentMap.clear();
        mTriggers.clear();
    }
}