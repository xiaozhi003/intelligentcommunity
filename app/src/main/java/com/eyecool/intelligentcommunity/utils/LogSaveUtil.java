package com.eyecool.intelligentcommunity.utils;

import com.eyecool.intelligentcommunity.BuildConfig;

import org.apache.log4j.Logger;

/**
 * 打印日志并保存到本地
 */
public class LogSaveUtil {
    private static final String tag = "Intelligentcommunity-TAG";
    private static final Logger LOGGER = Logger.getLogger(tag);

    public static void d(String tag, String msg) {
        LOGGER.debug(tag + "---->" + msg);
    }

    public static void dInDebug(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            LOGGER.debug(tag + "---->" + msg);
        }
    }

    public static void e(String tag, String msg) {
        LOGGER.error(tag + "---->" + msg);
    }

    public static void eInDebug(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            LOGGER.error(tag + "---->" + msg);
        }
    }
}
