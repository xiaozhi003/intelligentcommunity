package com.eyecool.intelligentcommunity.utils;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by vinda on 2017/5/25.
 */

public class GpioManager {

  public static void open() {
    try {
      FileWriter writeF = new FileWriter("/sys/devices/platform/power/GPIO4");
      writeF.write("H", 0, "H".length());
      writeF.close();

      FileWriter writeGpio1 = new FileWriter("/sys/devices/platform/power/GPIO1");
      writeGpio1.write("H", 0, "H".length());
      writeGpio1.close();
      System.out.println("gpio1high=================================");
    } catch (IOException ioe) {
      System.out.println("gpio1low err=================================" + ioe.getMessage());
    }
  }


  public static void close() {

    try {
      FileWriter writeF = new FileWriter("/sys/devices/platform/power/GPIO4");
      writeF.write("L", 0, "L".length());
      writeF.close();

      FileWriter writeGpio1 = new FileWriter("/sys/devices/platform/power/GPIO1");
      writeGpio1.write("L", 0, "L".length());
      writeGpio1.close();
      System.out.println("gpio1low=================================");
    } catch (IOException ioe) {
      System.out.println("gpio1low err=================================");
    }

  }
}


