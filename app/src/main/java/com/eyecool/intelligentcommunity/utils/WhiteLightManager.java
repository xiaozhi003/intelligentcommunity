package com.eyecool.intelligentcommunity.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.eyecool.intelligentcommunity.MyApp;
import com.eyecool.library.utils.Logs;
import com.eyecool.library.utils.SharedPreferencesUtils;
import com.eyecool.library.utils.TimeUtil;

/**
 * @author : yan
 * @date : 2018/10/19 16:47
 * @desc : todo
 */
public class WhiteLightManager {

    private static final String TAG = "WhiteLightManager";

    public static void handleFaceCameraLight(Context context) {
        String startTime = SharedPreferencesUtils.getString(context, Constant.sharedPreferencesName,
                SharedPreferencesUtils.KEY_CAMERA_LIGHT_START_TIME);
        String stopTime = SharedPreferencesUtils.getString(context, Constant.sharedPreferencesName,
                SharedPreferencesUtils.KEY_CAMERA_LIGHT_CLOSE_TIME);
        if (TextUtils.isEmpty(startTime)) {
            startTime = Constant.DEFAULT_OPEN_LIGHT_TIME;
        }
        if (TextUtils.isEmpty(stopTime)) {
            stopTime = Constant.DEFAULT_CLOSE_LIGHT_TIME;
        }

        Log.e("yan---", "handleCameraLight: startTime="+startTime +", stopTime="+stopTime);
        int currentHour = Integer.parseInt(TimeUtil.getCurrent24Hour());
        int currentMin = Integer.parseInt(TimeUtil.getCurrentMin());
        int totalCurrentMin = currentHour * 60 + currentMin;

        String[] splitStart = startTime.split(":");
        int startHour = Integer.parseInt(splitStart[0]);
        int startMin = Integer.parseInt(splitStart[1]);
        int totalStartMin = startHour * 60 + startMin;

        String[] splitStop = stopTime.split(":");
        int stopHour = Integer.parseInt(splitStop[0]);
        int stopMin = Integer.parseInt(splitStop[1]);
        int totalStopMin = stopHour * 60 + stopMin;

        Log.e(TAG, "totalCurrentMin=" + totalCurrentMin + ", totalStartMin=" + totalStartMin + ", totalStopMin=" + totalStopMin);

        if (totalStartMin > totalStopMin) {
            //跨一天时间
            if (totalCurrentMin < totalStopMin || totalCurrentMin > totalStartMin) {
                //开启补光灯
                Log.e(TAG, "handleCameraLight: 开启补光灯");
                MyApp.mDoor.openFaceCameraLight();
            } else {
                MyApp.mDoor.closeFaceCameraLight();
            }
        } else if (totalStartMin == totalStopMin) {
            Log.e(TAG, "handleCameraLight: 开启补光灯");
            MyApp.mDoor.openFaceCameraLight();
        } else {
            //一天时间段内
            if (totalCurrentMin > totalStartMin && totalCurrentMin < totalStopMin) {
                Log.e(TAG, "handleCameraLight: 开启补光灯");
                MyApp.mDoor.openFaceCameraLight();
            } else {
                MyApp.mDoor.closeFaceCameraLight();
            }
        }

        handleAlarm(context);
    }

    /**
     * 开启定时白灯闹钟
     */
    private  static void handleAlarm(Context context) {
        String openTime = SharedPreferencesUtils.getString(context, Constant.sharedPreferencesName,
                SharedPreferencesUtils.KEY_CAMERA_LIGHT_START_TIME);
        String closeTime = SharedPreferencesUtils.getString(context, Constant.sharedPreferencesName,
                SharedPreferencesUtils.KEY_CAMERA_LIGHT_CLOSE_TIME);
        boolean openSetting = SharedPreferencesUtils.getBoolean(context, Constant.sharedPreferencesName,
                SharedPreferencesUtils.KEY_CAMERA_LIGHT_SETTING, true);

        if (TextUtils.isEmpty(openTime)) {
            openTime = Constant.DEFAULT_OPEN_LIGHT_TIME;
        }
        if (TextUtils.isEmpty(closeTime)) {
            closeTime = Constant.DEFAULT_CLOSE_LIGHT_TIME;
        }

        Logs.e("openSetting=" + openSetting);
        if (openSetting) {
            //开启定时闹钟
            AlarmManagerUtils.getInstance(context).startRemind(openTime, Constant.ALARM_OPEN_LIGHT);
            AlarmManagerUtils.getInstance(context).startRemind(closeTime, Constant.ALARM_CLOSE_LIGHT);
        } else {
            AlarmManagerUtils.getInstance(context).cancelAll();
        }
    }

}
