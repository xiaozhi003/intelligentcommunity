package com.eyecool.intelligentcommunity.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by lhf on 2018/1/16.
 * 用于后台服务启动计算时间
 */

public class TimingUtil {
    private Calendar mCalendar;

    public TimingUtil() {
    }

    /**
     * 设置定时时间
     * @param delayDay   事例：0 表示当天， 7 表示7天后
     * @return
     */
    public void setTimingDateTime(int delayDay,int hour,int min){

        //得到日历实例，主要是为了下面的获取时间
        mCalendar = Calendar.getInstance();
        mCalendar.setTimeInMillis(System.currentTimeMillis());

        //获取当前毫秒值
        long systemTime = System.currentTimeMillis();

        //是设置日历的时间，主要是让日历的年月日和当前同步
        mCalendar.setTimeInMillis(System.currentTimeMillis());
        // 这里时区需要设置一下，不然可能个别手机会有8个小时的时间差
        mCalendar.setTimeZone(TimeZone.getTimeZone("GMT+8"));

        //设置在几点提醒  设置的为24点
        mCalendar.set(Calendar.HOUR_OF_DAY, hour);
        //设置在几分提醒  设置的为0分
        mCalendar.set(Calendar.MINUTE, min);
        //设置在几秒 几毫秒提醒
        mCalendar.set(Calendar.SECOND, 0);
        mCalendar.set(Calendar.MILLISECOND, 0);

        mCalendar.add(Calendar.DAY_OF_MONTH, delayDay);

        //获取上面设置的时间的毫秒值
        long selectTime = mCalendar.getTimeInMillis();


        // 如果当前时间大于设置的时间，那么就从第二天的设定时间开始
        if(systemTime > selectTime) {
            mCalendar.add(Calendar.DAY_OF_MONTH, 1);
        }
    }

    /**
     * 获取设置时间
     * @return 返回设置时间
     */
    public long getTimingDateTime(){
        //获取上面设置的时间的毫秒值
        long selectTime = mCalendar.getTimeInMillis();

        return selectTime;
    }

    /**
     * 计算时间间隔
     * @param strDate 起始时间
     * @param desDate 截至时间
     * @return 返回时间间隔
     */
    public int countIntervalDate(Date strDate, Date desDate){
        int date = 0;

        date = (int) ((desDate.getTime() - strDate.getTime()) / (1000 * 24 * 3600));  //换算成天数

        return date;
    }
}
