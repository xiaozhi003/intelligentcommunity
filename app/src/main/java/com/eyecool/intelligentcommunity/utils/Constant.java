package com.eyecool.intelligentcommunity.utils;

import com.eyecool.intelligentcommunity.entity.FeatureEntry;

import java.util.ArrayList;

/**
 * Created by ouyangfan on 2018/8/24.
 */

public class Constant {
    //服务器ip
    public static String sIp = "http://192.168.0.250";
//    public static String sIp = "http://192.168.0.116";
    //    public static String sIp = "http://114.116.13.29";
    public static String sPort = "8080";
    public static String sInterface = "/too-pro-system-v2/api/v2";
    public static String sDeviceCode = "";
    public static String sUrl = sIp + ":" + sPort + sInterface;
    public static String sharedPreferencesName = "intelligentCommunity";
    public static String sDeviceId = "";
    /**
     * 音量值
     */
    public static int sVolume = 50;
    /**
     * GPIO延迟时间（ms）
     */
    public static int sGPIODelay = 300;
    /**
     * 是否保存检活失败图片
     */
    public static boolean isSaveFailePic = false;
    //内存中的特征值
    public static ArrayList<FeatureEntry> sFeatureEntries = new ArrayList<>();

    /**
     * key-pubId, value-javabean
     */
//    public static Map<String, FeatureEntry> sFeatureEntries = new ConcurrentHashMap<>();
    //本地设备ip
    public static String sLocalIp = "";

    //每秒钟采集多少张图片
    public static String sTimeToCapture = "3";
    //夜晚比对分值
    public static int sNightScore = 65;
    //白天比对分值
    public static int sDayScore = 72;//80;
    //多少秒之内不允许同一个人重复出现
    public static int sTimeToFilterUser = 5;
    //白天起始时间
    public static String sDayPeriodTime = "06:00:00";
    //晚上起始时间
    public static String sNightPeriodTime = "18:00:00";

    public static final String DEFAULT_OPEN_LIGHT_TIME = "18:00";
    public static final String DEFAULT_CLOSE_LIGHT_TIME = "07:00";

    //补光灯开启/关闭  reqCode
    public static final int ALARM_OPEN_LIGHT = 100;
    public static final int ALARM_CLOSE_LIGHT = 101;
}
