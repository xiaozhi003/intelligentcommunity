package com.eyecool.intelligentcommunity.model;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.eyecool.intelligentcommunity.dao.DaoManager;
import com.eyecool.intelligentcommunity.dao.FeatureEntity;
import com.eyecool.intelligentcommunity.dao.SpecialUserEntity;
import com.eyecool.intelligentcommunity.entity.FeatureEntry;
import com.eyecool.intelligentcommunity.utils.Constant;
import com.eyecool.library.utils.Logs;

import java.util.List;

/**
 * Created by ouyangfan on 2018/8/30.
 */

public class DatabaseProcessing {

    private static final String TAG = DatabaseProcessing.class.getSimpleName();

    private DaoManager mDaoManager;

    public DatabaseProcessing(Context context) {
        mDaoManager = new DaoManager(context);
    }

    public void addFeature() {
        //查找正常特征数据、黑名单数据
        List<FeatureEntity> list = mDaoManager.getFeatureEntities();
        List<SpecialUserEntity> specialUserEntities = mDaoManager.getSpecialUserEntitiesUsed();
        List<FeatureEntry> featureEntries = Constant.sFeatureEntries;

        if (list != null) {
            Logs.e("开始添加本地数据:  " + list.size());
            Log.e(TAG, "add feature: 本地添加正常前size=" + featureEntries.size());
            for (FeatureEntity featureEntity : list) {
                // 人员状态，0：禁用1：启用2：待审核
                int faceStatus = featureEntity.getFaceStatus();
                if (faceStatus != 1) continue;

                String peopleCode = featureEntity.getPeopleCode();
                String feature = featureEntity.getFeature();
                byte[] featureByte = Base64.decode(feature, Base64.DEFAULT);
                FeatureEntry featureEntry = new FeatureEntry();
                featureEntry.setFeature(featureByte);//用来识别
                featureEntry.setPeopleCode(peopleCode);//用来找user表中的人，对应user表的pubId
                String pubId = featureEntity.getPubId();
                featureEntry.setFaceCode(pubId);//用来上传开门记录

                if (!featureEntries.contains(featureEntry)) {
                    featureEntries.add(featureEntry);
                }
            }
            Log.e(TAG, "add feature: 本地添加正常后size=" + featureEntries.size());
        } else {
            Logs.e(TAG,"添加本地数据失败");
        }

        if (specialUserEntities != null) {
            for (SpecialUserEntity specialUserEntity : specialUserEntities) {
                String peopleCode = specialUserEntity.getPeopleCode();
                String feature = specialUserEntity.getFeature();
                byte[] featureByte = Base64.decode(feature, Base64.DEFAULT);
                FeatureEntry featureEntry = new FeatureEntry();
                featureEntry.setFeature(featureByte);//用来识别
                featureEntry.setPeopleCode(peopleCode);//用来找user表中的人，对应user表的pubId
                String pubId = specialUserEntity.getPubId();
                featureEntry.setFaceCode(pubId);//用来上传开门记录

                if (!featureEntries.contains(featureEntry)) {
                    featureEntries.add(featureEntry);
                }
            }
            Log.e(TAG, "add feature: 本地添加黑名单后size=" + featureEntries.size());
        }
    }

}
