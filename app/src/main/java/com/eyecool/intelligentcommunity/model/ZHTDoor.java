package com.eyecool.intelligentcommunity.model;

import com.eyecool.library.algorithm.door.IDoor;
import com.eyecool.library.utils.Logs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by ouyangfan on 2018/9/4.
 * 振汇通开门
 */

public class ZHTDoor implements IDoor {

    //继电器
    private static final String RELAY_ON = "9";
    private static final String RELAY_OFF = "10";

    private static final String LED_CTL_PATH = "/sys/class/zh_gpio_out/out";

    private static ZHTDoor sZHTDoor;

    public static ZHTDoor getInstance(){
        if (sZHTDoor == null){
            synchronized (ZHTDoor.class){
                if (sZHTDoor == null){
                    sZHTDoor = new ZHTDoor();
                }
            }
        }
        return sZHTDoor;
    }

    private ZHTDoor() {
    }

    @Override
    public void open() {
        ctlLedRelay(RELAY_ON);
    }

    @Override
    public void close() {
        ctlLedRelay(RELAY_OFF);
    }

    private void ctlLedRelay(String val) {
        File file = new File(LED_CTL_PATH);
        if (!file.exists() || !file.canWrite()) {
            Logs.e("LED ctl path is not exists or can not write!!");
            return;
        }
        try {
            FileOutputStream fout = new FileOutputStream(file);
            PrintWriter pWriter = new PrintWriter(fout);
            pWriter.println(val);
            pWriter.flush();
            pWriter.close();
            fout.close();
        } catch (IOException re) {
            re.printStackTrace();
        }
    }

    public void closeLight() {//绿灯
        ctlLedRelay("4");
    }

    public void openLight(){//绿灯
        ctlLedRelay("3");
    }

    //打开人脸摄像头补光灯
    public void openFaceCameraLight(){
        ctlLedRelay("5");
    }

    //关闭人脸摄像头补光灯
    public void closeFaceCameraLight(){
        ctlLedRelay("6");
    }

    //打开红外摄像头补光灯
    public void openCameraLight(){
        ctlLedRelay("7");
    }

    //关闭红外摄像头补光灯
    public void closeCameraLight(){
        ctlLedRelay("8");
    }
}
