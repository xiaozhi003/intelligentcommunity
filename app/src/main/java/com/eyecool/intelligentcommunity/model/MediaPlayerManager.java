package com.eyecool.intelligentcommunity.model;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import com.eyecool.intelligentcommunity.R;

import com.eyecool.intelligentcommunity.utils.Constant;

/**
 * Created by ouyangfan on 2018/8/28.
 */

public class MediaPlayerManager {

  private static MediaPlayerManager sMediaPlayerManager;
  private MediaPlayer mMediaPlayer;

  public static MediaPlayerManager getInstance(Context context, int rawId) {
    if (sMediaPlayerManager == null) {
      synchronized (MediaPlayerManager.class) {
        if (sMediaPlayerManager == null) {
          sMediaPlayerManager = new MediaPlayerManager(context, rawId);
        }
      }
    }
    return sMediaPlayerManager;
  }

  private MediaPlayerManager(Context context, int rawId) {
    mMediaPlayer = MediaPlayer.create(context, rawId);
  }

  public void start() {
    if (mMediaPlayer != null) {
      mMediaPlayer.start();
      mMediaPlayer.setVolume(Constant.sVolume / 100f, Constant.sVolume / 100f);
      mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
          mediaPlayer.stop();
          release();
        }
      });
    }
  }


  public static void playSound(Context context) {
    SoundPool soundPool;
    if (Build.VERSION.SDK_INT >= 21) {
      SoundPool.Builder builder = new SoundPool.Builder();
      //传入音频的数量
      builder.setMaxStreams(1);
      //AudioAttributes是一个封装音频各种属性的类
      AudioAttributes.Builder attrBuilder = new AudioAttributes.Builder();
      //设置音频流的合适属性
      attrBuilder.setLegacyStreamType(AudioManager.STREAM_MUSIC);
      builder.setAudioAttributes(attrBuilder.build());
      soundPool = builder.build();
    } else {
      //第一个参数是可以支持的声音数量，第二个是声音类型，第三个是声音品质
      soundPool = new SoundPool(1, AudioManager.STREAM_SYSTEM, 5);
    }
    //第一个参数Context,第二个参数资源Id，第三个参数优先级
    soundPool.load(context,  R.raw.welcome, 1);
    soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
      @Override
      public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
        soundPool.play(1, 1, 1, 0, 0, 1);
      }
    });

  }

  public void stop() {
    if (mMediaPlayer != null) {
      mMediaPlayer.stop();
    }
  }

  public void release() {
    if (mMediaPlayer != null) {
      mMediaPlayer.reset();
      mMediaPlayer.release();
      mMediaPlayer = null;
      sMediaPlayerManager = null;
    }
  }
}
