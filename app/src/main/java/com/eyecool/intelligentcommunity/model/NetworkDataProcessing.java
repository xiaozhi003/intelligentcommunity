package com.eyecool.intelligentcommunity.model;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.eyecool.intelligentcommunity.BuildConfig;
import com.eyecool.intelligentcommunity.entity.ApkUpdateEntity;
import com.eyecool.intelligentcommunity.eventmsg.EventMsg;
import com.eyecool.intelligentcommunity.manager.ApkUpdateManager;
import com.eyecool.intelligentcommunity.manager.CameraManager;
import com.eyecool.intelligentcommunity.dao.DaoManager;
import com.eyecool.intelligentcommunity.dao.FeatureEntity;
import com.eyecool.intelligentcommunity.dao.FilterRecordEntity;
import com.eyecool.intelligentcommunity.dao.RecordEntity;
import com.eyecool.intelligentcommunity.dao.SpecialUserEntity;
import com.eyecool.intelligentcommunity.dao.UserEntity;
import com.eyecool.intelligentcommunity.entity.DeletePeopleEntity;
import com.eyecool.intelligentcommunity.entity.DeviceEntity;
import com.eyecool.intelligentcommunity.entity.DeviceParamEntity;
import com.eyecool.intelligentcommunity.entity.DeviceResult;
import com.eyecool.intelligentcommunity.entity.FeatureEntry;
import com.eyecool.intelligentcommunity.utils.Constant;
import com.eyecool.intelligentcommunity.utils.LogSaveUtil;
import com.eyecool.intelligentcommunity.view.CustomToast;
import com.eyecool.intelligentcommunity.view.IInitView;
import com.eyecool.library.utils.GsonUtils;
import com.eyecool.library.utils.Logs;
import com.eyecool.library.utils.NetworkData;
import com.eyecool.library.utils.SharedPreferencesUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observer;

/**
 * Created by ouyangfan on 2018/8/24.
 */

public class NetworkDataProcessing {

    private static final String TAG = NetworkDataProcessing.class.getSimpleName();
    private DaoManager mDaoManager;
    private Context mContext;
    private UIHandler mUIHandler = null;
    private int mUpdateIpErrorNum = -1;
    private IInitView mIInitView;

    private static NetworkDataProcessing sInstance = null;

    public static NetworkDataProcessing getInstance() {
        if (sInstance == null) {
            sInstance = new NetworkDataProcessing();
        }
        return sInstance;
    }

    private NetworkDataProcessing() {
    }

    public void init(Context context, IInitView iInitView) {
        mContext = context.getApplicationContext();
        mDaoManager = new DaoManager(context);
        mUIHandler = new UIHandler(mContext.getMainLooper());
        mIInitView = iInitView;
    }

    //获取设备参数
    public void getDevicesParam(final String deviceCode, final IInitView iInitView) {
        LogSaveUtil.d(TAG, "网络交互,获取设备参数，deviceCode" + deviceCode);
        Observer<String> observer = new Observer<String>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                LogSaveUtil.d(TAG, "网络交互,获取设备参数。网络错误，请确保设备网络良好");
                Logs.e(TAG, "获取设备参数，网络错误，请确保设备网络良好  " + e.getMessage());
//                iInitView.showDeviceCodeErrorDialog("网络错误，请确保设备网络良好");
                loadLocalFeatureWhileEmpty();
                if (iInitView != null) {
                    iInitView.initComplete();
                }
            }

            @Override
            public void onNext(String s) {
                if (!TextUtils.equals("服务器出错", s)) {
                    NetworkData<DeviceResult> networkData = GsonUtils.parseJson(s);
                    LogSaveUtil.d(TAG, "网络交互,获取设备参数。返回数据:" + s);
                    if (networkData != null && (networkData.getResCode() == 2000)) {
                        Constant.sDeviceCode = deviceCode;
                        SharedPreferencesUtils.saveString(mContext, Constant.sharedPreferencesName, "deviceCode", deviceCode);
                        String deviceResultJson = GsonUtils.toJson(networkData.getResData());
                        DeviceResult deviceResult = GsonUtils.fromJson(deviceResultJson, DeviceResult.class);
                        DeviceEntity deviceEntity = deviceResult.getDevice();
                        Constant.sDeviceId = deviceEntity.getPubId();
                        //设备参数
                        DeviceParamEntity[] deviceParamList = deviceResult.getDeviceParamList();
                        if (deviceParamList != null && deviceParamList.length > 0) {
                            for (int i = 0; i < deviceParamList.length; i++) {
                                String key = deviceParamList[i].getParamKey();
                                String value = deviceParamList[i].getParamVal();
                                if (TextUtils.isEmpty(key) || TextUtils.isEmpty(value)) {
                                    //key或者value为空，则进行下一个循环
                                    Logs.e(TAG, "设备参数为空");
                                    continue;
                                }
                                switch (key) {
                                    case "TIME_TO_CAPTURE"://每秒钟采集多少张图片
                                        try {
                                            int timeToCapture = Integer.parseInt(value);
                                            int intervalTime = 1000 / timeToCapture;
                                            CameraManager.getInstance(true).setIntervalTime(intervalTime);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        break;
                                    case "NIGHT_SCORE"://夜晚比对分值
                                        try {
                                            int nightScore = Integer.parseInt(value);
                                            Constant.sNightScore = nightScore;
                                            android.util.Log.e("huangmin", "Constant.sNightScore= " + Constant.sNightScore);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        break;
                                    case "DAY_SCORE"://白天比对分值
                                        try {
                                            int dayScore = Integer.parseInt(value);
                                            Constant.sDayScore = dayScore;
                                            android.util.Log.e("huangmin", "Constant.sDayScore= " + Constant.sDayScore);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        break;
                                    case "TIME_TO_FILTER_USER"://多少秒之内不允许同一个人重复出现
                                        try {
                                            int timeToFilterUser = Integer.parseInt(value);
                                            Constant.sTimeToFilterUser = timeToFilterUser;
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        break;
                                    case "DAY_PERIOD_TIME"://白天起始时间
                                        Constant.sDayPeriodTime = value;
                                        break;
                                    case "NIGHT_PERIOD_TIME"://晚上起始时间
                                        Constant.sNightPeriodTime = value;
                                        break;
                                }
                            }
                        }
                        Logs.i(TAG, "获取设备参数 " + deviceEntity.toString());
                        if (mIInitView != null) {
                            mIInitView.onRegCode(deviceEntity.getAuthorizationCode());
                        }
                        getUser(iInitView);
                    } else {
                        LogSaveUtil.d(TAG, "网络交互,获取设备参数,networkData = null || networkData.getResCode() !=2000,设备号错误，请查看设备号");
                        iInitView.showDeviceCodeErrorDialog("设备号错误，请查看设备号");
                    }
                } else {
                    LogSaveUtil.d(TAG, "网络交互,获取设备参数。服务器出错");
                    iInitView.showDeviceCodeErrorDialog("服务器出错");
                }
            }
        };
        Observable<String> observable = NetworkManager.getDeviceParam(deviceCode);
        observable.subscribe(observer);
    }

    //获取用户
    public void getUser(final IInitView iInitView) {
        Observer<String> observer = new Observer<String>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                if (iInitView != null) {
                    Logs.e(TAG, "获取用户，网络错误，请确保设备网络良好  " + e.getMessage());
                    iInitView.showDeviceCodeErrorDialog("网络错误，请确保设备网络良好");
                    loadLocalFeatureWhileEmpty();
                }
            }

            @Override
            public void onNext(String s) {
//                Logs.e(TAG,"getUser: " + s);
                if (!TextUtils.equals("服务器出错", s)) {
                    NetworkData<ArrayList<UserEntity>> networkData = GsonUtils.parseJson(s);
                    if (networkData != null && (networkData.getResCode() == 2000)) {
//                        Logs.e(TAG,networkData.toString());
                        ArrayList<UserEntity> list = networkData.getResData();
//                        Logs.e(TAG,list.size() + "  " + list.toString());
                        Log.e(TAG, "getUser: " + networkData.toString() + ", listSize=" + list.size());
                        if (list == null) {
                            Logs.e(TAG, "解析失败");
                            return;
                        }
                        if (list.size() == 0) {
                            Logs.e(TAG, "无人员数据更新");
                            getSpecialUser(iInitView);
                            return;
                        }
                        Logs.e(TAG, "有人员数据");
                        String taskIds = "";
                        for (int i = 0; i < list.size(); i++) {
                            //因为list里面属于泛型，gson解析的时候不知道里面的类型，所以必须再进行一次类型确定
                            String json = GsonUtils.toJson(list.get(i));
                            UserEntity userEntity = GsonUtils.fromJson(json, UserEntity.class);
                            mDaoManager.insertUser(userEntity);
                            taskIds = taskIds + userEntity.getTaskId();
                            taskIds = taskIds + ",";
                        }
                        taskIds = taskIds.substring(0, taskIds.length() - 1);//把最后一个逗号去掉
                        responseBatch(taskIds, "用户数据");
                        int surplusCount = networkData.getSurplusCount();
                        if (surplusCount > 0) {//如果剩余数量大于0，则继续获取用户
                            getUser(iInitView);
                        } else {
                            getSpecialUser(iInitView);
                        }
                    } else {
                        DatabaseProcessing databaseProcessing = new DatabaseProcessing(mContext);
                        databaseProcessing.addFeature();
                        if (iInitView != null) {
                            iInitView.initComplete();
                        }
                    }
                } else {
                    if (iInitView != null) {
                        iInitView.showDeviceCodeErrorDialog("服务器出错");
                    }
                }
            }
        };
        Observable<String> observable = NetworkManager.getUser();
        observable.subscribe(observer);
    }

    //获取特殊人员，包括红黑名单
    public void getSpecialUser(final IInitView iInitView) {
        Logs.i(TAG, "获取红黑名单");
        Observer<String> observer = new Observer<String>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                if (iInitView != null) {
                    Logs.e(TAG, "获取红黑名单，网络错误，请确保设备网络良好  " + e.getMessage());
//                    iInitView.showDeviceCodeErrorDialog("网络错误，请确保设备网络良好");
                    loadLocalFeatureWhileEmpty();
                }
            }

            @Override
            public void onNext(String s) {
                if (!TextUtils.equals("服务器出错", s)) {
                    NetworkData<ArrayList<SpecialUserEntity>> networkData = GsonUtils.parseJson(s);
                    if (networkData != null && (networkData.getResCode() == 2000)) {
//                        Logs.e(TAG,networkData.toString());
                        ArrayList<SpecialUserEntity> list = networkData.getResData();
//                        Logs.e(TAG,list.size() + "  " + list.toString());
                        Log.e(TAG, "getSpecialUser: " + networkData.toString() + ", listSize=" + list.size());
                        if (list == null) {
                            return;
                        }
                        if (list.size() == 0) {
                            getFeature(iInitView);
                            return;
                        }
                        String taskIds = "";
                        List<FeatureEntry> featureEntries = Constant.sFeatureEntries;
                        Log.e(TAG, "add feature: 添加黑名单前size=" + featureEntries.size());
                        for (int i = 0; i < list.size(); i++) {
                            //因为list里面属于泛型，gson解析的时候不知道里面的类型，所以必须再进行一次类型确定
                            String json = GsonUtils.toJson(list.get(i));
                            SpecialUserEntity specialUserEntity = GsonUtils.fromJson(json, SpecialUserEntity.class);
                            mDaoManager.insertSpecialUser(specialUserEntity);
                            taskIds = taskIds + specialUserEntity.getTaskId();
                            taskIds = taskIds + ",";
                            FeatureEntry featureEntry = new FeatureEntry();
                            String faceCode = specialUserEntity.getFaceCode();
                            featureEntry.setFaceCode(faceCode);
                            byte[] feature = Base64.decode(specialUserEntity.getFeature(), Base64.DEFAULT);
                            featureEntry.setFeature(feature);
                            featureEntry.setPeopleCode(specialUserEntity.getPeopleCode());
//                            Constant.sFeatureEntries.add(featureEntry);
                            featureEntries.add(featureEntry);

                        }
                        Log.e(TAG, "add feature: 添加黑名单后size=" + featureEntries.size());
                        taskIds = taskIds.substring(0, taskIds.length() - 1);//把最后一个逗号去掉
                        responseBatch(taskIds, "红黑名单数据");
                        int surplusCount = networkData.getSurplusCount();
                        if (surplusCount > 0) {//如果剩余数量大于0，则继续获取用户
                            getUser(iInitView);
                        } else {
                            Logs.i(TAG, "获取特征...");
                            getFeature(iInitView);
                        }
                    } else {
                        DatabaseProcessing databaseProcessing = new DatabaseProcessing(mContext);
                        databaseProcessing.addFeature();
                        if (iInitView != null) {
                            iInitView.initComplete();
                        }
                    }
                } else {
                    if (iInitView != null) {
                        iInitView.showDeviceCodeErrorDialog("服务器出错");
                    }
                }
            }
        };
        Observable<String> observable = NetworkManager.getSpecialUser();
        observable.subscribe(observer);
    }

    //获取特征值
    public void getFeature(final IInitView iInitView) {
        Logs.e(TAG, "获取特征值");
        Observer<String> observer = new Observer<String>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                if (iInitView != null) {
                    Logs.e(TAG, "获取特征值，网络错误，请确保设备网络良好  " + e.getMessage());
//                    iInitView.showDeviceCodeErrorDialog("网络错误，请确保设备网络良好");
                    loadLocalFeatureWhileEmpty();
                }
            }

            @Override
            public void onNext(String s) {
                if (!TextUtils.equals("服务器出错", s)) {
                    NetworkData<ArrayList<FeatureEntity>> networkData = GsonUtils.parseJson(s);
                    if (networkData != null && (networkData.getResCode() == 2000)) {
//                        Logs.e(TAG,networkData.toString());
                        ArrayList<FeatureEntity> list = networkData.getResData();
//                        Logs.e(TAG,list.size() + "  " + list.toString());
                        Log.e(TAG, "getFeature: " + networkData.toString() + ", listSize=" + list.size());
                        if (list == null) {
                            return;
                        }
                        if (list.size() == 0) {
                            DatabaseProcessing databaseProcessing = new DatabaseProcessing(mContext);
                            Constant.sFeatureEntries.clear();
                            databaseProcessing.addFeature();
                            if (iInitView != null) {
                                iInitView.initComplete();
                            }
                            return;
                        }
                        Logs.e(TAG, "有特征数据");
                        String taskIds = "";

//                        ArrayList<FeatureEntry> featureEntries = Constant.sFeatureEntries;
                        List<FeatureEntry> featureEntries = Constant.sFeatureEntries;
                        Log.e(TAG, "add feature: 原始size=" + featureEntries.size());
                        List<FeatureEntry> tmpList = new ArrayList<>();
                        for (int i = 0; i < list.size(); i++) {
                            //因为list里面属于泛型，gson解析的时候不知道里面的类型，所以必须再进行一次类型确定
                            String json = GsonUtils.toJson(list.get(i));
                            FeatureEntity featureEntity = GsonUtils.fromJson(json, FeatureEntity.class);
                            String pubId = featureEntity.getPubId();
                            mDaoManager.insertFeatureEntity(featureEntity);
                            taskIds = taskIds + featureEntity.getTaskId();
                            taskIds = taskIds + ",";
                            int faceStaus = featureEntity.getFaceStatus();
                            String peopleCode = featureEntity.getPeopleCode();
                            if (faceStaus == 1) {//人员状态，0：禁用1：启用2：待审核
                                String feature = featureEntity.getFeature();
                                byte[] featureByte = null;
                                try {
                                    featureByte = Base64.decode(feature, Base64.DEFAULT);
                                } catch (Exception e) {
                                    int taskId = featureEntity.getTaskId();
                                    Log.e(TAG, "decode failed, taskId = " + taskId);
                                }
                                FeatureEntry featureEntry = new FeatureEntry();
                                featureEntry.setFeature(featureByte);//用来识别
                                featureEntry.setPeopleCode(peopleCode);//用来找user表中的人，对应user表的pubId
                                featureEntry.setFaceCode(pubId);//用来上传开门记录
                                Logs.e(TAG, "add feature: " + featureEntry.toString());
//                                int sameCount = 0;
//                                for (FeatureEntry entry : featureEntries) {
//                                    String faceCode = entry.getFaceCode();
//                                    if (faceCode.equals(pubId)) {
//                                        //相同跳过
//                                        sameCount++;
//                                        Log.e(TAG, "filter feature: same");
//                                        break;
//                                    }
//                                }
//                                if (sameCount == 0) {
//                                    tmpList.add(featureEntry);
//                                }
                                featureEntries.add(featureEntry);
                            } else {
                                FeatureEntry entry = new FeatureEntry();
                                entry.setFaceCode(pubId);
                                entry.setPeopleCode(peopleCode);
                                featureEntries.remove(entry);
                            }
                        }
//                        featureEntries.addAll(tmpList);
                        Log.e(TAG, "add feature: 添加后size=" + featureEntries.size());
                        taskIds = taskIds.substring(0, taskIds.length() - 1);//把最后一个逗号去掉
                        responseBatch(taskIds, "特征值数据");
                        int surplusCount = networkData.getSurplusCount();
                        Logs.e(TAG, "surplusCount = " + surplusCount);
                        if (surplusCount > 0) {//如果剩余数量大于0，则继续获取用户
                            getFeature(iInitView);
                        } else {
                            DatabaseProcessing databaseProcessing = new DatabaseProcessing(mContext);
                            databaseProcessing.addFeature();
                            if (iInitView != null) {
                                iInitView.initComplete();
                            }
                        }
                    } else {
                        DatabaseProcessing databaseProcessing = new DatabaseProcessing(mContext);
                        databaseProcessing.addFeature();
                        if (iInitView != null) {
                            iInitView.initComplete();
                        }
                    }
                } else {
                    if (iInitView != null) {
                        iInitView.showDeviceCodeErrorDialog("服务器出错");
                    }
                }
            }
        };
        Observable<String> observable = NetworkManager.getFeature();
        observable.subscribe(observer);
    }

    //上传开门记录
    public void uploadRecord(final ArrayList<RecordEntity> list) {
        Observer<String> observer = new Observer<String>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                Logs.e(TAG, "upload record:" + e.getMessage());
                for (int i = 0; i < list.size(); i++) {
                    mDaoManager.insertRecordEntity(list.get(i));
                }
            }

            @Override
            public void onNext(String s) {
                if (!TextUtils.equals("服务器出错", s)) {
                    NetworkData<String> networkData = GsonUtils.parseJson(s);
                    if (networkData != null && (networkData.getResCode() == 2000)) {
                        Logs.e(TAG, "上传开门日志成功：" + networkData);
                        String response = networkData.getResData();
                        if (response.contains(",")) {
                            String[] pubIds = response.split(",");
                            for (int i = 0; i < pubIds.length; i++) {
                                mDaoManager.deleteRecord(pubIds[i]);
                            }
                        } else {
                            mDaoManager.deleteRecord(response);
                        }
                    } else {
                        for (int i = 0; i < list.size(); i++) {
                            mDaoManager.insertRecordEntity(list.get(i));
                        }
                    }
                }
            }
        };
        RecordEntity recordEntity = list.get(0);
        Logs.i(TAG, "时间戳：" + recordEntity.getCreateTime());
        Observable<String> observable = NetworkManager.uploadRecord(GsonUtils.toJson(list));
        observable.subscribe(observer);
    }

    //上传红黑名单识别记录
    public void uploadFilterRecord(final ArrayList<FilterRecordEntity> list) {
        Observer<String> observer = new Observer<String>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                for (int i = 0; i < list.size(); i++) {
                    mDaoManager.insertFilterRecordEntity(list.get(i));
                }
            }

            @Override
            public void onNext(String s) {
                if (!TextUtils.equals("服务器出错", s)) {
                    NetworkData<String> networkData = GsonUtils.parseJson(s);
                    if (networkData != null && (networkData.getResCode() == 2000)) {
                        Logs.e(TAG, "上传红黑名单识别日志成功");
                        String response = networkData.getResData();
                        if (response.contains(",")) {
                            String[] pubIds = response.split(",");
                            for (int i = 0; i < pubIds.length; i++) {
                                mDaoManager.deleteFilterRecord(pubIds[i]);
                            }
                        } else {
                            mDaoManager.deleteFilterRecord(response);
                        }
                    } else {
                        for (int i = 0; i < list.size(); i++) {
                            mDaoManager.insertFilterRecordEntity(list.get(i));
                        }
                    }
                }
            }
        };
        Observable<String> observable = NetworkManager.uploadFilterRecord(GsonUtils.toJson(list));
        observable.subscribe(observer);
    }

    //上传下载数据反馈
    public void responseBatch(String taskIds, final String type) {
        Observer<String> observer = new Observer<String>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                Logs.e(TAG, type + "   反馈失败");
            }

            @Override
            public void onNext(String s) {
                if (!TextUtils.equals("服务器出错", s)) {
                    NetworkData<ArrayList<DeletePeopleEntity>> networkData = GsonUtils.parseJson(s);
                    if (networkData != null && (networkData.getResCode() == 2000)) {
                        Logs.e(TAG, "反馈成功");
                    } else {
                        Logs.e(TAG, type + "   反馈失败");
                    }
                }
            }
        };
        Observable<String> observable = NetworkManager.responseBatch(taskIds);
        observable.subscribe(observer);
    }

    public void deletePeople(final String tableName) {
        Observer<String> observer = new Observer<String>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(String s) {
                if (!TextUtils.equals("服务器出错", s)) {
                    Log.e(TAG, "deletePeople: " + s);
                    NetworkData<ArrayList<DeletePeopleEntity>> networkData = GsonUtils.parseJson(s);
                    if (networkData != null && (networkData.getResCode() == 2000)) {
                        ArrayList<DeletePeopleEntity> list = networkData.getResData();
                        String taskIds = "";
                        for (int i = 0; i < list.size(); i++) {
                            String data = GsonUtils.toJson(list.get(i));
                            DeletePeopleEntity deletePeopleEntity = GsonUtils.fromJson(data, DeletePeopleEntity.class);
                            String recordId = deletePeopleEntity.getRecordId();
                            switch (tableName) {
                                case "pro_people":
                                    //内存特征删除，特征表也需要删除
                                    //次数对应user表pub_id字段，对应feature表peopleCode字段
                                    mDaoManager.deleteUser(recordId);
//                                    deleteFeature(recordId);
                                    mDaoManager.deleteFeatureByPeopleCode(recordId);
                                    deleteFeatureInMemory2(recordId);
                                    break;
                                case "pro_filter_list":
                                    mDaoManager.deleteSpecialUser(recordId);
                                    break;
                                case "pro_face_feature":
//                                    deleteFeature(recordId);
                                    mDaoManager.deleteFeature(recordId);
                                    deleteFeatureInMemory(recordId);
                                    break;
                            }
                            taskIds = taskIds + deletePeopleEntity.getTaskId();
                            taskIds = taskIds + ",";

                            deleteRecord(recordId);
                        }
                        taskIds = taskIds.substring(0, taskIds.length() - 1);//把最后一个逗号去掉
                        responseBatch(taskIds, "特征值数据");
                    }
                }
            }
        };
        Observable<String> observable = NetworkManager.deletePeople(tableName);
        observable.subscribe(observer);
    }

    private void deleteFeatureInMemory(String pubId) {
        List<FeatureEntry> removeEntries = new ArrayList<>();
        List<FeatureEntry> entries = Constant.sFeatureEntries;
        Logs.e(TAG, "删除前特征size=" + entries.size());
        for (FeatureEntry entry : entries) {
            //查找对应pubId
            if (pubId.equals(entry.getFaceCode())) {
                removeEntries.add(entry);
            }
        }
        entries.removeAll(removeEntries);
        Logs.e(TAG, "删除后特征size=" + entries.size());
    }

    private void deleteFeatureInMemory2(String peopleCode) {
        List<FeatureEntry> removeEntries = new ArrayList<>();
        List<FeatureEntry> entries = Constant.sFeatureEntries;
        Logs.e(TAG, "删除前特征size=" + entries.size());
        for (FeatureEntry entry : entries) {
            //查找对应pubId
            if (peopleCode.equals(entry.getPeopleCode())) {
                removeEntries.add(entry);
            }
        }
        entries.removeAll(removeEntries);
        Logs.e(TAG, "删除后特征size=" + entries.size());
    }

    //删除开门记录
    private void deleteRecord(String recordId) {
        //删除白名单开门记录
        mDaoManager.deleteRecordByPeopleCode(recordId);
        //删除红黑名单开门记录
        mDaoManager.deleteFilterRecord(recordId);
    }

//    public void deleteFeature(String pubId) {
////        Iterator<FeatureEntry> iterator = Constant.sFeatureEntries.iterator();
//        Iterator<Map.Entry<String, FeatureEntry>> iterator = Constant.sFeatureEntries.entrySet().iterator();
//        while (iterator.hasNext()) {
//            Map.Entry<String, FeatureEntry> next = iterator.next();
//            String pid = next.getKey();
//            if (TextUtils.equals(pid, pubId)) {
//                Logs.e(TAG,"删除特征pid： " + pid);
//                mDaoManager.deleteFeature(pubId);
//            }
//        }
//    }

    public void updateIp(final String ip) {
        Observer<String> observer = new Observer<String>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                Logs.e(TAG, "onError..." + e.getMessage() + " errorNum:" + mUpdateIpErrorNum);
                mUpdateIpErrorNum++;
                Message msg = mUIHandler.obtainMessage();
                msg.obj = ip;
                if (mUpdateIpErrorNum < 10) {
                    mUIHandler.sendMessageDelayed(msg, 3000);
                }
            }

            @Override
            public void onNext(String s) {
                if (!TextUtils.equals("服务器出错", s)) {
                    NetworkData<ArrayList<DeletePeopleEntity>> networkData = GsonUtils.parseJson(s);
                    if (networkData != null && (networkData.getResCode() == 2000)) {
                        Constant.sLocalIp = ip;
                        Logs.e(TAG, "更新ip成功:" + ip);
                        mUpdateIpErrorNum = 0;
                    }
                }
            }
        };
        Observable<String> observable = NetworkManager.updateIp(ip);
        observable.subscribe(observer);
    }

    //检查应用升级
    public void checkApkUpDate() {
        LogSaveUtil.d(TAG, "检查更新");
        Observer<String> observer = new Observer<String>() {
            @Override
            public void onCompleted() {
                LogSaveUtil.d(TAG, "检查更新onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                LogSaveUtil.d(TAG, "检查更新:" + e.getMessage());
            }

            @Override
            public void onNext(String s) {
                LogSaveUtil.d(TAG, "检查更新: " + s);
                if (!TextUtils.equals("服务器出错", s)) {
                    NetworkData<ApkUpdateEntity> networkData = GsonUtils.parseJson(s);
                    if (networkData != null && (networkData.getResCode() == 2000)) {
                        LogSaveUtil.d(TAG, "获取升级信息成功");
                        String deviceResultJson = GsonUtils.toJson(networkData.getResData());
                        ApkUpdateEntity apkUpdateEntity = GsonUtils.fromJson(deviceResultJson, ApkUpdateEntity.class);
                        if (apkUpdateEntity.getAppType() == 2) {//1，一体机或摆闸，2，门禁平板，3，访客机
                            if (Integer.parseInt(apkUpdateEntity.getVer()) > BuildConfig.VERSION_CODE) {
                                LogSaveUtil.d(TAG, "满足升级条件准备进行升级,发送消息");
                                EventBus.getDefault().post(new EventMsg(apkUpdateEntity.getFilePath(),EventMsg.ACTION_START_UPDATE));
                            } else {
                                Toast.makeText(mContext, "已是最新版本", Toast.LENGTH_LONG).show();
                                LogSaveUtil.d(TAG, "版本号错误，不进行升级");
                            }
                        }else {
                            LogSaveUtil.d(TAG, "type != 2  !!!");
                        }
                    }else {
                        LogSaveUtil.d(TAG, "networkData = "+networkData+" getResCode :"+networkData.getResCode() );
                    }
                }
            }
        };
        Observable<String> observable = NetworkManager.checkApkUpgrade();
        observable.subscribe(observer);
    }

    /**
     * 加载本地特征库
     */
    private void loadLocalFeatureWhileEmpty() {
        if (Constant.sFeatureEntries.size() == 0) {
            DatabaseProcessing databaseProcessing = new DatabaseProcessing(mContext);
            Constant.sFeatureEntries.clear();
            databaseProcessing.addFeature();
        }
    }

    private class UIHandler extends Handler {

        public UIHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String ip = (String) msg.obj;
            Logs.i(TAG, "ip:" + ip);
            updateIp(ip);
        }
    }
}
