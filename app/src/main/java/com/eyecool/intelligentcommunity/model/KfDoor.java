package com.eyecool.intelligentcommunity.model;

import com.eyecool.library.algorithm.door.IDoor;
import com.eyecool.library.utils.Logs;
import com.eyecool.library.utils.StringUtils;
import com.eyecool.serialport.SerialPortUtils;

/**
 * Created by ouyangfan on 2018/8/30.
 * 科发设备开门类（开门要根据不同设备创建不同的类）
 */

public class KfDoor implements IDoor {


    private static KfDoor sDoor;
    private SerialPortUtils mSerialPortUtils;

    private String path = "/dev/ttyS4";
    private int baudrate = 9600;

    public static KfDoor getInstance() {
        if (sDoor == null) {
            synchronized (KfDoor.class) {
                if (sDoor == null) {
                    sDoor = new KfDoor();
                }
            }
        }
        return sDoor;
    }

    private KfDoor() {
        mSerialPortUtils = new SerialPortUtils();
        mSerialPortUtils.openSerialPort(baudrate, path);
    }

    @Override
    public void open() {
        //把门关上
        //拉低继电器，把门打开
        byte[] openCmd = StringUtils.hexStringToByteArray("00A7030501A0");
        mSerialPortUtils.sendSerialPort(openCmd);
    }

    @Override
    public void close() {
        byte[] closeCmd = StringUtils.hexStringToByteArray("00A7030500A1");
        mSerialPortUtils.sendSerialPort(closeCmd);
        Logs.e("关闭了");
    }

    public void openLight() {
        //把绿灯打开
        byte[] turnGreenLight = StringUtils.hexStringToByteArray("00A7030101A7");
        mSerialPortUtils.sendSerialPort(turnGreenLight);
    }

    public void closeLight() {
        byte[] closeCmd = StringUtils.hexStringToByteArray("00A7030100A7");
        mSerialPortUtils.sendSerialPort(closeCmd);
    }
}
