package com.eyecool.intelligentcommunity.model;

import android.content.Context;
import android.text.TextUtils;

import com.eyecool.intelligentcommunity.MyApp;
import com.eyecool.intelligentcommunity.utils.Constant;
import com.eyecool.library.BaseApplication;
import com.eyecool.library.http.HttpManager;
import com.eyecool.library.utils.SharedPreferencesUtils;

import java.util.HashMap;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ouyangfan on 2018/8/23.
 */

public class NetworkManager {

  public static HttpManager httpManager = new HttpManager(15, 15);

  public static Observable<String> getDeviceParam(final String deviceCode) {
    final HashMap<String, Object> hashMap = new HashMap<>();
    hashMap.put("apiKey", "syn_download_device");
    hashMap.put("deviceType", "2");
    hashMap.put("deviceCode", deviceCode);

    return Observable.create(new Observable.OnSubscribe<String>() {
      @Override
      public void call(Subscriber<? super String> subscriber) {
        httpManager.post(getUrl(), hashMap, subscriber);
      }
    }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
  }

  public static Observable<String> getUser() {
    HashMap<String, Object> hashMap = new HashMap<>();
    hashMap.put("pageSize", "10");
    hashMap.put("apiKey", "syn_download_people");
    return execute(hashMap);
  }

  public static Observable<String> getSpecialUser() {
    HashMap<String, Object> hashMap = new HashMap<>();
    hashMap.put("pageSize", "10");
    hashMap.put("apiKey", "syn_download_filter_list");
    return execute(hashMap);
  }

  public static Observable<String> getFeature() {
    HashMap<String, Object> hashMap = new HashMap<>();
    hashMap.put("pageSize", "10");
    hashMap.put("apiKey", "syn_download_face");
    return execute(hashMap);
  }

  public static Observable<String> uploadRecord(String data) {
    HashMap<String, Object> hashMap = new HashMap<>();
    hashMap.put("apiKey", "syn_upload_record");
    hashMap.put("synData", data);
    return execute(hashMap);
  }

  public static Observable<String> uploadFilterRecord(String data) {
    HashMap<String, Object> hashMap = new HashMap<>();
    hashMap.put("apiKey", "syn_upload_filter_record");
    hashMap.put("synData", data);
    return execute(hashMap);
  }

  public static Observable<String> deletePeople(String tableName) {
    HashMap<String, Object> hashMap = new HashMap<>();
    hashMap.put("apiKey", "syn_delete");
    hashMap.put("tableName", tableName);
    return execute(hashMap);
  }

  public static Observable<String> recoveryDevice() {
    HashMap<String, Object> hashMap = new HashMap<>();
    hashMap.put("apiKey", "syn_recovery_device");
    return execute(hashMap);
  }

  public static Observable<String> updateIp(String ip) {
    HashMap<String, Object> hashMap = new HashMap<>();
    hashMap.put("apiKey", "syn_update_ip");
    hashMap.put("deviceIp", ip);
    hashMap.put("operationCode", (MyApp.sRunCode == null || "".equals(MyApp.sRunCode)) ? "未获取运行码" : MyApp.sRunCode);

    return execute(hashMap);
  }

  public static Observable<String> responseBatch(String taskIds) {
    HashMap<String, Object> hashMap = new HashMap<>();
    hashMap.put("apiKey", "syn_response_batch");
    hashMap.put("taskIds", taskIds);
    return execute(hashMap);
  }


  public static Observable<String> execute(final HashMap<String, Object> hashMap) {
    hashMap.put("deviceType", "2");
    hashMap.put("deviceCode", Constant.sDeviceCode);
    return Observable.create(new Observable.OnSubscribe<String>() {
      @Override
      public void call(Subscriber<? super String> subscriber) {
        httpManager.post(getUrl(), hashMap, subscriber);
      }
    }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
  }

  //设备程序更新
  public static Observable<String> checkApkUpgrade() {
    HashMap<String, Object> hashMap = new HashMap<>();
    hashMap.put("apiKey", "syn_update_app");
    return execute(hashMap);
  }


  private static String getUrl() {
    String url = "";

    Context context = BaseApplication.get();
    String mIp = SharedPreferencesUtils.getString(context, Constant.sharedPreferencesName, "ip");
    if (TextUtils.isEmpty(mIp)) {
      mIp = Constant.sIp;
    }
    String mPort = SharedPreferencesUtils.getString(context, Constant.sharedPreferencesName, "port");
    if (TextUtils.isEmpty(mPort)) {
      mPort = Constant.sPort;
    }

    url = mIp + ":" + mPort + Constant.sInterface;

    return url;
  }
}
