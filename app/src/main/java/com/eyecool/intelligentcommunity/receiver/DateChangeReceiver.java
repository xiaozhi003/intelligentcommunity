package com.eyecool.intelligentcommunity.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.eyecool.intelligentcommunity.utils.WhiteLightManager;
import com.eyecool.library.utils.Logs;

/**
 * @author : yan
 * @date : 2018/10/19 16:37
 * @desc : todo
 */
public class DateChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Logs.e("日期变化了~");
        //白灯变化处理
        WhiteLightManager.handleFaceCameraLight(context);
    }
}
