package com.eyecool.intelligentcommunity.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import com.eyecool.intelligentcommunity.model.NetworkDataProcessing;
import com.eyecool.intelligentcommunity.utils.Constant;
import com.eyecool.library.utils.Logs;
import com.eyecool.library.utils.SystemUtils;

/**
 * Created by ouyangfan on 2018/8/30.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {

    private static final String TAG = NetworkChangeReceiver.class.getSimpleName();

    @Override
    public void onReceive(final Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isAvailable()) {
            String ip = SystemUtils.getIp(context);
            if (!TextUtils.isEmpty(ip) && !TextUtils.equals(ip, Constant.sLocalIp)) {
                Logs.i(TAG,"NetworkChangeReceiver...");
                NetworkDataProcessing networkDataProcessing = NetworkDataProcessing.getInstance();
                networkDataProcessing.updateIp(ip);
            }
        }
    }
}
