package com.eyecool.intelligentcommunity.receiver;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.eyecool.intelligentcommunity.model.ZHTDoor;
import com.eyecool.intelligentcommunity.utils.AlarmManagerUtils;
import com.eyecool.intelligentcommunity.utils.Constant;
import com.eyecool.library.utils.SharedPreferencesUtils;

/**
 * Created by huangmin on 2018/6/4.
 */

public class AlarmBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "MyBroadcastReceiver";

    @SuppressLint("NewApi")
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "onReceive: 定时广播");
        int reqCode = intent.getIntExtra("reqCode", -1);
        switch (reqCode) {
            case Constant.ALARM_OPEN_LIGHT:
                //开灯
                String openTime = SharedPreferencesUtils.getString(context, Constant.sharedPreferencesName, SharedPreferencesUtils.KEY_CAMERA_LIGHT_START_TIME);
                if (TextUtils.isEmpty(openTime)) {
                    openTime = Constant.DEFAULT_OPEN_LIGHT_TIME;
                }
                AlarmManagerUtils.getInstance(context).startRemind(openTime, reqCode);
                ZHTDoor.getInstance().openFaceCameraLight();
                break;
            case Constant.ALARM_CLOSE_LIGHT:
                //关灯
                String closeTime = SharedPreferencesUtils.getString(context, Constant.sharedPreferencesName, SharedPreferencesUtils.KEY_CAMERA_LIGHT_CLOSE_TIME);
                if (TextUtils.isEmpty(closeTime)) {
                    closeTime = Constant.DEFAULT_CLOSE_LIGHT_TIME;
                }
                AlarmManagerUtils.getInstance(context).startRemind(closeTime, reqCode);
                ZHTDoor.getInstance().closeFaceCameraLight();
                break;
        }
//        AlarmManagerUtils.getInstance(context).getUpAlarmManagerWorkOnReceiver(reqCode, 3 * 60 * 1000);
    }
}
