package com.eyecool.intelligentcommunity.dao;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by ouyangfan on 2018/8/25.
 */
@Entity
public class SpecialUserEntity{
    @Property(nameInDb = "faceImgBase64")
    private String faceImgBase64;
    @Property(nameInDb = "proCode")
    private String proCode;
    @Property(nameInDb = "feature")
    private String feature;
    @Property(nameInDb = "faceCode")
    private String faceCode;
    @Property(nameInDb = "peopleCode")
    private String peopleCode;
    @Id
    private String pubId;
    @Property(nameInDb = "taskId")
    private int taskId;
    @Property(nameInDb = "filterType")
    private int filterType;
    @Property(nameInDb = "filterStatus")
    private int filterStatus;
    @Property(nameInDb = "createTime")
    private long createTime;
    public long getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }
    public int getFilterStatus() {
        return this.filterStatus;
    }
    public void setFilterStatus(int filterStatus) {
        this.filterStatus = filterStatus;
    }
    public int getFilterType() {
        return this.filterType;
    }
    public void setFilterType(int filterType) {
        this.filterType = filterType;
    }
    public int getTaskId() {
        return this.taskId;
    }
    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }
    public String getPubId() {
        return this.pubId;
    }
    public void setPubId(String pubId) {
        this.pubId = pubId;
    }
    public String getPeopleCode() {
        return this.peopleCode;
    }
    public void setPeopleCode(String peopleCode) {
        this.peopleCode = peopleCode;
    }
    public String getFaceCode() {
        return this.faceCode;
    }
    public void setFaceCode(String faceCode) {
        this.faceCode = faceCode;
    }
    public String getFeature() {
        return this.feature;
    }
    public void setFeature(String feature) {
        this.feature = feature;
    }
    public String getProCode() {
        return this.proCode;
    }
    public void setProCode(String proCode) {
        this.proCode = proCode;
    }
    public String getFaceImgBase64() {
        return this.faceImgBase64;
    }
    public void setFaceImgBase64(String faceImgBase64) {
        this.faceImgBase64 = faceImgBase64;
    }
    @Generated(hash = 960956367)
    public SpecialUserEntity(String faceImgBase64, String proCode, String feature,
            String faceCode, String peopleCode, String pubId, int taskId,
            int filterType, int filterStatus, long createTime) {
        this.faceImgBase64 = faceImgBase64;
        this.proCode = proCode;
        this.feature = feature;
        this.faceCode = faceCode;
        this.peopleCode = peopleCode;
        this.pubId = pubId;
        this.taskId = taskId;
        this.filterType = filterType;
        this.filterStatus = filterStatus;
        this.createTime = createTime;
    }
    @Generated(hash = 1250300228)
    public SpecialUserEntity() {
    }

}
