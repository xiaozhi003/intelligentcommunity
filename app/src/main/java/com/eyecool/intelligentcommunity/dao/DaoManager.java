package com.eyecool.intelligentcommunity.dao;

import android.content.Context;
import android.database.Cursor;

import com.eyecool.library.utils.Logs;

import java.util.List;

/**
 * Created by ouyangfan on 2018/9/8.
 */

public class DaoManager {

  private static final String TAG = DaoManager.class.getSimpleName();

  private DaoSession mDaoSession;

  //数据库缓存记录最大数
  private static final int MAX_RECORD_NUM = 100;

  public DaoManager(Context context) {
    DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(context, "intelligentcommunity", null);
    DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDatabase());
    mDaoSession = daoMaster.newSession();
  }

  //人员表
  public void insertUser(UserEntity userEntity) {
    UserEntity userEntity1 = getUserEntity(userEntity.getPubId());
    if (userEntity1 == null) {
      mDaoSession.getUserEntityDao().insert(userEntity);
    } else {
      mDaoSession.getUserEntityDao().update(userEntity);
    }
  }

  public UserEntity getUserEntity(String pubId) {
    return mDaoSession.getUserEntityDao().queryBuilder().where(UserEntityDao.Properties.PubId.eq(pubId)).build().unique();
  }

  /**
   * 获取可用
   */
  public UserEntity getUserEntityUsed(String pubId) {
    return mDaoSession.getUserEntityDao().queryBuilder().where(UserEntityDao.Properties.PubId.eq(pubId), UserEntityDao.Properties.PeopleStatus.eq(1)).build().unique();
  }

  public void deleteUser(String recordId) {
    UserEntity userEntity = getUserEntity(recordId);
    if (userEntity != null) {
      mDaoSession.getUserEntityDao().delete(userEntity);
    }
  }

  //红黑名单表
  public void insertSpecialUser(SpecialUserEntity specialUserEntity) {
    SpecialUserEntity specialUserEntity1 = getSpecialUserEntity(specialUserEntity.getPubId());
    if (specialUserEntity1 == null) {
      mDaoSession.getSpecialUserEntityDao().insert(specialUserEntity);
    } else {
      mDaoSession.getSpecialUserEntityDao().update(specialUserEntity);
    }
  }

  public SpecialUserEntity getSpecialUserEntity(String pubId) {
    return mDaoSession.getSpecialUserEntityDao().queryBuilder().where(SpecialUserEntityDao.Properties.PubId.eq(pubId)).build().unique();
  }

  public void deleteSpecialUser(String recordId) {
    SpecialUserEntity specialUserEntity = getSpecialUserEntity(recordId);
    if (specialUserEntity != null) {
      mDaoSession.getSpecialUserEntityDao().delete(specialUserEntity);
    }
  }

  public SpecialUserEntity getSpecialUserEntityByPeopleCode(String peopleCode) {
    return mDaoSession.getSpecialUserEntityDao().queryBuilder().where(SpecialUserEntityDao.Properties.PeopleCode.eq(peopleCode)).build().unique();
  }

  public List<SpecialUserEntity> getSpecialUserEntitiesByPeopleCode(String peopleCode) {
    return mDaoSession.getSpecialUserEntityDao().queryBuilder().where(SpecialUserEntityDao.Properties.PeopleCode.eq(peopleCode)).build().list();
  }

  //查找启动的黑名单列表数据
  public List<SpecialUserEntity> getSpecialUserEntitiesUsed() {
    return mDaoSession.getSpecialUserEntityDao().queryBuilder().where(SpecialUserEntityDao.Properties.FilterStatus.eq(1)).build().list();
  }

  //特征表
  public void insertFeatureEntity(FeatureEntity featureEntity) {
    FeatureEntity featureEntity1 = getFeatureEntity(featureEntity.getPubId());
    if (featureEntity1 == null) {
      mDaoSession.getFeatureEntityDao().insert(featureEntity);
    } else {
      mDaoSession.getFeatureEntityDao().update(featureEntity);
    }
  }

  public FeatureEntity getFeatureEntity(String pubId) {
    return mDaoSession.getFeatureEntityDao().queryBuilder().where(FeatureEntityDao.Properties.PubId.eq(pubId)).build().unique();
  }

  public List<FeatureEntity> getFeatureEntityByPeopleCode(String peopleCode) {
    return mDaoSession.getFeatureEntityDao().queryBuilder().where(FeatureEntityDao.Properties.PeopleCode.eq(peopleCode)).build().list();
  }

  public void deleteFeature(String pubId) {
    FeatureEntity featureEntity = getFeatureEntity(pubId);
    if (featureEntity != null) {
      mDaoSession.getFeatureEntityDao().delete(featureEntity);
    }
  }

  public void deleteFeatureByPeopleCode(String peopleCode) {
    List<FeatureEntity> featureEntities = getFeatureEntityByPeopleCode(peopleCode);
    for (FeatureEntity featureEntity : featureEntities) {
      if (featureEntity != null) {
        mDaoSession.getFeatureEntityDao().delete(featureEntity);
      }
    }
  }

  public List<FeatureEntity> getFeatureEntities() {
    return mDaoSession.getFeatureEntityDao().queryBuilder().build().list();
  }

  //开门记录表
  public void insertRecordEntity(RecordEntity recordEntity) {
    RecordEntity RecordEntity1 = getRecordEntity(recordEntity.getPubId());

    if (RecordEntity1 == null) {
      new Thread() {
        @Override
        public void run() {
          checkRecordSize();
        }
      }.start();

      mDaoSession.getRecordEntityDao().insert(recordEntity);
    } else {
      mDaoSession.getRecordEntityDao().update(recordEntity);
    }
  }


  private long GetAllDataCount() {
    long count = 0;
    String sql = "select count(*) from RECORD_ENTITY";
    Cursor cursor = mDaoSession.getRecordEntityDao().getDatabase().rawQuery(sql, null);
    if (cursor.moveToFirst()) {
      count = cursor.getLong(0);
    }


    cursor.close();
    return count;

  }

  private void checkRecordSize() {
    long size = GetAllDataCount();
    Logs.i(TAG, "record size:" + size);
    if (size >= MAX_RECORD_NUM) {
      String puid = "";
      String sql = "select * from RECORD_ENTITY";
      Cursor cursor = mDaoSession.getRecordEntityDao().getDatabase().rawQuery(sql, null);
      if (cursor.moveToFirst()) {
        puid = cursor.getString(cursor.getColumnIndex("PUB_ID"));
      }

      RecordEntity recordEntity = getRecordEntity(puid);
      if (recordEntity != null) {
        mDaoSession.getRecordEntityDao().delete(recordEntity);
      }
      cursor.close();
    }
  }

  public RecordEntity getRecordEntity(String pubId) {
    return mDaoSession.getRecordEntityDao().queryBuilder().where(RecordEntityDao.Properties.PubId.eq(pubId)).build().unique();
  }

  public RecordEntity getRecordEntityByPeopleCode(String peopleCode) {
    return mDaoSession.getRecordEntityDao().queryBuilder().where(RecordEntityDao.Properties.PeopleCode.eq(peopleCode)).build().unique();
  }

  public void deleteRecord(String pubId) {
    RecordEntity recordEntity = getRecordEntity(pubId);
    if (recordEntity != null) {
      mDaoSession.getRecordEntityDao().delete(recordEntity);
    }
  }

  public void deleteAllRecord() {
    mDaoSession.getRecordEntityDao().deleteAll();
  }

  public void deleteRecordByPeopleCode(String peopleCode) {
    RecordEntity recordEntity = getRecordEntityByPeopleCode(peopleCode);
    if (recordEntity != null) {
      mDaoSession.getRecordEntityDao().delete(recordEntity);
    }
  }

  public List<RecordEntity> getRecordEntities() {
    return mDaoSession.getRecordEntityDao().queryBuilder().build().list();
  }

  //红黑名单识别记录
  public void insertFilterRecordEntity(FilterRecordEntity filterRecordEntity) {
    FilterRecordEntity filterRecordEntity1 = getFilterRecordEntity(filterRecordEntity.getPubId());
    if (filterRecordEntity1 == null) {
      mDaoSession.getFilterRecordEntityDao().insert(filterRecordEntity);
    } else {
      mDaoSession.getFilterRecordEntityDao().update(filterRecordEntity);
    }
  }

  public FilterRecordEntity getFilterRecordEntity(String pubId) {

    return mDaoSession.getFilterRecordEntityDao().queryBuilder().where(FilterRecordEntityDao.Properties.PubId.eq(pubId)).build().unique();
  }

  public void deleteFilterRecord(String pubId) {
    FilterRecordEntity filterRecordEntity = getFilterRecordEntity(pubId);
    if (filterRecordEntity != null) {
      mDaoSession.getFilterRecordEntityDao().delete(filterRecordEntity);
    }
  }

  public List<FilterRecordEntity> getFilterRecordEntityEntities() {
    return mDaoSession.getFilterRecordEntityDao().queryBuilder().build().list();
  }

  public void clearAllTable() {
    mDaoSession.getUserEntityDao().deleteAll();
    mDaoSession.getSpecialUserEntityDao().deleteAll();
    mDaoSession.getFeatureEntityDao().deleteAll();
    mDaoSession.getRecordEntityDao().deleteAll();
    mDaoSession.getFilterRecordEntityDao().deleteAll();
  }
}
