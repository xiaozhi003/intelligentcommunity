package com.eyecool.intelligentcommunity.dao;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by ouyangfan on 2018/8/28.
 */
@Entity
public class FilterRecordEntity{
    @Id
    private String pubId;
    @Property(nameInDb = "filterListId")
    private String filterListId;
    @Property(nameInDb = "deviceCode")
    private String deviceCode;
    @Property(nameInDb = "snapshootImgBase64")
    private String snapshootImgBase64;
    @Property(nameInDb = "createTime")
    private long createTime;
    @Property(nameInDb = "matchScore")
    private float matchScore;
    public float getMatchScore() {
        return this.matchScore;
    }
    public void setMatchScore(float matchScore) {
        this.matchScore = matchScore;
    }
    public long getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }
    public String getSnapshootImgBase64() {
        return this.snapshootImgBase64;
    }
    public void setSnapshootImgBase64(String snapshootImgBase64) {
        this.snapshootImgBase64 = snapshootImgBase64;
    }
    public String getDeviceCode() {
        return this.deviceCode;
    }
    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }
    public String getFilterListId() {
        return this.filterListId;
    }
    public void setFilterListId(String filterListId) {
        this.filterListId = filterListId;
    }
    public String getPubId() {
        return this.pubId;
    }
    public void setPubId(String pubId) {
        this.pubId = pubId;
    }
    @Generated(hash = 1199475335)
    public FilterRecordEntity(String pubId, String filterListId, String deviceCode,
            String snapshootImgBase64, long createTime, float matchScore) {
        this.pubId = pubId;
        this.filterListId = filterListId;
        this.deviceCode = deviceCode;
        this.snapshootImgBase64 = snapshootImgBase64;
        this.createTime = createTime;
        this.matchScore = matchScore;
    }
    @Generated(hash = 113625969)
    public FilterRecordEntity() {
    }
}
