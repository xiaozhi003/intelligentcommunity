package com.eyecool.intelligentcommunity.dao;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by ouyangfan on 2018/8/28.
 */
@Entity
public class RecordEntity {
    @Id
    private String pubId;
    @Property(nameInDb = "peopleCode")
    private String peopleCode;
    @Property(nameInDb = "peopleName")
    private String faceCode;
    @Property(nameInDb = "snapshootImgBase64")
    private String snapshootImgBase64;
    @Property(nameInDb = "peopleType")
    private int peopleType;
    @Property(nameInDb = "matchScore")
    private float matchScore;
    @Property(nameInDb = "createTime")
    private long createTime;
    public long getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }
    public float getMatchScore() {
        return this.matchScore;
    }
    public void setMatchScore(float matchScore) {
        this.matchScore = matchScore;
    }
    public int getPeopleType() {
        return this.peopleType;
    }
    public void setPeopleType(int peopleType) {
        this.peopleType = peopleType;
    }
    public String getSnapshootImgBase64() {
        return this.snapshootImgBase64;
    }
    public void setSnapshootImgBase64(String snapshootImgBase64) {
        this.snapshootImgBase64 = snapshootImgBase64;
    }
    public String getFaceCode() {
        return this.faceCode;
    }
    public void setFaceCode(String faceCode) {
        this.faceCode = faceCode;
    }
    public String getPeopleCode() {
        return this.peopleCode;
    }
    public void setPeopleCode(String peopleCode) {
        this.peopleCode = peopleCode;
    }
    public String getPubId() {
        return this.pubId;
    }
    public void setPubId(String pubId) {
        this.pubId = pubId;
    }
    @Generated(hash = 277419136)
    public RecordEntity(String pubId, String peopleCode, String faceCode,
            String snapshootImgBase64, int peopleType, float matchScore,
            long createTime) {
        this.pubId = pubId;
        this.peopleCode = peopleCode;
        this.faceCode = faceCode;
        this.snapshootImgBase64 = snapshootImgBase64;
        this.peopleType = peopleType;
        this.matchScore = matchScore;
        this.createTime = createTime;
    }
    @Generated(hash = 867663846)
    public RecordEntity() {
    }

}
