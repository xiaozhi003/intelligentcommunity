package com.eyecool.intelligentcommunity.dao;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by ouyangfan on 2018/8/25.
 */
@Entity
public class FeatureEntity{
    @Id
    private String pubId;
    @Property(nameInDb = "peopleCode")
    private String peopleCode;
    @Property(nameInDb = "faceImgPath")
    private String faceImgPath;
    @Property(nameInDb = "imgBase64")
    private String imgBase64;
    @Property(nameInDb = "feature")
    private String feature;
    @Property(nameInDb = "featureInfo")
    private String featureInfo;
    @Property(nameInDb = "creator")
    private String creator;
    @Property(nameInDb = "updateAuthor")
    private String updateAuthor;
    @Property(nameInDb = "proCode")
    private String proCode;
    @Property(nameInDb = "taskId")
    private int taskId;
    @Property(nameInDb = "regFrom")
    private int regFrom;
    @Property(nameInDb = "faceStatus")
    private int faceStatus;
    @Property(nameInDb = "updateTime")
    private long updateTime;
    @Property(nameInDb = "createTime")
    private long createTime;
    public long getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }
    public long getUpdateTime() {
        return this.updateTime;
    }
    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }
    public int getFaceStatus() {
        return this.faceStatus;
    }
    public void setFaceStatus(int faceStatus) {
        this.faceStatus = faceStatus;
    }
    public int getRegFrom() {
        return this.regFrom;
    }
    public void setRegFrom(int regFrom) {
        this.regFrom = regFrom;
    }
    public int getTaskId() {
        return this.taskId;
    }
    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }
    public String getProCode() {
        return this.proCode;
    }
    public void setProCode(String proCode) {
        this.proCode = proCode;
    }
    public String getUpdateAuthor() {
        return this.updateAuthor;
    }
    public void setUpdateAuthor(String updateAuthor) {
        this.updateAuthor = updateAuthor;
    }
    public String getCreator() {
        return this.creator;
    }
    public void setCreator(String creator) {
        this.creator = creator;
    }
    public String getFeatureInfo() {
        return this.featureInfo;
    }
    public void setFeatureInfo(String featureInfo) {
        this.featureInfo = featureInfo;
    }
    public String getFeature() {
        return this.feature;
    }
    public void setFeature(String feature) {
        this.feature = feature;
    }
    public String getImgBase64() {
        return this.imgBase64;
    }
    public void setImgBase64(String imgBase64) {
        this.imgBase64 = imgBase64;
    }
    public String getFaceImgPath() {
        return this.faceImgPath;
    }
    public void setFaceImgPath(String faceImgPath) {
        this.faceImgPath = faceImgPath;
    }
    public String getPeopleCode() {
        return this.peopleCode;
    }
    public void setPeopleCode(String peopleCode) {
        this.peopleCode = peopleCode;
    }
    public String getPubId() {
        return this.pubId;
    }
    public void setPubId(String pubId) {
        this.pubId = pubId;
    }
    @Generated(hash = 83726610)
    public FeatureEntity(String pubId, String peopleCode, String faceImgPath,
            String imgBase64, String feature, String featureInfo, String creator,
            String updateAuthor, String proCode, int taskId, int regFrom,
            int faceStatus, long updateTime, long createTime) {
        this.pubId = pubId;
        this.peopleCode = peopleCode;
        this.faceImgPath = faceImgPath;
        this.imgBase64 = imgBase64;
        this.feature = feature;
        this.featureInfo = featureInfo;
        this.creator = creator;
        this.updateAuthor = updateAuthor;
        this.proCode = proCode;
        this.taskId = taskId;
        this.regFrom = regFrom;
        this.faceStatus = faceStatus;
        this.updateTime = updateTime;
        this.createTime = createTime;
    }
    @Generated(hash = 247241888)
    public FeatureEntity() {
    }
}
