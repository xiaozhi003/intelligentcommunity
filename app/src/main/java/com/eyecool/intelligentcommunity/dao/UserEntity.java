package com.eyecool.intelligentcommunity.dao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

/**
 * Created by ouyangfan on 2018/8/24.
 */
@Entity
public class UserEntity{
    @Id
    private String pubId;
    @Property(nameInDb = "peopleName")
    private String peopleName;
    @Property(nameInDb = "acronym")
    private String acronym;
    @Property(nameInDb = "peoplePhone")
    private String peoplePhone;
    @Property(nameInDb = "peopleAddr")
    private String peopleAddr;
    @Property(nameInDb = "peopleIdCard")
    private String peopleIdCard;
    @Property(nameInDb = "ImgBase64")
    private String ImgBase64;
    @Property(nameInDb = "idCardImgPath")
    private String idCardImgPath;
    @Property(nameInDb = "peopleRoom")
    private String peopleRoom;
    @Property(nameInDb = "nodeId")
    private String nodeId;
    @Property(nameInDb = "creator")
    private String creator;
    @Property(nameInDb = "updateAuthor")
    private String updateAuthor;
    @Property(nameInDb = "proCode")
    private String proCode;
    @Property(nameInDb = "taskId")
    private int taskId;
    @Property(nameInDb = "peopleType")
    private int peopleType;
    @Property(nameInDb = "peopleSex")
    private int peopleSex;
    @Property(nameInDb = "peopleFloor")
    private String peopleFloor;
    @Property(nameInDb = "regFrom")
    private int regFrom;
    @Property(nameInDb = "peopleStatus")
    private int peopleStatus;
    @Property(nameInDb = "peopleBirthday")
    private long peopleBirthday;
    @Property(nameInDb = "expiryTime")
    private long expiryTime;
    @Property(nameInDb = "createTime")
    private long createTime;
    @Property(nameInDb = "updateTime")
    private long updateTime;
    public long getUpdateTime() {
        return this.updateTime;
    }
    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }
    public long getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }
    public long getExpiryTime() {
        return this.expiryTime;
    }
    public void setExpiryTime(long expiryTime) {
        this.expiryTime = expiryTime;
    }
    public long getPeopleBirthday() {
        return this.peopleBirthday;
    }
    public void setPeopleBirthday(long peopleBirthday) {
        this.peopleBirthday = peopleBirthday;
    }
    public int getPeopleStatus() {
        return this.peopleStatus;
    }
    public void setPeopleStatus(int peopleStatus) {
        this.peopleStatus = peopleStatus;
    }
    public int getRegFrom() {
        return this.regFrom;
    }
    public void setRegFrom(int regFrom) {
        this.regFrom = regFrom;
    }
    public String getPeopleFloor() {
        return this.peopleFloor;
    }
    public void setPeopleFloor(String peopleFloor) {
        this.peopleFloor = peopleFloor;
    }
    public int getPeopleSex() {
        return this.peopleSex;
    }
    public void setPeopleSex(int peopleSex) {
        this.peopleSex = peopleSex;
    }
    public int getPeopleType() {
        return this.peopleType;
    }
    public void setPeopleType(int peopleType) {
        this.peopleType = peopleType;
    }
    public int getTaskId() {
        return this.taskId;
    }
    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }
    public String getProCode() {
        return this.proCode;
    }
    public void setProCode(String proCode) {
        this.proCode = proCode;
    }
    public String getUpdateAuthor() {
        return this.updateAuthor;
    }
    public void setUpdateAuthor(String updateAuthor) {
        this.updateAuthor = updateAuthor;
    }
    public String getCreator() {
        return this.creator;
    }
    public void setCreator(String creator) {
        this.creator = creator;
    }
    public String getNodeId() {
        return this.nodeId;
    }
    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }
    public String getPeopleRoom() {
        return this.peopleRoom;
    }
    public void setPeopleRoom(String peopleRoom) {
        this.peopleRoom = peopleRoom;
    }
    public String getIdCardImgPath() {
        return this.idCardImgPath;
    }
    public void setIdCardImgPath(String idCardImgPath) {
        this.idCardImgPath = idCardImgPath;
    }
    public String getImgBase64() {
        return this.ImgBase64;
    }
    public void setImgBase64(String ImgBase64) {
        this.ImgBase64 = ImgBase64;
    }
    public String getPeopleIdCard() {
        return this.peopleIdCard;
    }
    public void setPeopleIdCard(String peopleIdCard) {
        this.peopleIdCard = peopleIdCard;
    }
    public String getPeopleAddr() {
        return this.peopleAddr;
    }
    public void setPeopleAddr(String peopleAddr) {
        this.peopleAddr = peopleAddr;
    }
    public String getPeoplePhone() {
        return this.peoplePhone;
    }
    public void setPeoplePhone(String peoplePhone) {
        this.peoplePhone = peoplePhone;
    }
    public String getAcronym() {
        return this.acronym;
    }
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }
    public String getPeopleName() {
        return this.peopleName;
    }
    public void setPeopleName(String peopleName) {
        this.peopleName = peopleName;
    }
    public String getPubId() {
        return this.pubId;
    }
    public void setPubId(String pubId) {
        this.pubId = pubId;
    }
    @Generated(hash = 1279593253)
    public UserEntity(String pubId, String peopleName, String acronym,
            String peoplePhone, String peopleAddr, String peopleIdCard,
            String ImgBase64, String idCardImgPath, String peopleRoom,
            String nodeId, String creator, String updateAuthor, String proCode,
            int taskId, int peopleType, int peopleSex, String peopleFloor,
            int regFrom, int peopleStatus, long peopleBirthday, long expiryTime,
            long createTime, long updateTime) {
        this.pubId = pubId;
        this.peopleName = peopleName;
        this.acronym = acronym;
        this.peoplePhone = peoplePhone;
        this.peopleAddr = peopleAddr;
        this.peopleIdCard = peopleIdCard;
        this.ImgBase64 = ImgBase64;
        this.idCardImgPath = idCardImgPath;
        this.peopleRoom = peopleRoom;
        this.nodeId = nodeId;
        this.creator = creator;
        this.updateAuthor = updateAuthor;
        this.proCode = proCode;
        this.taskId = taskId;
        this.peopleType = peopleType;
        this.peopleSex = peopleSex;
        this.peopleFloor = peopleFloor;
        this.regFrom = regFrom;
        this.peopleStatus = peopleStatus;
        this.peopleBirthday = peopleBirthday;
        this.expiryTime = expiryTime;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }
    @Generated(hash = 1433178141)
    public UserEntity() {
    }
}
