package com.eyecool.intelligentcommunity.manager;

import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.view.SurfaceHolder;

import com.eyecool.library.utils.Logs;
import com.eyecool.library.utils.TimeUtil;

import java.io.IOException;

/**
 * Created by ouyangfan on 2018/8/17.
 */

public class CameraManager {

  private static final String TAG = CameraManager.class.getSimpleName();

  //双例模式，主要有前后两个摄像头，如果有三个摄像头，则多添加一个
  private static CameraManager sCameraManager1, sCameraManager2;

  private Camera mCamera;
  private boolean isOpen = false;
  //上一次从视频流中获取数据的时间
  private long mLastTime;
  //每隔多长时间从视频流中获取一次数据，默认300ms，表示一秒钟大概获取3帧数据
  private int mIntervalTime = 100;
  private int mWidth, mHeight, mImageFormat;
  //主要用来重启摄像头
  private int mCameraId;
  private SurfaceHolder mSurfaceHolder;
  //视频流数据监听
  private ICameraPreviewListener mICameraPreviewListener;
  //相机出错监听
  private ICameraError mICameraError;

  public static CameraManager getInstance(boolean isColor) {
    if (isColor) {
      if (sCameraManager1 == null) {
        synchronized (CameraManager.class) {
          if (sCameraManager1 == null) {
            sCameraManager1 = new CameraManager();
          }
        }
      }
      return sCameraManager1;
    } else {
      if (sCameraManager2 == null) {
        synchronized (CameraManager.class) {
          if (sCameraManager2 == null) {
            sCameraManager2 = new CameraManager();
          }
        }
      }
      return sCameraManager2;
    }
  }

  private CameraManager() {
  }

  public void setICameraError(ICameraError ICameraError) {
    mICameraError = ICameraError;
  }

  public void setParameters(Camera.Parameters parameters) {
    if (mCamera != null) {
      mCamera.setParameters(parameters);
    }
  }

  public Camera.Parameters getParameters() {
    if (mCamera != null) {
      return mCamera.getParameters();
    }
    return null;
  }

  public void setIntervalTime(int intervalTime) {
    mIntervalTime = intervalTime;
  }

  public void setICameraPreviewListener(ICameraPreviewListener ICameraPreviewListener) {
    mICameraPreviewListener = ICameraPreviewListener;
  }

  /**
   * 打开摄像头
   *
   * @param cameraId 摄像头编号
   * @return
   */
  public synchronized boolean openCamera(int cameraId) {
    if (mCamera != null && isOpen) {
      return true;
    }
    try {
      Logs.i(TAG,"openCamera...");
      mCamera = Camera.open(cameraId);
      Camera.Parameters parameters = mCamera.getParameters();
      parameters.setPreviewSize(640, 480);
      parameters.setPictureSize(640, 480);
      mCamera.setParameters(parameters);
      mCameraId = cameraId;
      isOpen = true;
      setErrorCallback();
    } catch (RuntimeException e) {
      e.printStackTrace();
      isOpen = false;
      return false;
    }
    return true;
  }

  //设置预览
  public synchronized void setPreview(SurfaceHolder holder) {
    Logs.i(TAG,"setPreview... isOpen:" + isOpen);
    try {
      if (mCamera != null && isOpen) {
        Logs.i(TAG,"设置了预览界面");
        mCamera.setPreviewDisplay(holder);
        Camera.Size previewSize = mCamera.getParameters().getPictureSize();
        int format = mCamera.getParameters().getPreviewFormat();
        byte[] preBuffer = new byte[previewSize.width * previewSize.height * ImageFormat.getBitsPerPixel(format) / 8];
        mCamera.addCallbackBuffer(preBuffer);
        mCamera.startPreview();
        mSurfaceHolder = holder;
      }
    } catch (IOException e) {
      e.printStackTrace();
      Logs.e(TAG,e.getMessage());
    }
  }

  //停止预览
  public synchronized void stopPreview() {
    if (mCamera != null && isOpen) {
      try {
        Logs.i(TAG,"stopPreview...");
        mCamera.setPreviewCallback(null);
        mCamera.setPreviewDisplay(null);
        mCamera.stopPreview();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    mSurfaceHolder = null;
  }

  /**
   * 关闭摄像头
   */
  public synchronized void closeCamera() {
    stopPreview();
    if (mCamera != null) {
      mCamera.release();
      mCamera = null;
    }
    mCameraId = -1;
    isOpen = false;
  }

  //设置预览回调，用于获取视频流数据
  public void startPreviewCallback() {
    if (mCamera != null && isOpen) {
      mCamera.setPreviewCallback(new Camera.PreviewCallback() {
        @Override
        public void onPreviewFrame(byte[] data, Camera camera) {
          if (TimeUtil.getCurrentTimeMillis() - mLastTime > mIntervalTime) {
            mLastTime = TimeUtil.getCurrentTimeMillis();
            compressData(camera, data);
          }
        }
      });
//      mCamera.setPreviewCallbackWithBuffer(new Camera.PreviewCallback() {
//        @Override
//        public void onPreviewFrame(byte[] data, Camera camera) {
//          mCamera.addCallbackBuffer(data);
//          if (TimeUtil.getCurrentTimeMillis() - mLastTime > mIntervalTime) {
//            mLastTime = TimeUtil.getCurrentTimeMillis();
//            compressData(camera, data);
//          }
//        }
//      });
    }
  }


  //压缩原始照片数据
  private void compressData(Camera camera, byte[] data) {
    if (mWidth == 0 && mHeight == 0 && mImageFormat == 0) {
      Camera.Parameters parameters = camera.getParameters();
      mImageFormat = parameters.getPreviewFormat();
      mWidth = parameters.getPreviewSize().width;
      mHeight = parameters.getPreviewSize().height;
    }
    if (mICameraPreviewListener != null) {
      mICameraPreviewListener.onPreviewYuv(data);
    }

//    long start = TimeUtil.getCurrentTimeMillis();
//    Bitmap bitmap = Nv21Image.nv21ToBitmap(MyApp.getRs(), data, mWidth, mHeight);
//    Logs.e(TAG,"转Bitmap时间：" + (TimeUtil.getCurrentTimeMillis() - start));
//    //不可用，存在内存泄漏
//    if (mICameraPreviewListener != null) {
//      mICameraPreviewListener.onPreviewFrame(bitmap);
//    }
  }

  /**
   * 释放摄像头，同时把该类的对象也释放掉
   */
  public synchronized void release(boolean isBack) {
    closeCamera();
    if (isBack) {
      sCameraManager1 = null;
    } else {
      sCameraManager2 = null;
    }
  }

  private void setErrorCallback() {
    if (mCamera != null) {
      mCamera.setErrorCallback(new Camera.ErrorCallback() {
        @Override
        public void onError(int error, Camera camera) {
          Logs.e(TAG,"Camera报错了，需要重启Camera");
          if (mICameraError != null) {
            mICameraError.cameraError(error);
          }
          closeCamera();
          new Thread() {
            @Override
            public void run() {
              try {
                Thread.sleep(300);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              openCamera(mCameraId);
              setPreview(mSurfaceHolder);
            }
          }.start();
        }
      });
    }
  }

  public interface ICameraPreviewListener {
    void onPreviewFrame(Bitmap bitmap);

    void onPreviewYuv(byte[] yuv);
  }

  public interface ICameraError {
    void cameraError(int error);
  }
}
