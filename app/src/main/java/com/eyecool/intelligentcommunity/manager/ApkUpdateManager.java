package com.eyecool.intelligentcommunity.manager;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.eyecool.intelligentcommunity.BuildConfig;
import com.eyecool.intelligentcommunity.MyApp;
import com.eyecool.intelligentcommunity.R;
import com.eyecool.intelligentcommunity.activity.MainActivity;
import com.eyecool.intelligentcommunity.entity.ApkUpdateEntity;
import com.eyecool.intelligentcommunity.model.NetworkDataProcessing;
import com.eyecool.intelligentcommunity.utils.LogSaveUtil;
import com.eyecool.intelligentcommunity.view.CustomToast;
import com.eyecool.library.utils.FileUtils;
import com.google.common.eventbus.EventBus;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 远程升级接口
 */
public class ApkUpdateManager implements View.OnClickListener {
    private final String TAG = "ApkUpdateManager";
    private Context context;
    private static ApkUpdateManager INSTANCE = null;
    private CancleCallBack cancleCallBack;
    private String downLoadUrl = "";
    private Thread downLoadThread;
    private boolean isUpgrading;//是否正在升级过程当中
    private String savePath = "";
    private String saveFileName = "";
    private PopupWindow upgradeWindow;//升级弹框
    private ProgressBar downloadProgressBar;
    private TextView textViewProgress, tv_tip;
    private int progress;
    private boolean interceptFlag = false;
    private TextView tv_cancle;
    private String mInstallResult;

    private static final int DOWNLOAD_PROGRESS_UPDATE = 1;
    private static final int DOWNLOAD_OVER = 2;
    private static final int DOWNLOAD_FAILED = 3;

    public static ApkUpdateManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ApkUpdateManager();
        }
        return INSTANCE;
    }

    private Handler h = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
            switch (msg.what) {
                case DOWNLOAD_PROGRESS_UPDATE:
                    setProgress();
                    break;
                case DOWNLOAD_OVER://下载完成进行安装
                    tv_tip.setText("开始安装升级文件,请稍后");
                    startInstallApk();
                    break;
                case DOWNLOAD_FAILED:
                    tv_tip.setText("文件下载失败！");
                    break;
            }
        }
    };

    public void init(Context context, CancleCallBack cancleCallBack) {
        this.cancleCallBack = cancleCallBack;
        this.context = context;
        savePath = FileUtils.getInStallApkPath(context);
        saveFileName = savePath + "/install.apk";
    }

    public void getUpdata() {
        NetworkDataProcessing.getInstance().getInstance().checkApkUpDate();
    }

    public void checkUpdate(ApkUpdateEntity apkUpdateEntity) {

    }

    //开始进行升级
    public void startUpdate(View rootView, String downLoadUrl) {
        this.downLoadUrl = downLoadUrl;
        if (null != upgradeWindow && upgradeWindow.isShowing()) {
            upgradeWindow.dismiss();
            return;
        }
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View popupWindowView = layoutInflater.inflate(R.layout.popup_upgrade, null);
        downloadProgressBar = popupWindowView.findViewById(R.id.progressBarDownload);
        textViewProgress = popupWindowView.findViewById(R.id.textViewProgress);
        tv_tip = popupWindowView.findViewById(R.id.tv_tip);
        tv_cancle = popupWindowView.findViewById(R.id.tv_cancle);
        tv_cancle.setOnClickListener(this);
        final int w = (int) (MyApp.screenWidth * 0.6);
        final int h = (int) (MyApp.screenWidth * 0.3);
        upgradeWindow = new PopupWindow(popupWindowView, w, h, true);
        upgradeWindow.setFocusable(true);
        upgradeWindow.setOutsideTouchable(false);
        upgradeWindow.showAtLocation(rootView, Gravity.CENTER, 0, 0);
        tv_tip.setText("文件下载中");
        downloadApk();
    }

    /**
     * 启动下载apk任务
     *
     * @param
     */
    private void downloadApk() {
        downLoadThread = new Thread(mdownApkRunnable);
        downLoadThread.start();
    }

    private final Runnable mdownApkRunnable = new Runnable() {
        @Override
        public void run() {
            LogSaveUtil.d(TAG, "apk升级，开始下载");
            isUpgrading = true;
            try {
                URL url = new URL(downLoadUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.connect();
                int length = conn.getContentLength();
                InputStream is = conn.getInputStream();

                File file = new File(savePath);
                if (!file.exists()) {
                    file.mkdir();
                }
                String apkFile = saveFileName;
                File ApkFile = new File(apkFile);
                FileOutputStream fos = new FileOutputStream(ApkFile);

                int count = 0;
                byte buf[] = new byte[4096];

                do {
                    int numread = is.read(buf);
                    count += numread;
                    progress = (int) (((float) count / length) * 100);
                    h.sendEmptyMessage(DOWNLOAD_PROGRESS_UPDATE);
                    if (numread <= 0) {
                        h.sendEmptyMessage(DOWNLOAD_OVER);
                        break;
                    }
                    fos.write(buf, 0, numread);
                } while (!interceptFlag);

                fos.close();
                is.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
                h.sendEmptyMessage(DOWNLOAD_FAILED);
                isUpgrading = false;
            } catch (IOException e) {
                e.printStackTrace();
                h.sendEmptyMessage(DOWNLOAD_FAILED);
                isUpgrading = false;
            }
            isUpgrading = false;
        }
    };

    private void setProgress() {
        downloadProgressBar.setProgress(progress);
        textViewProgress.setText(progress + "%");
    }

    public void startInstallApk() {
        //判断sdk版本
        new Thread() {
            @Override
            public void run() {
                super.run();
                File f = new File(saveFileName);
                if (!f.exists()) {
                    return;
                }
                if (systemIsAndroidN()) {
                    LogSaveUtil.d(TAG, "安装apk,>7.0");
                    installApkForAndroidN(f.getAbsolutePath());
                    Intent intent = new Intent(context, MainActivity.class);
                    @SuppressLint("WrongConstant") PendingIntent restartIntent = PendingIntent.getActivity(context, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
                    AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, restartIntent);

                } else {
                    try {
                        LogSaveUtil.d(TAG, "安装apk,<7.0");
                        mInstallResult = installApk(f.getAbsolutePath());
                    } catch (Exception e) {
                        tv_tip.setText("安装失败");
                        e.printStackTrace();
                    }
                }
            }
        }.start();

    }

    /**
     * 7.0 静默安装apk方式
     *
     * @param installPath
     */
    private int installApkForAndroidN(String installPath) {
        int result = 0;
        try {
            String[] args = {"pm", "install", "-i ", "com.eyecool.intelligentcommunity", "-r", installPath};
            ProcessBuilder processBuilder = new ProcessBuilder(args);

            Process process = null;
            BufferedReader successResult = null;
            BufferedReader errorResult = null;
            StringBuilder successMsg = new StringBuilder();
            StringBuilder errorMsg = new StringBuilder();

            try {
                process = processBuilder.start();
                successResult = new BufferedReader(new InputStreamReader(
                        process.getInputStream()));
                errorResult = new BufferedReader(new InputStreamReader(
                        process.getErrorStream()));
                String s;

                while ((s = successResult.readLine()) != null) {
                    successMsg.append(s);
                    LogSaveUtil.d(TAG, "安装apk,>7.0,successMsg：" + successMsg);
                }

                while ((s = errorResult.readLine()) != null) {
                    errorMsg.append(s);
                    LogSaveUtil.d(TAG, "安装apk,>7.0,失败：" + errorMsg);
                }
            } catch (IOException e) {
                e.printStackTrace();
                LogSaveUtil.d(TAG, "安装apk,>7.0,失败：" + e.getMessage());
                result = 2;
            } catch (Exception e) {
                LogSaveUtil.d(TAG, "安装apk,>7.0,失败：" + e.getMessage());
                e.printStackTrace();
                result = 2;
            } finally {
                try {
                    if (successResult != null) {
                        successResult.close();
                    }
                    if (errorResult != null) {
                        errorResult.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (process != null) {
                    process.destroy();
                }
            }

            if (successMsg.toString().contains("Success")
                    || successMsg.toString().contains("success")) {
                result = 1;
            } else {
                result = 2;
            }

        } catch (Exception e) {
            result = -1;
        }
        return result;
    }

    private String installApk(String apkAbsolutePath) throws Exception {
        LogSaveUtil.d(TAG,"installAPK: " + apkAbsolutePath);
        String command = "chmod 777 " + apkAbsolutePath;
        Runtime runtime = Runtime.getRuntime();
        Process proc = runtime.exec(command);
        LogSaveUtil.d(TAG,"installAPK: " + apkAbsolutePath);

        //静默安装
        File file = new File(apkAbsolutePath);
        if (!file.exists()) {
            LogSaveUtil.d(TAG,"file is not exist");
            return "安装文件不存在";
        }

        String[] args = {"pm", "install", "-rf", apkAbsolutePath};
        String result = "";
        ProcessBuilder processBuilder = new ProcessBuilder(args);
        Process process = null;
        InputStream errIs = null;
        InputStream inIs = null;
        LogSaveUtil.d(TAG,"开始安装");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int read = -1;
        process = processBuilder.start();
        errIs = process.getErrorStream();
        while ((read = errIs.read()) != -1) {
            baos.write(read);
        }
        baos.write('\n');
        inIs = process.getInputStream();
        while ((read = inIs.read()) != -1) {
            baos.write(read);
        }
        byte[] data = baos.toByteArray();
        result = new String(data);
        LogSaveUtil.d(TAG,"安装完成：+ " + result);
        if (errIs != null) {
            errIs.close();
        }
        if (inIs != null) {
            inIs.close();
        }
        if (process != null) {
            process.destroy();
        }
        return result;
    }

    private boolean systemIsAndroidN() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        //取消升级
        cancleCallBack.cancleUpdate();
    }

    public void release() {
        if (upgradeWindow != null) {
            upgradeWindow.dismiss();
            upgradeWindow = null;
        }
        interceptFlag = true;
        context = null;
    }

    public interface CancleCallBack {
        void cancleUpdate();
    }
}
