package com.eyecool.intelligentcommunity.eventmsg;

/**
 * Created by Vinda on 2018/5/17.
 * Describe
 */

public class EventMsg {
    public static final int ACTION_START_UPDATE = 1;//开始进行升级（apk下载与安装）

    private Object bundle;
    private int type;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public EventMsg(Object bundle, int type) {
        this.bundle = bundle;
        this.type = type;
    }

    public Object getBundle() {
        return bundle;
    }

    public void setBundle(Object bundle) {
        this.bundle = bundle;
    }
}
