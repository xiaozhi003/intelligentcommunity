package com.eyecool.intelligentcommunity.presenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.os.Power;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.ChlFaceSdk.ChlFaceSdk;
import com.eyecool.intelligentcommunity.MyApp;
import com.eyecool.intelligentcommunity.R;
import com.eyecool.intelligentcommunity.dao.DaoManager;
import com.eyecool.intelligentcommunity.dao.FilterRecordEntity;
import com.eyecool.intelligentcommunity.dao.RecordEntity;
import com.eyecool.intelligentcommunity.dao.SpecialUserEntity;
import com.eyecool.intelligentcommunity.dao.UserEntity;
import com.eyecool.intelligentcommunity.entity.FeatureEntry;
import com.eyecool.intelligentcommunity.model.MediaPlayerManager;
import com.eyecool.intelligentcommunity.model.NetworkDataProcessing;
import com.eyecool.intelligentcommunity.model.ZHTDoor;
import com.eyecool.intelligentcommunity.utils.Constant;
import com.eyecool.intelligentcommunity.utils.ExecutorUtil;
import com.eyecool.intelligentcommunity.utils.GpioJhc3399;
import com.eyecool.intelligentcommunity.utils.GpioManager;
import com.eyecool.intelligentcommunity.utils.SoundUtils;
import com.eyecool.intelligentcommunity.utils.SystemUtil;
import com.eyecool.intelligentcommunity.view.IRecognitionView;
import com.eyecool.library.algorithm.face.five003.DetectResult;
import com.eyecool.library.algorithm.face.five003.Hard5003;
import com.eyecool.library.algorithm.face.five003.Soft5003;
import com.eyecool.library.utils.FileUtils;
import com.eyecool.library.utils.Logs;
import com.eyecool.library.utils.SystemUtils;
import com.eyecool.library.utils.TimeUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by ouyangfan on 2018/8/23.
 */

public class RecognitionPresenter implements IRecognitionPresenter {

  private static final String TAG = RecognitionPresenter.class.getSimpleName();

  private IRecognitionView mIRecognitionView;

  //彩色照片连续无人脸的次数，当大于30次时显示广告页
  private long count;
  //彩色照片有人脸时的检测结果
  private DetectResult mDetectResult;

  private byte[] mColorRGB24;
  private byte[] mTempColorYUV; // 可见光yuv数据
  private byte[] mTempInfraredYUV; // 近红外yuv数据

  private long mInfraredLastPreviewTime = 0; // 近红外最后返回数据时间
  private long mColorLastPreviewTime = 0; // 可见光最后返回数据时间

  private int mDetectWidth = 0;
  private int mDetectHeight = 0;

  private DetectYUVTask mDetectYUVTask;

  //黑白照片连续无人脸的次数，当大于6次（大概2秒）还没人脸时提示非活体
  private int bwCount;

  private long lastRecognitionTime;
  private String lastRecognitionPeople;
  private Context mContext;
  //语音播报
  private MediaPlayerManager mMediaPlayerManager;
  //数据库查询
  private DaoManager mDaoManager;
  //网络请求数据
  private NetworkDataProcessing mNetworkDataProcessing;
  //比对分数阈值
  private int mStandardScore = 80;
  // 识别失败次数
  int mFailRecognizeNum = 0;
  // 识别无人员时间
  long mRecognizeNoPeopleTime = 0;

  DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
  String dateStr1 = "6:00:00";
  String dateStr2 = "18:00:00";
  Date mStartTime;
  Date mEndTime;

  public RecognitionPresenter(IRecognitionView recognitionView, AppCompatActivity activity) {
    mIRecognitionView = recognitionView;
    mContext = activity;
    if (MyApp.isSoft) {
      MyApp.mFaceAlgorithm = Soft5003.getInstance(activity);
    } else {
      MyApp.mFaceAlgorithm = Hard5003.getInstance(activity);
    }
    mDaoManager = new DaoManager(activity);
    mNetworkDataProcessing = NetworkDataProcessing.getInstance();
    MyApp.mDoor = ZHTDoor.getInstance();

    try {
      mStartTime = dateFormat.parse(dateStr1);
      mEndTime = dateFormat.parse(dateStr2);
    } catch (ParseException e) {
      e.printStackTrace();
    }
  }

  @Override
  public synchronized void detectColorFace(final Bitmap bitmap) {
  }

  public void setCount(long count) {
    this.count = count;
  }

  @Override
  public synchronized void detectBWFace(final Bitmap bitmap) {
  }

  @Override
  public void detectColorFace(byte[] yuv) {
    mColorLastPreviewTime = TimeUtil.getCurrentTimeMillis();
    mTempColorYUV = yuv;
  }

  @Override
  public void detectBWFace(byte[] yuv) {
    mInfraredLastPreviewTime = TimeUtil.getCurrentTimeMillis();
    mTempInfraredYUV = yuv;
  }

  public void onResume() {
    if (mDetectYUVTask == null) {
      mInfraredLastPreviewTime = TimeUtil.getCurrentTimeMillis();
      mColorLastPreviewTime = TimeUtil.getCurrentTimeMillis();
      mDetectYUVTask = new DetectYUVTask();
      ExecutorUtil.exec(mDetectYUVTask);
    }
  }

  public void onPause() {
    if (mDetectYUVTask != null) {
      mDetectYUVTask.setPause(true);
      mDetectYUVTask = null;
    }
  }

  ArrayList<FeatureEntry> mFeatureEntries = new ArrayList<>();

  private synchronized void compare(byte[] feature) {
    Logs.e(TAG, "开始比对");
    long startCompare = TimeUtil.getCurrentTimeMillis();
    if (mFeatureEntries.size() != Constant.sFeatureEntries.size()) {
      mFeatureEntries.clear();
      mFeatureEntries.addAll(Constant.sFeatureEntries);
    }
    Logs.i(TAG, "addAll时间：" + (TimeUtil.getCurrentTimeMillis() - startCompare) + "ms");

    // 创建比对库句柄
    long startTime = TimeUtil.getCurrentTimeMillis();
    int compareList = 0;
    if (ChlFaceSdk.sFeatureList == -1 || mFeatureEntries.size() > ChlFaceSdk.sMaxFeatureNum) {
      ChlFaceSdk.sMaxFeatureNum = (int) (mFeatureEntries.size() * 1.5);
      if (ChlFaceSdk.sFeatureList != -1) {
        ChlFaceSdk.ListDestroy(ChlFaceSdk.sFeatureList);
      }
      ChlFaceSdk.sFeatureList = compareList = ChlFaceSdk.ListCreate(ChlFaceSdk.sMaxFeatureNum);
    } else {
      compareList = ChlFaceSdk.sFeatureList;
    }

    // 添加特征到算法库中
    Logs.d(TAG, "特征列表：" + compareList + " ");
    if (ChlFaceSdk.sInsertNum != mFeatureEntries.size() && mFeatureEntries.size() > 0) {
      ChlFaceSdk.sInsertNum = 0;
      long startClear = TimeUtil.getCurrentTimeMillis();
      ChlFaceSdk.ListClearAll(compareList);
      Logs.i(TAG, "清空列表时间：" + (TimeUtil.getCurrentTimeMillis() - startClear) + "ms");

      for (int i = 0; i < mFeatureEntries.size(); i++) {
        byte[] feature2 = mFeatureEntries.get(i).getFeature();
        int faceFeatureInsert = ChlFaceSdk.ListInsert(compareList, new int[]{i}, feature2, 1);
        ChlFaceSdk.sInsertNum++;
        Logs.d(TAG, "特征insert：" + faceFeatureInsert);
      }
      Logs.d(TAG, "特征insert size：" + ChlFaceSdk.sInsertNum);
      Logs.i(TAG, "特征insert时间：" + (TimeUtil.getCurrentTimeMillis() - startTime) + "ms");
    }
    if (mFeatureEntries.size() == 0) {
      Logs.e(TAG, "mFeatureEntries.size=" + mFeatureEntries.size());
      if (TimeUtil.getCurrentTimeMillis() - mRecognizeNoPeopleTime > 2000) {
        mIRecognitionView.showResult(IRecognitionPresenter.RECOGNITION_TYPE_NO_PEOPLE, null);
        mRecognizeNoPeopleTime = TimeUtil.getCurrentTimeMillis();
      }
      return;
    }

    byte[] scores = new byte[mFeatureEntries.size()];
    int compare = ChlFaceSdk.ListCompare(0, compareList, feature, 0, mFeatureEntries.size(), scores);
    Logs.i(TAG, "特征compare时间：" + (TimeUtil.getCurrentTimeMillis() - startTime) + "ms");

    Logs.e(TAG, "特征compare:" + compare);
    for (int index = 0; index < scores.length; index++) {
      int score = scores[index];
      FeatureEntry featureEntry = mFeatureEntries.get(index);
      Logs.i(TAG, "特征比对分值：" + score);
      String currentTime = TimeUtil.getCurrentTime("HH:mm:ss");

      try {
        if (!TimeUtil.compareTime(currentTime, Constant.sDayPeriodTime) && TimeUtil.compareTime(currentTime, Constant.sNightPeriodTime)) {
          //晚于白天时间，并且早于晚上时间，则是白天比对分数
          mStandardScore = Constant.sDayScore;
        } else {
          mStandardScore = Constant.sNightScore;
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
      Logs.e(TAG, "比对分数： " + score + "    people:  " + featureEntry.getFaceCode() + "   标准分数： " + mStandardScore);
      if (score > mStandardScore) {
        mFailRecognizeNum = 0;
        String peopleCode = featureEntry.getPeopleCode();
        if ((TimeUtil.getCurrentTimeMillis() - lastRecognitionTime) < (Constant.sTimeToFilterUser * 1000) && TextUtils.equals(lastRecognitionPeople, peopleCode)) {
          //同一个人5秒内不再开门
          Logs.e(TAG, "同一个人 " + (TimeUtil.getCurrentTimeMillis() - lastRecognitionTime) + " Constant.sTimeToFilterUser=" + Constant.sTimeToFilterUser);
          return;
        }
        lastRecognitionTime = TimeUtil.getCurrentTimeMillis();
        lastRecognitionPeople = peopleCode;
        Logs.e(TAG, "peopleCode=" + peopleCode);
        //先判断是否是红黑名单的人
        long startQuerySpecial = TimeUtil.getCurrentTimeMillis();
        List<SpecialUserEntity> specialUserEntitys = mDaoManager.getSpecialUserEntitiesByPeopleCode(peopleCode == null ? "" : peopleCode);
        Logs.i(TAG, "查询黑名单耗时：" + (TimeUtil.getCurrentTimeMillis() - startQuerySpecial) + "ms");

        String path = SystemUtils.getRootPath() + "/temp/";
        String fileName = path + "tempColours.jpg";
        ChlFaceSdk.SaveJpegFile(fileName, mColorRGB24, mDetectWidth, mDetectHeight, 24, 75);
        byte[] jpgByte = FileUtils.readFile(fileName);
        String jpgImgBase64 = Base64.encodeToString(jpgByte, Base64.DEFAULT);
        Logs.i(TAG, "jpg转base64耗时：" + (TimeUtil.getCurrentTimeMillis() - startCompare) + "ms");

        for (SpecialUserEntity specialUserEntity : specialUserEntitys) {
          if (specialUserEntity != null) {
            int filterType = specialUserEntity.getFilterType();
            if (filterType == 0) {//黑名单人员
              Logs.e(TAG, "黑名单人员");

              FilterRecordEntity filterRecordEntity = new FilterRecordEntity();
              String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
              filterRecordEntity.setPubId(uuid);
              filterRecordEntity.setCreateTime(TimeUtil.getCurrentTimeMillis());
              filterRecordEntity.setMatchScore(score);
              filterRecordEntity.setSnapshootImgBase64(jpgImgBase64);
              filterRecordEntity.setFilterListId(specialUserEntity.getPubId());
              filterRecordEntity.setDeviceCode(Constant.sDeviceId);
              ArrayList<FilterRecordEntity> list = new ArrayList<>();
              list.add(filterRecordEntity);
              mNetworkDataProcessing.uploadFilterRecord(list);
              mIRecognitionView.showResult(IRecognitionPresenter.RECOGNITION_TYPE_BLACKLIST, null);

              return;
            }
          }
        }
//                    UserEntity userEntity = mDaoManager.getUserEntity(peopleCode);
        long startQueryUser = TimeUtil.getCurrentTimeMillis();
        UserEntity userEntity = mDaoManager.getUserEntityUsed(peopleCode == null ? "" : peopleCode);
        Logs.i(TAG, "查询user耗时：" + (TimeUtil.getCurrentTimeMillis() - startCompare) + "ms");
        if (userEntity == null) {//找不到这个人
          Logs.e(TAG, "找不到人");
          mIRecognitionView.showResult(IRecognitionPresenter.RECOGNITION_TYPE_FAILED, null);
          return;
        }
        String name = userEntity.getPeopleName();
        long expiryTime = userEntity.getExpiryTime();
        if (expiryTime < TimeUtil.getCurrentTimeMillis()) {//超过有效期了
          mIRecognitionView.showResult(IRecognitionPresenter.RECOGNITION_TYPE_OUT_EXPIRY_TIME, name);
          return;
        }
        if (0 == userEntity.getPeopleStatus()) {// 禁用
          Logs.i("人员禁用...");
          mIRecognitionView.showResult(IRecognitionPresenter.RECOGNITION_TYPE_OUT_EXPIRY_TIME, name);
          return;
        } else if(1 == userEntity.getPeopleStatus()) { // 启用
        } else if(2 == userEntity.getPeopleStatus()) { // 待审核
          Logs.i(TAG,"人员待审核...");
          mIRecognitionView.showResult(IRecognitionPresenter.RECOGNITION_TYPE_OUT_EXPIRY_TIME, name);
          return;
        }
        Logs.i(TAG, "识别成功耗时：" + (TimeUtil.getCurrentTimeMillis() - startCompare) + "ms");

        if ("rk3399-mid".equals(SystemUtil.getSystemModel())) {
          SoundUtils.playSound(mContext);
          playSoundReleaseHandler.sendEmptyMessageDelayed(1, 2500);
        } else {
          mMediaPlayerManager = MediaPlayerManager.getInstance(mContext, R.raw.welcome);
          mMediaPlayerManager.start();
        }

        Log.e("Recognition", "model:" + SystemUtil.getSystemModel());

        Logs.i(TAG, "播放语音耗时：" + (TimeUtil.getCurrentTimeMillis() - startCompare) + "ms");

        // gpio控制开关门（开门）
        if ("rk3399-all".equals(SystemUtil.getSystemModel())) { // rk3399
          GpioManager.open();
        } else if ("rk3288".equals(SystemUtil.getSystemModel())) { // rk3388 new
          Power.set_zysj_gpio_value(4, 1);
        } else if ("rk3399-mid".equals(SystemUtil.getSystemModel())) {
          GpioJhc3399.init();
          GpioJhc3399.setValue(85, 1);//开门
        }

        new Thread() {
          @Override
          public void run() {
            try {
              Thread.sleep(Constant.sGPIODelay);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            // gpio控制开关门（关门）@
            if ("rk3399-all".equals(SystemUtil.getSystemModel())) { // YZ_rk3399
              GpioManager.close();
            } else if ("rk3288".equals(SystemUtil.getSystemModel())) { //rk3388new
              Power.set_zysj_gpio_value(4, 0);
            } else if ("rk3399-mid".equals(SystemUtil.getSystemModel())) {//JHC_rk3399关门
              GpioJhc3399.setValue(85, 0);
            }

          }
        }.start();

        Logs.i(TAG, "开门GPIO耗时：" + (TimeUtil.getCurrentTimeMillis() - startCompare) + "ms");

        RecordEntity uploadRecordEntity = new RecordEntity();
        String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
        uploadRecordEntity.setPubId(uuid);
        uploadRecordEntity.setFaceCode(featureEntry.getFaceCode());
        uploadRecordEntity.setMatchScore(score);
        uploadRecordEntity.setCreateTime(TimeUtil.getCurrentTimeMillis());

        uploadRecordEntity.setSnapshootImgBase64(jpgImgBase64);
        uploadRecordEntity.setPeopleCode(peopleCode);
        uploadRecordEntity.setPeopleType(userEntity.getPeopleType());
        ArrayList<RecordEntity> list = new ArrayList<>();
        list.add(uploadRecordEntity);
        mNetworkDataProcessing.uploadRecord(list);
        mIRecognitionView.showResult(IRecognitionPresenter.RECOGNITION_TYPE_SUCCESS, name);

        Logs.i(TAG, "上传记录耗时：" + (TimeUtil.getCurrentTimeMillis() - startCompare) + "ms");
        return;
      }
    }

    mFailRecognizeNum++;
    if (mFailRecognizeNum > 2) {
      Logs.e(TAG, "识别失败次数：" + mFailRecognizeNum);
      mFailRecognizeNum = 0;
      mIRecognitionView.showResult(IRecognitionPresenter.RECOGNITION_TYPE_FAILED, null);
    }
  }

  Handler playSoundReleaseHandler = new Handler() {
    @Override
    public void dispatchMessage(Message msg) {
      super.dispatchMessage(msg);
      SoundUtils.releasrSoundPool();
    }
  };

  public void deleteAllRecord() {
    mDaoManager.deleteAllRecord();
  }

  private class DetectYUVTask implements Runnable {

    private boolean isPause = false;

    public void setPause(boolean pause) {
      isPause = pause;
    }

    @Override
    public void run() {
      while (!isPause) {
        if (MyApp.mFaceAlgorithm == null || !MyApp.mFaceAlgorithm.isInit()) {
          Logs.i(TAG, "算法未初始化...");
          sleep(500);
          continue;
        }
        if (TimeUtil.getCurrentTimeMillis() - mInfraredLastPreviewTime > 30 * 1000
            || TimeUtil.getCurrentTimeMillis() - mColorLastPreviewTime > 30 * 1000) {
          SystemUtil.restartDevice();
          break;
        }

        final byte[] colorYuv = mTempColorYUV;
        final byte[] infraredYUV = mTempInfraredYUV;
        if (colorYuv == null || infraredYUV == null) {
          sleep(30);
          continue;
        }
        detect(colorYuv, infraredYUV);
      }
      Logs.i(TAG, "退出检测线程......");
    }
  }

  private void sleep(long millis) {
    try {
      Thread.sleep(millis);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  int i = 0;


  private void detect(byte[] colorYUV, byte[] infraredYUV) {
    Logs.i(TAG, "------彩色照片提取开始------ Thread.id=" + Thread.currentThread().getId());
    long start = TimeUtil.getCurrentTimeMillis();

    Logs.i(TAG, "colorYUV.size:" + (colorYUV == null ? null : colorYUV.length));
    Logs.i(TAG, "infraredYUV.size:" + (infraredYUV == null ? null : infraredYUV.length));

    // 检测可见光照片
    DetectResult colorDetectResult = MyApp.mFaceAlgorithm.detectFaceByNV21(0, colorYUV, 640, 480);

    Logs.e(TAG, "彩色照片提取完毕");
    if (colorDetectResult.getFaceDetectResult() != null) {
      Logs.e(TAG, "detectColorFace: mDetectResult != null");
      mColorRGB24 = colorDetectResult.getRgb24Data();
      count = TimeUtil.getCurrentTimeMillis();
      mIRecognitionView.hideAdv();
    } else {
      Logs.e(TAG, "detectColorFace: mDetectResult = null");
      if (TimeUtil.getCurrentTimeMillis() - count > 3000) {
        mIRecognitionView.showAdv();
      }
      return;
    }

    if (mDetectWidth != colorDetectResult.getWidth()) {
      mDetectWidth = colorDetectResult.getWidth();
    }
    if (mDetectHeight != colorDetectResult.getHeight()) {
      mDetectHeight = colorDetectResult.getHeight();
    }

    // 检测黑白照片
    Logs.e(TAG, "提取黑白照片信息开始");
    DetectResult infraredDetectResult = MyApp.mFaceAlgorithm.detectFaceByNV21(1, infraredYUV, 640, 480);
    Logs.e(TAG, "提取黑白照片信息完毕");

    int cameraType = 0;
    DetectResult tempDetectResult = null;

//    if (isDay(TimeUtil.getCurrentTime("HH:mm:ss"))) { // 白天
//      tempDetectResult = colorDetectResult;
//      cameraType = 0;
//    } else { // 晚上
//      tempDetectResult = infraredDetectResult;
//      cameraType = 1;
//    }

    tempDetectResult = infraredDetectResult;
    cameraType = 1;

    if (tempDetectResult.getFaceDetectResult() != null) {
      bwCount = 0;

      long time2 = System.currentTimeMillis();
      Logs.e(TAG, "开始检测活体");
//      int ret = MyApp.mFaceAlgorithm.liveDetect(width, height, colorRgb24, colorFaceDetectResult, bwRgb24, bwFaceDetectResult);
//      int ret = MyApp.mFaceAlgorithm.quickLiveDetect(0, width, height, bwRgb24, bwFaceDetectResult);
      int ret = MyApp.mFaceAlgorithm.liveDetectColor(0, cameraType, tempDetectResult.getWidth(), tempDetectResult.getHeight(), tempDetectResult.getRgb24Data(), tempDetectResult.getFaceDetectResult());
      long time3 = System.currentTimeMillis();
      Logs.e(TAG, "liveDetect 消费时间：" + (time3 - time2));

      Logs.e(TAG, "黑白照片检测结果：  " + ret);
      switch (ret) {
        case 0://非活体
          if (Constant.isSaveFailePic) {
            String path = SystemUtils.getRootPath() + "/temp/";
            String colorFilePath = path + "tempColours_" + i + ".jpg";
            String bwFilePath = path + "tempInfrared_" + i + ".jpg";
            ChlFaceSdk.SaveJpegFile(colorFilePath, colorDetectResult.getRgb24Data(), colorDetectResult.getWidth(), colorDetectResult.getHeight(), 24, 100);
            if (cameraType == 1) {
              ChlFaceSdk.SaveJpegFile(bwFilePath, infraredDetectResult.getRgb24Data(), infraredDetectResult.getWidth(), infraredDetectResult.getHeight(), 24, 100);
            }
          }
          i++;
          mIRecognitionView.showResult(IRecognitionPresenter.RECOGNITION_TYPE_NO_LIVE, null);
          break;
        case 1://活体
          //提取彩色照片特征值
          long time4 = System.currentTimeMillis();
          byte[] colorFeature = MyApp.mFaceAlgorithm.getFeature(colorDetectResult);
          long time5 = System.currentTimeMillis();
          Logs.e(TAG, "getFeature 消费时间：" + (time5 - time4));
          compare(colorFeature);
          long time6 = System.currentTimeMillis();
          Logs.e(TAG, "compare 消费时间：" + (time6 - time5));
          break;
        case -1://活体检测功能未授权
          break;
        case -2://参数错误
          break;
        case -3://内部错误
          break;
      }
    } else {
      Logs.e(TAG, "detectResult == null || mDetectResult == null");
      if (Constant.isSaveFailePic) {
        String path = SystemUtils.getRootPath() + "/temp/";
        String colorFilePath = path + "tempColours_noface_" + i + ".jpg";
        String bwFilePath = path + "tempInfrared_noface_" + i + ".jpg";
        ChlFaceSdk.SaveJpegFile(colorFilePath, colorDetectResult.getRgb24Data(), colorDetectResult.getWidth(), colorDetectResult.getHeight(), 24, 100);
        if (cameraType == 1) {
          ChlFaceSdk.SaveJpegFile(bwFilePath, infraredDetectResult.getRgb24Data(), infraredDetectResult.getWidth(), infraredDetectResult.getHeight(), 24, 100);
        }
      }
      i++;
      bwCount++;
      if (bwCount > 3) {
        bwCount = 0;
        mIRecognitionView.showResult(IRecognitionPresenter.RECOGNITION_TYPE_NO_LIVE, null);
      }
    }

    Logs.i(TAG, "------检测流程耗时------：" + (TimeUtil.getCurrentTimeMillis() - start) + "ms");
  }

  private static boolean isDay(String currentTime) {
    try {
      boolean isDay = !TimeUtil.compareTime(currentTime, Constant.sDayPeriodTime) && TimeUtil.compareTime(currentTime, Constant.sNightPeriodTime);
      return isDay;
    } catch (ParseException e) {
      e.printStackTrace();
      return false;
    }
  }
}