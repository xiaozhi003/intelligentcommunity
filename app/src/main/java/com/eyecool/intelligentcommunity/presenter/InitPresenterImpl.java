package com.eyecool.intelligentcommunity.presenter;

import android.content.Context;
import android.hardware.Camera;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.SurfaceHolder;

import com.eyecool.intelligentcommunity.manager.CameraManager;
import com.eyecool.intelligentcommunity.MyApp;
import com.eyecool.intelligentcommunity.R;
import com.eyecool.intelligentcommunity.model.MediaPlayerManager;
import com.eyecool.intelligentcommunity.model.NetworkDataProcessing;
import com.eyecool.intelligentcommunity.utils.Constant;
import com.eyecool.intelligentcommunity.utils.LogSaveUtil;
import com.eyecool.intelligentcommunity.view.IInitView;
import com.eyecool.library.algorithm.face.five003.Abstract5003;
import com.eyecool.library.algorithm.face.five003.Hard5003;
import com.eyecool.library.algorithm.face.five003.Soft5003;
import com.eyecool.library.utils.SharedPreferencesUtils;
import com.eyecool.library.utils.SystemUtils;

/**
 * 初始化Presenter实现类，主要初始化权限、算法、摄像头以及下载数据
 * Created by ouyangfan on 2018/8/22.
 */

public class InitPresenterImpl implements IInitPresenter, Abstract5003.FaceAlgorithmLogInfo {
    private final String TAG = "InitPresenterImpl";
    private IInitView mIInitView;

    private CameraManager mCameraManager;
    private CameraManager mInfraredCameraManager;
    private MediaPlayerManager mMediaPlayerManager;
    private NetworkDataProcessing mNetworkDataProcessing;
    private Context mContext;

    // 是否反转摄像头
    private boolean mReverseCamera = false;

    private Handler mHandler = new Handler();

    public InitPresenterImpl(IInitView IInitView, Context context) {
        mContext = context.getApplicationContext();
        mIInitView = IInitView;
        mCameraManager = CameraManager.getInstance(true);
        mInfraredCameraManager = CameraManager.getInstance(false);
        mNetworkDataProcessing = NetworkDataProcessing.getInstance();
    }

    //检查是否有相机权限
    private boolean checkPermission() {
        boolean cameraPermission = SystemUtils.checkPermission(mContext, "android.permission.CAMERA");
        return cameraPermission;
    }

    @Override
    public void init(AppCompatActivity activity) {
        initAlgorithm(activity);
    }

    //初始化算法
    private void initAlgorithm(final AppCompatActivity activity) {
        if (MyApp.mFaceAlgorithm != null && MyApp.mFaceAlgorithm.isInit()) {//假如已经初始化了算法
            LogSaveUtil.d(TAG, "算法初始化完成");
            initPermission();
            return;
        }
        if (MyApp.isSoft) {
            LogSaveUtil.d(TAG, "算法软授权初始化");
            MyApp.mFaceAlgorithm = Soft5003.getInstance(activity);
            MyApp.mFaceAlgorithm.setFaceAlgorithmLogInfo(this);
            LogSaveUtil.d(TAG, "初始化软授权对象");
            Soft5003 soft5003 = (Soft5003) MyApp.mFaceAlgorithm;
            LogSaveUtil.d(TAG, "检查软授权是否已进行授权");
            boolean isRegister = soft5003.isRegister();
            LogSaveUtil.d(TAG, "软授权查询结果：" + isRegister);
            if (isRegister) {
                softInit();
            } else {
                mIInitView.showAlgorithmDialog(IInitPresenter.ALGORITHM_TYPE_INPUT);
            }
        } else {
            LogSaveUtil.d(TAG, "算法硬狗授权初始化");
            MyApp.mFaceAlgorithm = Hard5003.getInstance(activity);
            Hard5003 hard5003 = (Hard5003) MyApp.mFaceAlgorithm;
            hard5003.setUsbConnectListener(new Hard5003.UsbConnectListener() {
                @Override
                public void usbConnected(boolean isConnect) {
                    if (isConnect) {
                        MyApp.mFaceAlgorithm.init();
                    } else {
                        mIInitView.showAlgorithmDialog(IInitPresenter.ALGORITHM_TYPE_FAILED);
                    }
                }
            });
            int ret = hard5003.connectUsb();
            if (ret != 0 && ret != -4) {//没有插入算法硬狗
                mIInitView.showAlgorithmDialog(IInitPresenter.ALGORITHM_TYPE_ERROR);
            }
        }
        MyApp.mFaceAlgorithm.setIInitListener(new Abstract5003.IInitListener() {
            @Override
            public void init(boolean isInit) {
                if (!isInit) {
                    mIInitView.showAlgorithmDialog(IInitPresenter.ALGORITHM_TYPE_FAILED);
                } else {//初始化成功，打开摄像头、下载数据
                    initPermission();
                }
            }
        });
    }

    public void softInit() {
        MyApp.mFaceAlgorithm.init();
    }

    public boolean register(String register) {
        if (MyApp.isSoft) {
            Soft5003 soft5003 = (Soft5003) MyApp.mFaceAlgorithm;
            return soft5003.register(register);
        }
        return false;
    }

    //初始化相机权限
    private void initPermission() {
        boolean isPermission = checkPermission();
        if (isPermission) {
            initMediaPlayer();
            initData();
            //查询本地是否保存了设备号
            String deviceCode = SharedPreferencesUtils.getString(mContext, Constant.sharedPreferencesName, "deviceCode");
            if (TextUtils.isEmpty(deviceCode)) {
                deviceCode = "PB10010101";
            }
            if (!TextUtils.isEmpty(deviceCode)) {
                final String finalDeviceCode = deviceCode;
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mNetworkDataProcessing.getDevicesParam(finalDeviceCode, mIInitView);
                    }
                }, 1000);
            } else {
                mIInitView.showDeviceCodeDialog();
            }
        } else {
            mIInitView.showPermissionDialog("缺少访问摄像头权限");
        }
    }

    private void initData() {
        String ip = SharedPreferencesUtils.getString(mContext, Constant.sharedPreferencesName, "ip");
        if (!TextUtils.isEmpty(ip)) {
            Constant.sIp = ip;
        }
        String port = SharedPreferencesUtils.getString(mContext, Constant.sharedPreferencesName, "port");
        if (!TextUtils.isEmpty(port)) {
            Constant.sPort = port;
        }
        Constant.sUrl = Constant.sIp + ":" + Constant.sPort + Constant.sInterface;
    }

    @Override
    public void openColorCamera() {
        boolean isOpen = mCameraManager.openCamera(Camera.CameraInfo.CAMERA_FACING_FRONT);
        if (!isOpen) {
            mIInitView.showCameraDialog(Camera.CameraInfo.CAMERA_FACING_FRONT);
        }
    }

    public void setColorPreview(SurfaceHolder surfaceHolder, CameraManager.ICameraPreviewListener cameraPreviewListener) {
        mCameraManager.setICameraPreviewListener(cameraPreviewListener);
        mCameraManager.setICameraError(new CameraManager.ICameraError() {
            @Override
            public void cameraError(int error) {
                releaseMediaPlayer();
                initMediaPlayer();
            }
        });
        mCameraManager.setPreview(surfaceHolder);
        mCameraManager.startPreviewCallback();
    }

    @Override
    public void openInfraredCamera() {
        boolean isOpen = mInfraredCameraManager.openCamera(Camera.CameraInfo.CAMERA_FACING_BACK);
        if (!isOpen) {
            mIInitView.showCameraDialog(Camera.CameraInfo.CAMERA_FACING_BACK);
        }
    }

    public void setInfraredPreview(SurfaceHolder surfaceHolder, CameraManager.ICameraPreviewListener cameraPreviewListener) {
        mInfraredCameraManager.setICameraPreviewListener(cameraPreviewListener);
        mInfraredCameraManager.setICameraError(new CameraManager.ICameraError() {
            @Override
            public void cameraError(int error) {
                releaseMediaPlayer();
                initMediaPlayer();
            }
        });
        mInfraredCameraManager.setPreview(surfaceHolder);
        mInfraredCameraManager.startPreviewCallback();
    }

    private void initMediaPlayer() {
        //初始化音频播放
        mMediaPlayerManager = MediaPlayerManager.getInstance(mContext, R.raw.welcome);
    }

    private void releaseMediaPlayer() {
        if (mMediaPlayerManager != null) {
            mMediaPlayerManager.stop();
            mMediaPlayerManager.release();
            mMediaPlayerManager = null;
        }
    }

    public void release() {
        mCameraManager.release(true);
        mInfraredCameraManager.release(false);
    }

    public void getDevicesParam(String deviceCode) {
        mNetworkDataProcessing.updateIp(SystemUtils.getIp(mContext));
        mNetworkDataProcessing.getDevicesParam(deviceCode, mIInitView);
    }

    public boolean checkRegCode(String regCode) {
        if (MyApp.isSoft) {
            Soft5003 soft5003 = (Soft5003) MyApp.mFaceAlgorithm;
            return soft5003.checkRegCode(regCode);
        }
        return false;
    }

    public String getRunCode() {
        if (MyApp.isSoft) {
            Soft5003 soft5003 = (Soft5003) MyApp.mFaceAlgorithm;
            return soft5003.getRunCode();
        }
        return "";
    }

    //注册日志回调
    @Override
    public void d(String debug) {
        LogSaveUtil.d(TAG, debug);
    }

    @Override
    public void e(String error) {
        LogSaveUtil.e(TAG, error);
    }
}
