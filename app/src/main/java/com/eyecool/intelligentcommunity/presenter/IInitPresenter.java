package com.eyecool.intelligentcommunity.presenter;

import android.support.v7.app.AppCompatActivity;

/**
 * 初始化Presenter
 * Created by ouyangfan on 2018/8/22.
 */

public interface IInitPresenter {

    //软授权算法输入框
    int ALGORITHM_TYPE_INPUT = 1;

    //算法初始化失败
    int ALGORITHM_TYPE_FAILED = 2;

    //没有插硬狗算法
    int ALGORITHM_TYPE_ERROR = 3;

    //初始化，包括获取权限、初始化算法
    void init(AppCompatActivity activity);

    //打开摄像头，并设置预览
    void openColorCamera();

    //打开红外摄像头
    void openInfraredCamera();


}
