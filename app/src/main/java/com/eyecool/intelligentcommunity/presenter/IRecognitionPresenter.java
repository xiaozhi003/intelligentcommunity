package com.eyecool.intelligentcommunity.presenter;

import android.graphics.Bitmap;

/**
 * Created by ouyangfan on 2018/8/23.
 */

public interface IRecognitionPresenter {

  int RECOGNITION_TYPE_SUCCESS = 1;

  int RECOGNITION_TYPE_FAILED = -1;

  int RECOGNITION_TYPE_BLACKLIST = -2;//黑名单

  int RECOGNITION_TYPE_OUT_EXPIRY_TIME = -3;//超过了有效期

  int RECOGNITION_TYPE_NO_LIVE = -4;

  int RECOGNITION_TYPE_NO_PEOPLE = -5; // 没有人员

  //检测彩色照片人脸
  void detectColorFace(Bitmap bitmap);

  //检测黑白照片人脸
  void detectBWFace(Bitmap bitmap);

  // 检测彩色照片人脸（yuv）
  void detectColorFace(byte[] yuv);

  // 检测黑白照片人脸（yuv）
  void detectBWFace(byte[] yuv);
}
