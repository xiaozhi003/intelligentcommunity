package com.eyecool.intelligentcommunity.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceView;

import com.eyecool.library.utils.Logs;

public class CameraSurfaceView extends SurfaceView {

  private static final String TAG = CameraSurfaceView.class.getSimpleName();

  private boolean isPortrait = false;
  private static final int sWidth = 480;
  private static final int sHeight = 640;

  public CameraSurfaceView(Context context) {
    super(context);
  }

  public CameraSurfaceView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public CameraSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public boolean isPortrait() {
    return isPortrait;
  }

  public void setPortrait(boolean portrait) {
    isPortrait = portrait;
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    Logs.i("获取显示区域参数");
    // WindowManager manager = (WindowManager)
    // mContext.getSystemService(Context.WINDOW_SERVICE);
    // Display display = manager.getDefaultDisplay();
    // int desiredWidth =display.getWidth();
    // int desiredHeight = display.getHeight();

    int desiredWidth;
    int desiredHeight;

    if (isPortrait) {
      desiredWidth = sWidth;
      desiredHeight = sHeight;
    } else {
      desiredWidth = sHeight;
      desiredHeight = sWidth;
    }

    float radio = (float) desiredWidth / (float) desiredHeight;
    Log.d(TAG, "获取显示区域参数 radio:" + radio);

    /**
     * 每个MeasureSpec均包含两种数据，尺寸和设定类型，需要通过 MeasureSpec.getMode和getSize进行提取
     */
    int widthMode = MeasureSpec.getMode(widthMeasureSpec);
    int widthSize = MeasureSpec.getSize(widthMeasureSpec);
    int heightMode = MeasureSpec.getMode(heightMeasureSpec);
    int heightSize = MeasureSpec.getSize(heightMeasureSpec);

    // 参考值竖屏 800 1214
    // 参考值横屏 1280 734
    int layout_width = 0;
    int layout_height = 0;

    if (widthMode == MeasureSpec.EXACTLY) {
      // 精确值情况
      layout_width = widthSize;
    } else if (widthMode == MeasureSpec.AT_MOST) {
      // 范围值情况，哪个小取哪个？
      layout_width = Math.min(desiredWidth, widthSize);
    } else {
      // 没设定就是默认的了，呵呵
      layout_width = desiredWidth;
    }
    // 高度设定同上
    if (heightMode == MeasureSpec.EXACTLY) {
      layout_height = heightSize;
    } else if (heightMode == MeasureSpec.AT_MOST) {
      layout_height = Math.min(desiredHeight, heightSize);
      // layout_height = Math.min(desiredWidth, widthSize) * 4 / 3;
    } else {
      layout_height = desiredHeight;
    }
    Log.i(TAG, "CSV选择宽度:" + layout_width + "  设定高度:" + layout_height);// 让我们来输出他们
    float layout_radio = (float) layout_width / (float) layout_height;

    if (layout_radio > radio) {
      layout_height = (int) (layout_width / radio);
    } else {
      layout_width = (int) (layout_height * radio);
    }

    setMeasuredDimension(layout_width, layout_height);
    Log.i(TAG, "CSV设定宽度:" + widthSize + "  设定高度:" + heightSize);// 让我们来输出他们
    Log.i(TAG, "CSV实际宽度:" + layout_width + "  实际高度:" + layout_height);// 让我们来输出他们
    Log.i(TAG, "显示比例：" + ((float) layout_width / (float) layout_height));
  }
}
