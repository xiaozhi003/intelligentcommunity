package com.eyecool.intelligentcommunity.view;

/**
 * Created by ouyangfan on 2018/8/22.
 */

public interface IInitView {

    /**
     * 显示未获取权限对话框
     */
    void showPermissionDialog(String message);

    /**
     *
     * @param type 1代表软授权输入对话框，2代表算法失败对话框，3代表没有插硬狗
     */
    void showAlgorithmDialog(int type);

    /**
     * 显示打开摄像头失败对话框
     * @param type 摄像头类型
     */
    void showCameraDialog(int type);

    //显示输入设备号对话框
    void showDeviceCodeDialog();

    //显示设备号错误提示框
    void showDeviceCodeErrorDialog(String message);

    //显示初始化类型
    void showInitType(String message);

    //初始化完成
    void initComplete();

    // 授权
    void onRegCode(String regCode);
}
