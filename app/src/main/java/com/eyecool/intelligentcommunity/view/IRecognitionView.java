package com.eyecool.intelligentcommunity.view;

/**
 * Created by ouyangfan on 2018/8/23.
 */

public interface IRecognitionView {

    void showAdv();

    void hideAdv();

    void showResult(int type, String message);

}
