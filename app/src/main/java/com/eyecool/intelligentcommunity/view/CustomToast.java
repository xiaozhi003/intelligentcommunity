package com.eyecool.intelligentcommunity.view;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eyecool.intelligentcommunity.R;
import com.eyecool.library.utils.Logs;

/**
 * Created by ouyangfan on 2018/7/13.
 */

public class CustomToast {

    private Toast mToast;
    private TextView mTextView;
    private ImageView mImageView;
    private TimeCount timeCount;
    private String message;
    private Handler mHandler = new Handler();
    private boolean canceled = true;

    public CustomToast(boolean isCorrect, Context context, String msg) {
        message = msg;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //自定义布局
        View view = inflater.inflate(R.layout.toast_correct, null);
        //自定义toast文本
        mImageView = view.findViewById(R.id.image_toast);
        mTextView = view.findViewById(R.id.text_toast);
        mTextView.setText(msg);
        if (!isCorrect) {//识别失败
            mTextView.setBackgroundResource(R.mipmap.error_bg);
            mImageView.setImageResource(R.mipmap.error);
        }
        Logs.i("Toast start...");
        if (mToast == null) {
            mToast = new Toast(context);
            Logs.i("Toast create...");
        }
        //设置toast居中显示
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setView(view);
    }

    public void setMessage(String message){
        if (mTextView != null){
            mTextView.setText(message);
        }
    }

    public void setIsCorrect(boolean isCorrect){
        if (!isCorrect) {//识别失败
            mTextView.setBackgroundResource(R.mipmap.error_bg);
            mImageView.setImageResource(R.mipmap.error);
        }else{
            mTextView.setBackgroundResource(R.mipmap.correct_bg);
            mImageView.setImageResource(R.mipmap.correct);
        }
    }

    /**
     * 自定义居中显示toast
     */
    public void show() {
        mToast.show();
    }

    /**
     * 自定义时长、居中显示toast
     *
     * @param duration
     */
    public void show(int duration) {
        timeCount = new TimeCount(duration, 1000);
        if (canceled) {
            timeCount.start();
            canceled = false;
            showUntilCancel();
        }
    }

    public boolean isCanceled() {
        return canceled;
    }

    /**
     * 隐藏toast
     */
    public void hide() {
        if (mToast != null) {
            mToast.cancel();
        }
        canceled = true;
    }

    private void showUntilCancel() {
        if (canceled) { //如果已经取消显示，就直接return
            return;
        }
        mToast.show();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Logs.i("Toast showUntilCancel...");
                showUntilCancel();
            }
        }, Toast.LENGTH_LONG);
    }

    /**
     * 自定义计时器
     */
    private class TimeCount extends CountDownTimer {

        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval); //millisInFuture总计时长，countDownInterval时间间隔(一般为1000ms)
        }

        @Override
        public void onTick(long millisUntilFinished) {
            mTextView.setText(message + ": " + millisUntilFinished / 1000 + "s后消失");
        }

        @Override
        public void onFinish() {
            hide();
        }
    }
}
